CREATE USER 'enugantis'@'localhost' IDENTIFIED BY 'P@ssword12*';

create database LIMS;

GRANT ALL PRIVILEGES ON LIMS.* TO 'enugantis'@'localhost' WITH GRANT OPTION;

SET GLOBAL log_bin_trust_function_creators = 1;

>mysql -u enugantis -p

USE LIMS;

CREATE TABLE COUNTRIES(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	code varchar(16) NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id)
 );

CREATE TABLE APPLICATION_STATUS(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description varchar(255) NOT NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id)
);

CREATE TABLE APPLICATION_DOCUMENT_TYPES(
	id int NOT NULL AUTO_INCREMENT,
	type varchar(255) NOT NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id)
 );
 
 CREATE TABLE APPLICATION_MAIN_MODULES(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1' default 1,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id)
 );

CREATE TABLE APPLICATION_MODULES(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1' default 1,
	main_module_id int not NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (main_module_id) REFERENCES APPLICATION_MAIN_MODULES(id)
 );

CREATE TABLE APPLICATION_ROLES(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	description text NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id)
 );
 
 CREATE TABLE AUDIT_LOG(
	id int NOT NULL AUTO_INCREMENT,
	entity_id int not NULL,
	module_id int not NULL,
	action_type varchar(20) NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (module_id) REFERENCES APPLICATION_MODULES(id)
);

 CREATE TABLE ADDRESS(
	id int NOT NULL AUTO_INCREMENT,
	country_id int NULL,
	address_line1 varchar(255) NULL,
	address_line2 varchar(255) NULL,
	city varchar(255) NULL,
	state varchar(255) NULL,
	pin varchar(10) NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (country_id) REFERENCES COUNTRIES(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

 CREATE TABLE CORPORATE(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description text NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_date datetime NULL,
	created_by int NOT NULL,
	updated_date datetime NULL,
	updated_by int NULL,
	physical_address_id int NULL,
	invoice_address_id int NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (physical_address_id) REFERENCES ADDRESS(id),
	FOREIGN KEY (invoice_address_id) REFERENCES ADDRESS(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE DEPARTMENTS(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description text NULL,
	corporate_id int not NULL,
	address_id int not NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_date datetime NOT NULL,
	created_by int NOT NULL,
	updated_date datetime NULL,
	updated_by int NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (corporate_id) REFERENCES CORPORATE(id),
	FOREIGN KEY (address_id) REFERENCES ADDRESS(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE LABS(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description text NULL,
	address_id int NULL,
	department_id int not NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_date datetime NOT NULL,
	created_by int NOT NULL,
	updated_date datetime NULL,
	updated_by int NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (department_id) REFERENCES DEPARTMENTS(id),
	FOREIGN KEY (address_id) REFERENCES ADDRESS(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE APPLICATION_SECURITY_ROLES(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description text NULL,
	lab_id int NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_date datetime NULL,
	created_by int NOT NULL,
	updated_date datetime NULL,
	updated_by int NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (lab_id) REFERENCES LABS(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE APPLICATION_SECURITY_ROLES_MAP(
	id int NOT NULL AUTO_INCREMENT,
	security_id int NOT NULL,
	module_id int NOT NULL,
	create_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	edit_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	view_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	archive_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	delete_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	print_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	run_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	upload_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	share_flag tinyint UNSIGNED NOT NULL DEFAULT '1',
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (security_id) REFERENCES APPLICATION_SECURITY_ROLES(id),
	FOREIGN KEY (module_id) REFERENCES APPLICATION_MODULES(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE APPLICATION_USER(
	id int NOT NULL AUTO_INCREMENT,
	first_name varchar(128) NULL,
	last_name varchar(128) NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	is_temporary tinyint UNSIGNED NOT NULL DEFAULT '1',
	username varchar(128) NOT NULL,
	password varchar(128) NOT NULL,
	emp_id varchar(128) NULL,
	email varchar(128) NULL,
	description text NULL,
	mobile_no varchar(128) NULL,
	created_date datetime NOT NULL,
	created_by int NULL,
	updated_date datetime NULL,
	updated_by int NULL,
	corporate_id int,
	last_login_date datetime NULL,
	last_logout_date datetime NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (corporate_id) REFERENCES CORPORATE(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 ); 
 
 CREATE TABLE APPLICATION_USER_ROLES(
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	security_role_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (security_role_id) REFERENCES APPLICATION_SECURITY_ROLES(id)
 );

CREATE TABLE APPLICATION_USER_DOCUMENTS(
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	isdeleted tinyint UNSIGNED NOT NULL DEFAULT '1',
	uploaded_by int NOT NULL,
	uploaded_date datetime NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (user_id) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id)
 );
 
 CREATE TABLE SOP_TYPE(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description text NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1' default 1,
	lab_id int not NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (lab_id) REFERENCES LABS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE SOP(
	id int NOT NULL AUTO_INCREMENT,
	sop_number varchar(255) NOT NULL,
	sop_title varchar(255) NOT NULL,
	sop_desciption text NULL,
	sop_type_id int not null,
	lab_id int not null,
	version float not null,
	revision float not null,
	comments text NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	status_id int not null,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sop_type_id) REFERENCES SOP_TYPE(id),
	FOREIGN KEY (lab_id) REFERENCES LABS(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE BATCH_TYPE(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description text NULL,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1' default 1,
	lab_id int not NULL,
	created_by int NOT NULL,
	created_date datetime NOT NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (lab_id) REFERENCES LABS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE BATCH_DETAILS(
	id int NOT NULL AUTO_INCREMENT,
	batch_number varchar(255) NULL,
	batch_title varchar(255) NOT NULL,
	batch_desciption text NULL,
	batch_type int NOT NULL,
	lab_id int not null,
	is_active tinyint UNSIGNED NOT NULL DEFAULT '1',
	created_by int NOt NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (batch_type) REFERENCES BATCH_TYPE(id),
	FOREIGN KEY (lab_id) REFERENCES LABS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );
 
 CREATE TABLE BATCH_TEAM(
	id int NOT NULL AUTO_INCREMENT,
	batch_detail_id int NOT NULL,
	module_id int NOT NULL,
	lab_id int not null,
	role varchar(200) NOT NULL,
	assigned_to int NOT NULL,
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	audit_id int NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (lab_id) REFERENCES LABS(id),
	FOREIGN KEY (batch_detail_id) REFERENCES BATCH_DETAILS(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
 );

 
 CREATE TABLE BATCH_RECORD(
	id int NOT NULL AUTO_INCREMENT,
	batch_detail_id int NOT NULL,
	status_id int not null,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_by int not NULL,
	uploaded_date datetime not NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (batch_detail_id) REFERENCES BATCH_DETAILS(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id)
 );
 
 CREATE TABLE BATCH_SOPS(
	id int NOT NULL AUTO_INCREMENT,
	batch_rec_id int NOT NULL,
	sop_id int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (batch_rec_id) REFERENCES BATCH_RECORD(id),
	FOREIGN KEY (sop_id) REFERENCES SOP(id)
);

CREATE TABLE QMS_DEVIATION(
	id int NOT NULL AUTO_INCREMENT,
	batch_number varchar(250) NOT NULL,
	deviation_number varchar(250) NOT NULL,
	initial_desc text NULL,
	imd_corrective_action text NULL,
	status_id int not null,
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	sop_change tinyint NULL,
	sop_id int NULL,
	sop_change_desc text NULL,
	audit_id int NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_by int not NULL,
	uploaded_date datetime not NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sop_id) REFERENCES SOP(id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_OOS(
	id int NOT NULL AUTO_INCREMENT,
	batch_number varchar(250) NOT NULL,
	batch_section varchar(250) NOT NULL,
	oos_number varchar(250) NOT NULL,
	initial_desc text NULL,
	imd_corrective_action text NULL,
	status_id int not null,
	investigation_results text NULL,
	sop_change tinyint NULL,
	sop_id int NULL,
	sop_change_desc text NULL,
	audit_id int NULL,
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_by int not NULL,
	uploaded_date datetime not NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sop_id) REFERENCES SOP(id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_INCIDENT(
	id int NOT NULL AUTO_INCREMENT,
	incident_source varchar(100) NOT NULL,
	incident_number varchar(250) NOT NULL,
	identificaton_desc text NOT NULL,
	sop_change tinyint NULL,
	sop_id int NULL,
	sop_change_desc text NULL,
	status_id int not null,
	audit_id int NULL,
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_by int not NULL,
	uploaded_date datetime not NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sop_id) REFERENCES SOP(id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_CHANGE_CONTROL(
	id int NOT NULL AUTO_INCREMENT,
	cc_number varchar(250) NOT NULL,
	batch_number varchar(250) NOT NULL,
	current_situation text NULL,
	current_situation_reason text NULL,
	expected_outcome text NULL,
	sop_change tinyint NULL,
	sop_id int NULL,
	sop_change_desc text NULL,
	status_id int not null,
	audit_id int NULL,
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_by int not NULL,
	uploaded_date datetime not NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sop_id) REFERENCES SOP(id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_CAPA(
	id int NOT NULL AUTO_INCREMENT,
	batch_number varchar(250) NOT NULL,
	capa_number varchar(250) NOT NULL,
	reporting_source varchar(250) NOT NULL,
	source_reference varchar(250) NULL,
	capa_desc text NULL,
	root_cause_desc text NULL,
	cor_prev_changes text NULL,
	target_invst_completion datetime NULL,
	invst_compl_ext tinyint NULL,
	new_invst_completion datetime NULL,
	extension_reason text NULL,
	status_id int not null,
	sop_change tinyint NULL,
	sop_id int NULL,
	sop_change_desc text NULL,
	audit_id int NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_by int not NULL,
	uploaded_date datetime not NULL,
	created_by int NOT NULL,
	created_date datetime not NULL,
	updated_by int NULL,
	updated_date datetime NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sop_id) REFERENCES SOP(id),
	FOREIGN KEY (status_id) REFERENCES APPLICATION_STATUS(id),
	FOREIGN KEY (created_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (updated_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (document_type_id) REFERENCES APPLICATION_DOCUMENT_TYPES(id),
	FOREIGN KEY (uploaded_by) REFERENCES APPLICATION_USER(id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);
 
 CREATE TABLE WORK_FLOW_METRIX(
	id int NOT NULL AUTO_INCREMENT,
	entity_id int not null,
	entity_type varchar(100),
	work_flow_level varchar(100) NULL,
	work_flow_order int null,
	comments text null,
	corrective_action text NULL,
	conclusion text NULL,
	recomendation text NULL,
	assigned_to int NULL,
	signed_flag tinyint NULL,
	signed_date datetime NULL,
	document_type_id int NULL,
	document_name varchar(256) NULL,
	content_type varchar(256) NULL,
	content LONGBLOB NULL,
	filesize int not NULL,
	uploaded_date datetime not NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (assigned_to) REFERENCES APPLICATION_USER(id)
 );
 
 CREATE TABLE APPLICATION_SCHEDULER(
	id int NOT NULL AUTO_INCREMENT,
	entity_id int not null,
	entity_type varchar(250),
	scheduler_event varchar(1000),
	sop_request varchar(50),
	scheduled_day varchar(100),
	no_of_weeks int,
	start_date datetime,
	end_date datetime,
	PRIMARY KEY (id)
 );
 
 CREATE TABLE APPLICATION_NOTIFICATION(
	id int NOT NULL AUTO_INCREMENT,
	entity_id int not null,
	entity_type varchar(100),
	notification_message varchar(2000),
	notification_type varchar(250),
	PRIMARY KEY (id)
);

-- LOG TABLES
 
CREATE TABLE ADDRESS_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE CORPORATE_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE DEPARTMENTS_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE LABS_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE SECURITY_ROLE_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE SECURITY_ROLE_MAP_LOG(
	id int NOT NULL AUTO_INCREMENT,
	field varchar(100) NULL,
	audit_id int not NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE APPLICATION_USER_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE SOP_TYPE_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE SOP_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE BATCH_TYPE_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not NULL, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE BATCH_DETAILS_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE BATCH_TEAM_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_DEVIATION_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_CAPA_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_OOS_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_INCIDENT_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

CREATE TABLE QMS_CHANGE_CONTROL_LOG(
	id int NOT NULL AUTO_INCREMENT,
	audit_id int not null, 
	field varchar(100) NULL,
	old_value text NULL,
	new_value text NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (audit_id) REFERENCES AUDIT_LOG(id)
);

-- Insert scrit

INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (1, 'United States', 1, 'USA', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (2, 'India', 1, 'IND', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (3, 'Canada', 1, 'CAN', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (4, 'United Kingdom', 1, 'UK', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (5, 'Australia', 1, 'AUS', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (6, 'Mexico', 1, 'MEX', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (7, 'Germany', 1, 'DEU', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (8, 'France', 1, 'FRA', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (9, 'Switzerland', 1, 'CHE', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (10, 'Spain', 1, 'ESP', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (11, 'Singapore', 1, 'SGP', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (12, 'Hongkong', 1, 'HKG', 1, sysdate());
INSERT COUNTRIES (Id, Name, is_active, Code, created_by, created_date) VALUES (13, 'Italy', 1, 'ITA', 1, sysdate());



INSERT APPLICATION_STATUS (ID, name, description, created_by, created_date) VALUES (1, 'DRAFT', 'In Draft', 1, sysdate());
INSERT APPLICATION_STATUS (ID, name, description, created_by, created_date) VALUES (2, 'IN_REVIEW', 'In Review', 1, sysdate());
INSERT APPLICATION_STATUS (ID, name, description, created_by, created_date) VALUES (3, 'FIRST_APPROVED', 'First Approved', 1, sysdate());
INSERT APPLICATION_STATUS (ID, name, description, created_by, created_date) VALUES (4, 'SECOND_APPROVED', 'Second Approved', 1, sysdate());
INSERT APPLICATION_STATUS (ID, name, description, created_by, created_date) VALUES (5, 'TRAINING', 'Training to do', 1, sysdate());
INSERT APPLICATION_STATUS (ID, name, description, created_by, created_date) VALUES (6, 'ACTIVE', 'Active', 1, sysdate());


INSERT APPLICATION_MAIN_MODULES (ID, name, created_by, created_date) VALUES (1, 'Admin', 1, sysdate());
INSERT APPLICATION_MAIN_MODULES (ID, name, created_by, created_date) VALUES (2, 'SOP Type', 1, sysdate());
INSERT APPLICATION_MAIN_MODULES (ID, name, created_by, created_date) VALUES (3, 'QMS', 1, sysdate());
INSERT APPLICATION_MAIN_MODULES (ID, name, created_by, created_date) VALUES (4, 'Training', 1, sysdate());



INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (1, 'SOP', 2, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (2, 'SOP Type', 2, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (3, 'Users', 1, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (4, 'User Groups', 1, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (5, 'Security Roles', 1, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (6, 'Bulk Uploads', 2, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (7, 'Deviations', 3, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (8, 'CAPA', 3, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (9, 'OOS', 3, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (10, 'Incident Report', 3, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (11, 'Change Control', 3, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (12, 'Training', 4, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (13, 'Corporate', 1, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (14, 'Department', 1, 1, sysdate());
INSERT APPLICATION_MODULES (ID, name, main_module_id, created_by, created_date) VALUES (15, 'Lab', 1, 1, sysdate());




INSERT APPLICATION_ROLES (ID, name, is_active, created_by, created_date) VALUES (1, 'Super Admin', 1, 1, sysdate());
INSERT APPLICATION_ROLES (ID, name, is_active, created_by, created_date) VALUES (2, 'Lab Admin', 1, 1, sysdate());
INSERT APPLICATION_ROLES (ID, name, is_active, created_by, created_date) VALUES (3, 'User', 1, 1, sysdate());
INSERT APPLICATION_ROLES (ID, name, is_active, created_by, created_date) VALUES (4, 'Contractor', 1, 1, sysdate());
INSERT APPLICATION_ROLES (ID, name, is_active, created_by, created_date) VALUES (5, 'Intern', 1, 1, sysdate());
INSERT APPLICATION_ROLES (ID, name, is_active, created_by, created_date) VALUES (6, 'Support Admin', 1, 1, sysdate());

----- After triggers compiled.

INSERT INTO AUDIT_LOG(id, entity_id, module_id, action_type, created_by, created_date) values (1, 1, 13, 'CREATE', 1, sysdate());
INSERT INTO ADDRESS(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (1, 2, 'Nizampet', NULL, 'Hyderabad', 'Telangana', '500090', 1);
INSERT INTO ADDRESS(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (4, 2, 'Nizampet', NULL, 'Hyderabad', 'Telangana', '500090', 1);
INSERT INTO CORPORATE(id, name, description, is_active, created_by, created_date, physical_address_id, invoice_address_id, audit_id) values (1, 'CIPLA', 'Pharma', 1, 1, sysdate(), 1, 4, 1);
UPDATE ADDRESS set audit_id=NULL;
UPDATE CORPORATE set audit_id=NULL;

INSERT INTO AUDIT_LOG(id, entity_id, module_id, action_type, created_by, created_date) values (4, 1, 5, 'CREATE', 1, sysdate());
INSERT INTO APPLICATION_SECURITY_ROLES(id,audit_id,created_by,created_date,description,is_active,name) values
(1, 4, 1,sysdate(), 'Super Admin', 1, 'Super Admin');
UPDATE APPLICATION_SECURITY_ROLES set audit_id=NULL;


INSERT INTO AUDIT_LOG(id, entity_id, module_id, action_type, created_by, created_date) values (5, 1, 3, 'CREATE', 1, sysdate());
INSERT INTO APPLICATION_USER (id,audit_id,corporate_id,created_by,created_date,description,email,emp_id,first_name,is_active,is_temporary,last_login_date,last_logout_date,last_name,mobile_no,password,username) values 
(1,5,1,1,sysdate(),'SuperAdmin','sudhakarenuganti@gmail.com',
'egil36255','sudhakar',1,0,NULL,NULL,
'enuganti','8008801145','password1','enugantis');
UPDATE APPLICATION_USER set audit_id=NULL;

INSERT INTO AUDIT_LOG(id, entity_id, module_id, action_type, created_by, created_date) values (2, 1, 14, 'CREATE', 1, sysdate());
INSERT INTO ADDRESS(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (2, 2, 'Ameerpet', NULL, 'Hyderabad', 'Telangana', '500090', 2);
INSERT INTO DEPARTMENTS(id, name, description, corporate_id, address_id, is_active, created_by, created_date, audit_id) values (1, 'QA', 'Pharma QA', 1, 2, 1, 1, sysdate(), 2);
UPDATE ADDRESS set audit_id=NULL;
UPDATE DEPARTMENTS set audit_id=NULL;

INSERT INTO AUDIT_LOG(id, entity_id, module_id, action_type, created_by, created_date) values (3, 1, 15, 'CREATE', 1, sysdate());
INSERT INTO ADDRESS(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (3, 2, 'Ameerpet', NULL, 'Hyderabad', 'Telangana', '500090', 3);
INSERT INTO LABS(id, name, description, department_id, address_id, is_active, created_by, created_date, audit_id) values (1, 'QA', 'Pharma QA', 1, 3, 1, 1, sysdate(), 3);
UPDATE ADDRESS set audit_id=NULL;
UPDATE LABS set audit_id=NULL;


-- ALTER Commands

ALTER TABLE COUNTRIES ADD CONSTRAINT FK_CTRY_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE COUNTRIES ADD CONSTRAINT FK_CTRY_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_STATUS ADD CONSTRAINT FK_APPL_STS_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_STATUS ADD CONSTRAINT FK_APPL_STS_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_DOCUMENT_TYPES ADD CONSTRAINT FK_APPL_DOC_TYP_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_DOCUMENT_TYPES ADD CONSTRAINT FK_APPL_DOC_TYP_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_MAIN_MODULES ADD CONSTRAINT FK_APPL_MMOD_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_MAIN_MODULES ADD CONSTRAINT FK_APPL_MMOD_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_MODULES ADD CONSTRAINT FK_APPL_MOD_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_MODULES ADD CONSTRAINT FK_APPL_MOD_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_ROLES ADD CONSTRAINT FK_APPL_ROLE_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_ROLES ADD CONSTRAINT FK_APPL_ROLE_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE AUDIT_LOG ADD CONSTRAINT FK_AUD_LOG_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE CORPORATE ADD CONSTRAINT FK_CORP_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE CORPORATE ADD CONSTRAINT FK_CORP_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE DEPARTMENTS ADD CONSTRAINT FK_DEPT_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE DEPARTMENTS ADD CONSTRAINT FK_DEPT_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE LABS ADD CONSTRAINT FK_LAB_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE LABS ADD CONSTRAINT FK_LAB_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_SECURITY_ROLES ADD CONSTRAINT FK_APP_SEC_ROLE_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_SECURITY_ROLES ADD CONSTRAINT FK_APP_SEC_ROLE_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);

ALTER TABLE APPLICATION_USER ADD CONSTRAINT FK_APP_USER_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES APPLICATION_USER (ID);
ALTER TABLE APPLICATION_USER ADD CONSTRAINT FK_APP_USER_updated_BY FOREIGN KEY (updated_BY) REFERENCES APPLICATION_USER (ID);