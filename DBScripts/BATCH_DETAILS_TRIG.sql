create trigger BATCH_DETAILS_INS_TRIG after insert on BATCH_DETAILS for each row
begin
if(new.audit_id is not null) then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH NUMBER', null, new.batch_number);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TITLE', null, new.batch_title);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', null, new.batch_desciption);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TYPE', null, new.batch_type);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger BATCH_DETAILS_UPD_TRIG after update on BATCH_DETAILS for each row
begin
if(new.audit_id is not null) then
if((new.batch_number <> old.batch_number) or (new.batch_number is null and old.batch_number is not null) or (new.batch_number is not null and old.batch_number is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH NUMBER', old.batch_number, new.batch_number);
end if;
if((new.batch_title <> old.batch_title) or (new.batch_title is null and old.batch_title is not null) or (new.batch_title is not null and old.batch_title is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TITLE', old.batch_title, new.batch_title);
end if;
if((new.batch_desciption <> old.batch_desciption) or (new.batch_desciption is null and old.batch_desciption is not null) or (new.batch_desciption is not null and old.batch_desciption is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', old.batch_desciption, new.batch_desciption);
end if;
if((new.batch_type <> old.batch_type) or (new.batch_type is null and old.batch_type is not null) or (new.batch_type is not null and old.batch_type is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TYPE', old.batch_type, new.batch_type);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger BATCH_DETAILS_DEL_TRIG after delete on BATCH_DETAILS for each row
begin
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH NUMBER', old.batch_number, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH TITLE', old.batch_title, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCRIPTION', old.batch_desciption, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH TYPE', old.batch_type, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//