create database lims;

GRANT ALL PRIVILEGES ON lims.* TO 'enugantis'@'localhost' WITH GRANT OPTION;

SET GLOBAL log_bin_trust_function_creators = 1;

set global max_allowed_packet=10000000000;

USE lims;

set global max_allowed_packet=10000000000;

create table countries(
	id int not null auto_increment,
	name varchar(255) not null,
	is_active tinyint unsigned not null default '1',
	code varchar(16) null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	primary key (id)
 );

create table application_document_types(
	id int not null auto_increment,
	type varchar(255) not null,
	is_active tinyint unsigned not null default '1',
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	primary key (id)
 );
 
 create table application_main_modules(
	id int not null auto_increment,
	name varchar(255) not null,
	enum_key varchar(100) not null,
	is_active tinyint unsigned not null default '1',
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	primary key (id)
 );

create table application_modules(
	id int not null auto_increment,
	name varchar(255) not null,
	enum_key varchar(100) not null,
	is_active tinyint unsigned not null default '1',
	main_module_id int not null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	primary key (id),
	foreign key (main_module_id) references application_main_modules(id)
 );

create table application_status(
	id int not null auto_increment,
	name varchar(255) not null,
	enum_key varchar(100) not null,
	module_id int not null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	primary key (id),
	foreign key (module_id) references application_modules(id)
);

create table application_roles(
	id int not null auto_increment,
	name varchar(255) not null,
	is_active tinyint unsigned not null default '1',
	description text null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	primary key (id)
 );
 
 create table audit_log(
	id int not null auto_increment,
	entity_id int not null,
	module_id int not null,
	action_type varchar(20) null,
	created_by int not null,
	created_date datetime not null,
	primary key (id),
	foreign key (module_id) references application_modules(id)
);

 create table address(
	id int not null auto_increment,
	country_id int null,
	address_line1 varchar(255) null,
	address_line2 varchar(255) null,
	city varchar(255) null,
	state varchar(255) null,
	pin varchar(10) null,
	audit_id int null,
	primary key (id),
	foreign key (country_id) references countries(id),
	foreign key (audit_id) references audit_log(id)
);

 create table corporate(
	id int not null auto_increment,
	name varchar(255) not null,
	description text null,
	is_active tinyint unsigned not null default '1',
	created_date datetime null,
	created_by int not null,
	updated_date datetime null,
	updated_by int null,
	physical_address_id int null,
	invoice_address_id int null,
	audit_id int null,
	primary key (id),
	foreign key (physical_address_id) references address(id),
	foreign key (invoice_address_id) references address(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table departments(
	id int not null auto_increment,
	name varchar(255) not null,
	description text null,
	corporate_id int not null,
	address_id int not null,
	is_active tinyint unsigned not null default '1',
	created_date datetime not null,
	created_by int not null,
	updated_date datetime null,
	updated_by int null,
	audit_id int null,
	primary key (id),
	foreign key (corporate_id) references corporate(id),
	foreign key (address_id) references address(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table labs(
	id int not null auto_increment,
	name varchar(255) not null,
	description text null,
	address_id int null,
	department_id int not null,
	is_active tinyint unsigned not null default '1',
	created_date datetime not null,
	created_by int not null,
	updated_date datetime null,
	updated_by int null,
	audit_id int null,
	primary key (id),
	foreign key (department_id) references departments(id),
	foreign key (address_id) references address(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table application_security_roles(
	id int not null auto_increment,
	name varchar(255) not null,
	description text null,
	lab_id int null,
	is_active tinyint unsigned not null default '1',
	created_date datetime null,
	created_by int not null,
	updated_date datetime null,
	updated_by int null,
	audit_id int null,
	primary key (id),
	foreign key (lab_id) references labs(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table application_security_roles_map(
	id int not null auto_increment,
	security_id int not null,
	module_id int not null,
	create_flag tinyint unsigned not null default '0',
	edit_flag tinyint unsigned not null default '0',
	view_flag tinyint unsigned not null default '0',
	archive_flag tinyint unsigned not null default '0',
	delete_flag tinyint unsigned not null default '0',
	print_flag tinyint unsigned not null default '0',
	run_flag tinyint unsigned not null default '0',
	upload_flag tinyint unsigned not null default '0',
	share_flag tinyint unsigned not null default '0',
	author_flag tinyint unsigned not null default '0',
	reviewer_flag tinyint unsigned not null default '0',
	approver_flag tinyint unsigned not null default '0',
	audit_id int null,
	primary key (id),
	foreign key (security_id) references application_security_roles(id),
	foreign key (module_id) references application_modules(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table application_user(
	id int not null auto_increment,
	first_name varchar(128) null,
	last_name varchar(128) null,
	is_active tinyint unsigned not null default '1',
	is_temporary tinyint unsigned not null default '1',
	username varchar(128) not null,
	password varchar(128) not null,
	emp_id varchar(128) null,
	email varchar(128) null,
	description text null,
	mobile_no varchar(128) null,
	created_date datetime not null,
	created_by int null,
	updated_date datetime null,
	updated_by int null,
	corporate_id int,
	notification_type varchar(250),
	last_login_date datetime null,
	last_logout_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (corporate_id) references corporate(id),
	foreign key (audit_id) references audit_log(id)
 ); 
 
 create table application_user_roles(
	id int not null auto_increment,
	user_id int not null,
	security_role_id int not null,
	audit_id int null,
	primary key (id),
	foreign key (user_id) references application_user(id),
	foreign key (security_role_id) references application_security_roles(id),
	foreign key (audit_id) references audit_log(id)
 );

create table application_user_documents(
	id int not null auto_increment,
	user_id int not null,
	document_type_id int null,
	document_name varchar(256) null,
	content_type varchar(256) null,
	content longblob null,
	filesize int not null,
	is_active tinyint unsigned not null default '1',
	isdeleted tinyint unsigned not null default '1',
	uploaded_by int not null,
	uploaded_date datetime not null,
	primary key(id),
	foreign key (user_id) references application_user(id),
	foreign key (document_type_id) references application_document_types(id),
	foreign key (uploaded_by) references application_user(id)
 );
 
 create table application_documents(
	id int not null auto_increment,
	lab_id int not null,
	entity_id int null,
	entity_type varchar(100),
	document_name varchar(256) null,
	document_type varchar(256) null,
	content longblob null,
	filesize int null,
	is_active tinyint unsigned not null default '1',
	isdeleted tinyint unsigned not null default '1',
	uploaded_by int not null,
	uploaded_date datetime not null,
	primary key(id),
	foreign key (lab_id) references labs(id),
	foreign key (uploaded_by) references application_user(id)
 );
 
 create table sop_type(
	id int not null auto_increment,
	name varchar(255) not null,
	description text null,
	is_active tinyint unsigned not null default '1',
	lab_id int not null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	color_code varchar(50),
	system_default tinyint unsigned not null default '0',
	visibility varchar(50),
	comments text null,
	primary key (id),
	foreign key (lab_id) references labs(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table sop_type_visibility(
 id int not null auto_increment,
 user_id int not null,
 sop_type_id int not null,
 audit_id int null,
 primary key (id),
 foreign key (user_id) references application_user(id),
 foreign key (sop_type_id) references sop_type(id),
 foreign key (audit_id) references audit_log(id)
 );
 
 create table application_module_steps(
	 id int not null auto_increment,
	 module_id int not null,
	 step_key varchar(255),
	 step_desc varchar(255),
	 enabled tinyint unsigned not null default '0',
	 clickable tinyint unsigned not null default '0',
	 step_order int not null,
	 created_by int not null,
	 created_date datetime not null,
	 primary key (id),
	 foreign key (module_id) references application_modules(id)
 );
 
 create table sop(
	id int not null auto_increment,
	number varchar(255) not null,
	name varchar(255) not null,
	desciption text null,
	type_id int not null,
	lab_id int not null,
	version varchar(20) null,
	revision varchar(20) null,
	status_id int null,
	is_active tinyint unsigned not null default '1',
	document_json longtext null,
	document_name varchar(256) null,
	content_type varchar(256) null,
	content longblob null,
	filesize int null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	current_step int null,
	completed_step int null,
	primary key (id),
	foreign key (type_id) references sop_type(id),
	foreign key (lab_id) references labs(id),
	foreign key (status_id) references application_status(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table work_flow_metrix(
	id int not null auto_increment,
	entity_id int not null,
	entity_type varchar(100),
	module_step_id int null,
	work_flow_order int null,
	corrective_action text null,
	conclusion text null,
	recomendation text null,
	assigned_to int null,
	signed_flag tinyint null,
	signed_date datetime null,
	document_name varchar(256) null,
	content_type varchar(256) null,
	content longblob null,
	filesize int null,
	uploaded_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (assigned_to) references application_user(id),
	foreign key (audit_id) references audit_log(id),
	foreign key (module_step_id) references application_module_steps(id)
 );
 
 create table application_comments(
	id int not null auto_increment,
	wf_metrix_id int not null,
	comments text null,
	updated_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (wf_metrix_id) references work_flow_metrix(id),
	foreign key (audit_id) references audit_log(id)
);
 
 
 create table application_scheduler(
	id int not null auto_increment,
	entity_id int not null,
	entity_type varchar(250),
	frequency_type varchar(100),
	repeat_on varchar(250),
	repeat_count int unsigned not null default '0',
	start_date datetime,
	end_date datetime,
	start_time varchar(50),
	end_time varchar(50),
	audit_id int null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table application_notification(
	id int not null auto_increment,
	entity_id int not null,
	entity_type varchar(100),
	notification_message varchar(2000),
    created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	primary key (id),
    foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
);

create table user_notification_status(
	id int not null auto_increment,
	user_id int not null,
	notification_id int not null,
	send_flag int unsigned not null default '0',
	read_flag int unsigned not null default '0',
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (user_id) references application_user(id),
	foreign key (notification_id) references application_notification(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
);


create table qms_deviation(
	id int not null auto_increment,
	batch_number varchar(250) not null,
	deviation_number varchar(250) not null,
	lab_id int not null,
	is_active tinyint unsigned not null default '1',
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	current_step int null,
	completed_step int null,
	primary key (id),
	foreign key (lab_id) references labs(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
);

create table qms_deviation_sops(
	id int not null auto_increment,
	deviation_id int not null,
	sop_id int not null,
	primary key (id),
	foreign key (deviation_id) references qms_deviation(id),
	foreign key (sop_id) references sop(id)
);

create table qms_oos(
	id int not null auto_increment,
	batch_number varchar(250) not null,
	batch_section varchar(250) not null,
	oos_number varchar(250) not null,
	lab_id int not null,
	is_active tinyint unsigned not null default '1',
	investigation_results text null,
	audit_id int null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	current_step int null,
	completed_step int null,
	primary key (id),
	foreign key (lab_id) references labs(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
);

create table qms_oos_sops(
	id int not null auto_increment,
	oos_id int not null,
	sop_id int not null,
	primary key (id),
	foreign key (oos_id) references qms_oos(id),
	foreign key (sop_id) references sop(id)
);


 create table batch_type(
	id int not null auto_increment,
	name varchar(255) not null,
	description text null,
	is_active tinyint unsigned not null default '1' default 1,
	lab_id int not null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (lab_id) references labs(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table batch_details(
	id int not null auto_increment,
	batch_number varchar(255) null,
	batch_title varchar(255) not null,
	batch_desciption text null,
	batch_type int not null,
	lab_id int not null,
	is_active tinyint unsigned not null default '1',
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (batch_type) references batch_type(id),
	foreign key (lab_id) references labs(id),
	foreign key (created_by) references application_user(id),
	foreign key (updated_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table batch_team(
	id int not null auto_increment,
	batch_detail_id int not null,
	module_id int not null,
	lab_id int not null,
	role varchar(200) not null,
	assigned_to int not null,
	created_by int not null,
	created_date datetime not null,
	updated_by int null,
	updated_date datetime null,
	audit_id int null,
	primary key (id),
	foreign key (lab_id) references labs(id),
	foreign key (batch_detail_id) references batch_details(id),
	foreign key (audit_id) references audit_log(id)
 );

 
 create table batch_record(
	id int not null auto_increment,
	batch_detail_id int not null,
	status_id int not null,
	document_name varchar(256) null,
	content_type varchar(256) null,
	content longblob null,
	filesize int not null,
	uploaded_by int not null,
	uploaded_date datetime not null,
	audit_id int null,
	primary key (id),
	foreign key (status_id) references application_status(id),
	foreign key (batch_detail_id) references batch_details(id),
	foreign key (uploaded_by) references application_user(id),
	foreign key (audit_id) references audit_log(id)
 );
 
 create table batch_sops(
	id int not null auto_increment,
	batch_rec_id int not null,
	sop_id int not null,
	audit_id int null,
	primary key (id),
	foreign key (batch_rec_id) references batch_record(id),
	foreign key (sop_id) references sop(id),
	foreign key (audit_id) references audit_log(id)
);


-- log tables
 
create table address_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table corporate_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table departments_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table labs_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table security_role_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table security_role_map_log(
	id int not null auto_increment,
	field varchar(100) null,
	audit_id int not null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table application_user_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table sop_type_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table sop_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);
create table work_flow_metrix_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table application_scheduler_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table application_notification_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table user_notification_status_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table application_document_comments_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table qms_oos_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table qms_deviation_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);


create table batch_type_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table batch_details_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table batch_team_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);

create table batch_record_log(
	id int not null auto_increment,
	audit_id int not null, 
	field varchar(100) null,
	old_value text null,
	new_value text null,
	primary key (id),
	foreign key (audit_id) references audit_log(id)
);


INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (1, 'United States', 1, 'USA', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (2, 'India', 1, 'IND', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (3, 'Canada', 1, 'CAN', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (4, 'United Kingdom', 1, 'UK', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (5, 'Australia', 1, 'AUS', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (6, 'Mexico', 1, 'MEX', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (7, 'Germany', 1, 'DEU', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (8, 'France', 1, 'FRA', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (9, 'Switzerland', 1, 'CHE', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (10, 'Spain', 1, 'ESP', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (11, 'Singapore', 1, 'SGP', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (12, 'Hongkong', 1, 'HKG', 1, sysdate());
INSERT into countries (Id, Name, is_active, Code, created_by, created_date) VALUES (13, 'Italy', 1, 'ITA', 1, sysdate());


INSERT into application_main_modules (ID, name, enum_key, created_by, created_date) VALUES (1, 'Admin', 'ADMIN', 1, sysdate());
INSERT into application_main_modules (ID, name, enum_key, created_by, created_date) VALUES (2, 'Sop', 'SOP', 1, sysdate());
INSERT into application_main_modules (ID, name, enum_key, created_by, created_date) VALUES (3, 'QMS', 'QMS', 1, sysdate());
INSERT into application_main_modules (ID, name, enum_key, created_by, created_date) VALUES (4, 'Training', 'TRAINING', 1, sysdate());



INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (1, 'Sop', 'SOP', 2, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (2, 'Sop Type', 'SOP_Type', 2, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (3, 'Users', 'USERS', 1, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (4, 'User Groups', 'USER_GROUPS', 1, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (5, 'Security Roles', 'SECURITY_ROLES', 1, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (6, 'Bulk Uploads', 'BULK_UPLOADS', 2, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (7, 'Deviations', 'DEVIATIONS', 3, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (8, 'CAPA', 'CAPA', 3, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (9, 'OOS', 'OOS', 3, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (10, 'Incident Report', 'INCIDENT_REPORT', 3, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (11, 'Change Control', 'CHANGE_CONTROL', 3, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (12, 'Training', 'TRAINING', 4, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (13, 'Corporate', 'CORPORATE', 1, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (14, 'Department', 'DEPARTMENT', 1, 1, sysdate());
INSERT into application_modules (ID, name, enum_key, main_module_id, created_by, created_date) VALUES (15, 'Lab', 'LAB', 1, 1, sysdate());

insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(1, 'INITIATOR', 'Sop Team', 1, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(1, 'AUTHOR', 'Create', 2, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(1, 'REVIEWER', 'Review', 3, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(1, 'FIRST_APPROVER', 'First Approval', 4, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(1, 'SECOND_APPROVER', 'Second Approval', 5, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(1, 'TRAINING', 'Training', 6, 1, 1, 1, sysdate());

INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (1, 'INITIATE', 'Initiate', 1, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (2, 'DRAFT', 'In Draft', 1, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (3, 'IN_REVIEW', 'In Review', 1, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (4, 'FIRST_APPROVED', 'First Approved', 1, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (5, 'SECOND_APPROVED', 'Second Approved', 1, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (6, 'TRAINING', 'Training to do', 1, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (7, 'ACTIVE', 'Active', 1, 1, sysdate());


INSERT into application_roles (ID, name, is_active, created_by, created_date) VALUES (1, 'Super Admin', 1, 1, sysdate());
INSERT into application_roles (ID, name, is_active, created_by, created_date) VALUES (2, 'Lab Admin', 1, 1, sysdate());
INSERT into application_roles (ID, name, is_active, created_by, created_date) VALUES (3, 'User', 1, 1, sysdate());
INSERT into application_roles (ID, name, is_active, created_by, created_date) VALUES (4, 'Contractor', 1, 1, sysdate());
INSERT into application_roles (ID, name, is_active, created_by, created_date) VALUES (5, 'Intern', 1, 1, sysdate());
INSERT into application_roles (ID, name, is_active, created_by, created_date) VALUES (6, 'Support Admin', 1, 1, sysdate());


DELIMITER //

create trigger address_ins_trig after insert on address for each row
begin
if(new.audit_id is not null) then
insert into address_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COUNTRY_ID', null, new.country_id);
insert into address_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS_LINE1', null, new.address_line1);
insert into address_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS_LINE2', null, new.address_line2);
insert into address_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CITY', null, new.city);
insert into address_log(audit_id, field, old_value, new_value) values (new.audit_id, 'STATE', null, new.state);
insert into address_log(audit_id, field, old_value, new_value) values (new.audit_id, 'PIN', null, new.pin);
end if;

end;
//
create trigger address_upd_trig after update on address for each row
begin
if(new.audit_id is not null) then
if((new.country_id <> old.country_id) or (new.country_id is null and old.country_id is not null) or (new.country_id is not null and old.country_id is null))
then
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'COUNTRY_ID', old.country_id, new.country_id);
end if;
if((new.address_line1 <> old.address_line1) or (new.address_line1 is null and old.address_line1 is not null) or (new.address_line1 is not null and old.address_line1 is null))
then
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE1', old.address_line1, new.address_line1);
end if;
if((new.address_line2 <> old.address_line2) or (new.address_line2 is null and old.address_line2 is not null) or (new.address_line2 is not null and old.address_line2 is null))
then
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE2', old.address_line2, new.address_line1);
end if;
if((new.city <> old.city) or (new.city is null and old.city is not null) or (new.city is not null and old.city is null))
then
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CITY', old.city, new.city);
end if;
if((new.state <> old.state) or (new.state is null and old.state is not null) or (new.state is not null and old.state is null))
then
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'STATE', old.state, new.state);
end if;
if((new.pin <> old.pin) or (new.pin is null and old.pin is not null) or (new.pin is not null and old.pin is null))
then
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'PIN', old.pin, new.pin);
end if;
end if;

end;
//
create trigger address_del_trig after delete on address for each row
begin
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'COUNTRY_ID', old.country_id, null);
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE1', old.address_line1, null);
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE2', old.address_line2, null);
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CITY', old.city, null);
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'STATE', old.state, null);
insert into address_log(audit_id, field, old_value, new_value) values (old.audit_id, 'PIN', old.pin, null);
end;
//


create trigger croporate_ins_trig after insert on corporate for each row
begin
if(new.audit_id is not null) then
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'PHYSICAL ADDRESS ID', null, new.physical_address_id);
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'INVOICE ADDRESS ID', null, new.invoice_address_id);
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger croporate_upd_trig after update on corporate for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into corporate_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger croporate_del_trig after delete on corporate for each row
begin
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'PHYSICAL ADDRESS ID', old.physical_address_id, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'INVOICE ADDRESS ID', old.invoice_address_id, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into corporate_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//


create trigger department_ins_trig after insert on departments for each row
begin
if(new.audit_id is not null) then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', null, new.corporate_id);
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS ID', null, new.address_id);
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger department_upd_trig after update on departments for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.corporate_id <> old.corporate_id) or (new.corporate_id is null and old.corporate_id is not null) or (new.corporate_id is not null and old.corporate_id is null))
then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', old.corporate_id, new.corporate_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into departments_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//

create trigger department_del_trig after delete on departments for each row
begin
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CORPORATE ID', old.corporate_id, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS ID', old.address_id, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into departments_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);

end;
//


create trigger lab_ins_trig after insert on labs for each row
begin
if(new.audit_id is not null) then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DEPARTMENT ID', null, new.department_id);
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS ID', null, new.address_id);
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger lab_upd_trig after update on labs for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.department_id <> old.department_id) or (new.department_id is null and old.department_id is not null) or (new.department_id is not null and old.department_id is null))
then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DEPARTMENT ID', old.department_id, new.department_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into labs_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger lab_del_trig after delete on labs for each row
begin
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DEPARTMENT ID', old.department_id, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS ID', old.address_id, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into labs_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//


create trigger security_role_ins_trig after insert on application_security_roles for each row
begin
if(new.audit_id is not null) then
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger security_role_upd_trig after update on application_security_roles for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into security_role_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger security_role_del_trig after delete on application_security_roles for each row
begin
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into security_role_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger application_user_ins_trig after insert on application_user for each row
begin
if(new.audit_id is not null) then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'FIRST NAME', null, new.first_name);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAST NAME', null, new.last_name);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'USERNAME', null, new.username);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'PASSWORD', null, new.password);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'EMPLOYEE ID', null, new.emp_id);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'EMAIL', null, new.email);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'MOBILE No.', null, new.mobile_no);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', null, new.corporate_id);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//

create trigger application_user_upd_trig after update on application_user for each row
begin
if(new.audit_id is not null) then
if((new.first_name <> old.first_name) or (new.first_name is null and old.first_name is not null) or (new.first_name is not null and old.first_name is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'FIRST NAME', old.first_name, new.first_name);
end if;
if((new.last_name <> old.last_name) or (new.last_name is null and old.last_name is not null) or (new.last_name is not null and old.last_name is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAST NAME', old.last_name, new.last_name);
end if;
if((new.username <> old.username) or (new.username is null and old.username is not null) or (new.username is not null and old.username is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'USERNAME', old.username, new.username);
end if;
if((new.password <> old.password) or (new.password is null and old.password is not null) or (new.password is not null and old.password is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'PASSWORD', old.password, new.password);
end if;
if((new.emp_id <> old.emp_id) or (new.emp_id is null and old.emp_id is not null) or (new.emp_id is not null and old.emp_id is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'EMPLOYEE ID', old.emp_id, new.emp_id);
end if;
if((new.email <> old.email) or (new.email is null and old.email is not null) or (new.email is not null and old.email is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'EMAIL', old.email, new.email);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.mobile_no <> old.mobile_no) or (new.mobile_no is null and old.mobile_no is not null) or (new.mobile_no is not null and old.mobile_no is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'MOBILE No.', old.mobile_no, new.mobile_no);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.corporate_id <> old.corporate_id) or (new.corporate_id is null and old.corporate_id is not null) or (new.corporate_id is not null and old.corporate_id is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', old.corporate_id, new.corporate_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger application_user_del_trig after delete on application_user for each row
begin
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'FIRST NAME', old.first_name, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'LAST NAME', old.last_name, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'USERNAME', old.username, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'PASSWORD', old.password, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'EMPLOYEE ID', old.emp_id, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'EMAIL', old.email, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'MOBILE No.', old.mobile_no, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CORPORATE ID', old.corporate_id, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);

end;
//

create trigger application_user_roles_ins_trig after insert on application_user_roles for each row
begin
if(new.audit_id is not null) then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SECURITY ROLE', null, new.security_role_id);
end if;

end;
//
create trigger application_user_roles_upd_trig after update on application_user_roles for each row
begin
if(new.audit_id is not null) then
if((new.security_role_id <> old.security_role_id) or (new.security_role_id is null and old.security_role_id is not null) or (new.security_role_id is not null and old.security_role_id is null))
then
insert into application_user_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SECURITY ROLE', old.security_role_id, new.security_role_id);
end if;

end if;

end;
//
create trigger application_user_roles_del_trig after delete on application_user_roles for each row
begin
insert into application_user_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SECURITY ROLE', old.security_role_id, null);

end;
//

create trigger security_role_map_ins_trig after insert on application_security_roles_map for each row
begin
if(new.audit_id is not null) then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATE FLAG', null, new.create_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'EDIT FLAG', null, new.edit_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ARCHIVE FLAG', null, new.archive_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'VIEW FLAG', null, new.view_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DELETE FLAG', null, new.delete_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'PRINT FLAG', null, new.print_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'RUN FLAG', null, new.run_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOAD FLAG', null, new.upload_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SHARE FLAG', null, new.share_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'AUTHOR FLAG', null, new.author_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REVIEWER FLAG', null, new.reviewer_flag);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'APPROVAL FLAG', null, new.approver_flag);

end if;
end;
//
create trigger security_role_map_upd_trig after update on application_security_roles_map for each row
begin
if(new.audit_id is not null) then
if((new.create_flag <> old.create_flag) or (new.create_flag is null and old.create_flag is not null) or (new.create_flag is not null and old.create_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATE FLAG', old.create_flag, new.create_flag);
end if;
if((new.edit_flag <> old.edit_flag) or (new.edit_flag is null and old.edit_flag is not null) or (new.edit_flag is not null and old.edit_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'EDIT FLAG', old.edit_flag, new.edit_flag);
end if;
if((new.view_flag <> old.view_flag) or (new.view_flag is null and old.view_flag is not null) or (new.view_flag is not null and old.view_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'VIEW FLAG', old.view_flag, new.view_flag);
end if;
if((new.archive_flag <> old.archive_flag) or (new.archive_flag is null and old.archive_flag is not null) or (new.archive_flag is not null and old.archive_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ARCHIVE FLAG', old.archive_flag, new.archive_flag);
end if;
if((new.delete_flag <> old.delete_flag) or (new.delete_flag is null and old.delete_flag is not null) or (new.delete_flag is not null and old.delete_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DELETE FLAG', old.delete_flag, new.delete_flag);
end if;
if((new.print_flag <> old.print_flag) or (new.print_flag is null and old.print_flag is not null) or (new.print_flag is not null and old.print_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'PRINT FLAG', old.print_flag, new.print_flag);
end if;
if((new.run_flag <> old.run_flag) or (new.run_flag is null and old.run_flag is not null) or (new.run_flag is not null and old.run_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'RUN FLAG', old.run_flag, new.run_flag);
end if;
if((new.upload_flag <> old.upload_flag) or (new.upload_flag is null and old.upload_flag is not null) or (new.upload_flag is not null and old.upload_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOAD FLAG', old.upload_flag, new.upload_flag);
end if;
if((new.share_flag <> old.share_flag) or (new.share_flag is null and old.share_flag is not null) or (new.share_flag is not null and old.share_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SHARE FLAG', old.share_flag, new.share_flag);
end if;
if((new.author_flag <> old.author_flag) or (new.author_flag is null and old.author_flag is not null) or (new.author_flag is not null and old.author_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'AUTHOR FLAG', old.author_flag, new.author_flag);
end if;
if((new.reviewer_flag <> old.reviewer_flag) or (new.reviewer_flag is null and old.reviewer_flag is not null) or (new.reviewer_flag is not null and old.reviewer_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REVIEWER FLAG', old.reviewer_flag, new.reviewer_flag);
end if;
if((new.approver_flag <> old.approver_flag) or (new.approver_flag is null and old.approver_flag is not null) or (new.approver_flag is not null and old.approver_flag is null))
then
insert into security_role_map_log(audit_id, field, old_value, new_value) values (new.audit_id, 'APPROVAL FLAG', old.approver_flag, new.approver_flag);
end if;

end if;
end;
//
create trigger security_role_map_del_trig after delete on application_security_roles_map for each row
begin
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATE FLAG', old.create_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'EDIT FLAG', old.edit_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ARCHIVE FLAG', old.archive_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'VIEW FLAG', old.view_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DELET FLAG', old.delete_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'PRINT FLAG', old.print_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'RUN FLAG', old.run_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPLOAD FLAG', old.upload_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SHARE FLAG', old.share_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'AUTHOR FLAG', old.author_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'REVIEWER FLAG', old.reviewer_flag, null);
insert into security_role_map_log(audit_id, field, old_value, new_value) values (old.audit_id, 'APPROVAL FLAG', old.approver_flag, null);

end;
//

create trigger sop_type_ins_trig after insert on sop_type for each row
begin
if(new.audit_id is not null) then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COLOR CODE', null, new.color_code);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SYSTEM DEFAULT', null, new.system_default);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'VISIBILITY', null, new.visibility);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', null, new.comments);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger sop_type_upd_trig after update on sop_type for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.color_code <> old.color_code) or (new.color_code is null and old.color_code is not null) or (new.color_code is not null and old.color_code is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COLOR CODE', old.color_code, new.color_code);
end if;
if((new.system_default <> old.system_default) or (new.system_default is null and old.system_default is not null) or (new.system_default is not null and old.system_default is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SYSTEM DEFAULT', old.system_default, new.system_default);
end if;
if((new.visibility <> old.visibility) or (new.visibility is null and old.visibility is not null) or (new.visibility is not null and old.visibility is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'VISIBILITY', old.visibility, new.visibility);
end if;
if((new.comments <> old.comments) or (new.comments is null and old.comments is not null) or (new.comments is not null and old.comments is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', old.comments, new.comments);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger sop_type_del_trig after delete on sop_type for each row
begin
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'COLOR CODE', old.color_code, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SYSTEM DEFAULT', old.system_default, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'VISIBILITY', old.visibility, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'COMMENTS', old.comments, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into sop_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger sop_type_visibility_ins_trig after insert on sop_type_visibility for each row
begin
if(new.audit_id is not null) then
insert into sop_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'USER ID', null, new.user_id);
end if;

end;
//

create trigger sop_type_visibility_del_trig before delete on sop_type_visibility for each row
begin

insert into sop_type_log(audit_id, field, old_value, new_value) values ((SELECT st.audit_id FROM sop_type st where st.id=old.sop_type_id), 'USER ID', old.user_id, null);

end;
//

create trigger sop_ins_trig after insert on sop for each row
begin
if(new.audit_id is not null) then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NUMBER', null, new.number);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', null, new.desciption);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TYPE', null, new.type_id);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'VERSION', null, new.version);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REVISION', null, new.revision);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS', null, new.status_id);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger sop_upd_trig after update on sop for each row
begin
if(new.audit_id is not null) then
if((new.number <> old.number) or (new.number is null and old.number is not null) or (new.number is not null and old.number is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NUMBER', old.number, new.number);
end if;
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.desciption <> old.desciption) or (new.desciption is null and old.desciption is not null) or (new.desciption is not null and old.desciption is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', old.desciption, new.desciption);
end if;
if((new.type_id <> old.type_id) or (new.type_id is null and old.type_id is not null) or (new.type_id is not null and old.type_id is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TYPE', old.type_id, new.type_id);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.version <> old.version) or (new.version is null and old.version is not null) or (new.version is not null and old.version is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'VERSION', old.version, new.version);
end if;
if((new.revision <> old.revision) or (new.revision is null and old.revision is not null) or (new.revision is not null and old.revision is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REVISION', old.revision, new.revision);
end if;
if((new.status_id <> old.status_id) or (new.status_id is null and old.status_id is not null) or (new.status_id is not null and old.status_id is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS', old.status_id, new.status_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into sop_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger sop_del_trig after delete on sop for each row
begin
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP NUMBER', old.number, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP TITLE', old.name, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCRIPTION', old.desciption, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP TYPE', old.type_id, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'VERSION', old.version, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'REVISION', old.revision, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'STATUS', old.status_id, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into sop_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//
create trigger work_flow_metrix_ins_trig after insert on work_flow_metrix for each row
begin
if(new.audit_id is not null) then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'MODULE STEP', null, new.module_step_id);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'WORK FLOW ORDER', null, new.work_flow_order);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CORRECTIVE ACTION', null, new.corrective_action);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CONCLUSION', null, new.conclusion);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'RECOMANDATION', null, new.recomendation);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ASSIGNED TO', null, new.assigned_to);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SIGNED FLAG', null, new.signed_flag);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SIGNED DATE', null, new.signed_date);

end if;

end;
//
create trigger work_flow_metrix_upd_trig after update on work_flow_metrix for each row
begin
if(new.audit_id is not null) then
if((new.module_step_id <> old.module_step_id) or (new.module_step_id is null and old.module_step_id is not null) or (new.module_step_id is not null and old.module_step_id is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'MODULE STEP', old.module_step_id, new.module_step_id);
end if;
if((new.work_flow_order <> old.work_flow_order) or (new.work_flow_order is null and old.work_flow_order is not null) or (new.work_flow_order is not null and old.work_flow_order is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'WORK FLOW ORDER', old.work_flow_order, new.work_flow_order);
end if;
if((new.corrective_action <> old.corrective_action) or (new.corrective_action is null and old.corrective_action is not null) or (new.corrective_action is not null and old.corrective_action is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CORRECTIVE ACTION', old.corrective_action, new.corrective_action);
end if;
if((new.conclusion <> old.conclusion) or (new.conclusion is null and old.conclusion is not null) or (new.conclusion is not null and old.conclusion is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CONCLUSION', old.conclusion, new.conclusion);
end if;
if((new.recomendation <> old.recomendation) or (new.recomendation is null and old.recomendation is not null) or (new.recomendation is not null and old.recomendation is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'RECOMANDATION', old.recomendation, new.recomendation);
end if;
if((new.assigned_to <> old.assigned_to) or (new.assigned_to is null and old.assigned_to is not null) or (new.assigned_to is not null and old.assigned_to is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ASSIGNED TO', old.assigned_to, new.assigned_to);
end if;
if((new.signed_flag <> old.signed_flag) or (new.signed_flag is null and old.signed_flag is not null) or (new.signed_flag is not null and old.signed_flag is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SIGNED FLAG', old.signed_flag, new.signed_flag);
end if;
if((new.signed_date <> old.signed_date) or (new.signed_date is null and old.signed_date is not null) or (new.signed_date is not null and old.signed_date is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SIGNED DATE', old.signed_date, new.signed_date);
end if;

end if;

end;
//
create trigger work_flow_metrix_del_trig after delete on work_flow_metrix for each row
begin
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'MODULE STEP', old.module_step_id, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'WORK FLOW ORDER', old.work_flow_order, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CORRECTIVE ACTION', old.corrective_action, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CONCLUSION', old.conclusion, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'RECOMANDATION', old.recomendation, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ASSIGNED TO', old.assigned_to, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SIGNED FLAG', old.signed_flag, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SIGNED DATE', old.signed_date, null);
end;
//

create trigger application_comments_ins_trig after insert on application_comments for each row
begin
if(new.audit_id is not null) then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', null, new.comments);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', null, new.updated_date);
end if;

end;
//
create trigger application_comments_upd_trig after update on application_comments for each row
begin
if(new.audit_id is not null) then
if((new.comments <> old.comments) or (new.comments is null and old.comments is not null) or (new.comments is not null and old.comments is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', old.comments, new.comments);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
end if;

end;
//
create trigger application_comments_del_trig after delete on application_comments for each row
begin
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'COMMENTS', old.comments, null);
insert into work_flow_metrix_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger application_scheduler_ins_trig after insert on application_scheduler for each row
begin
if(new.audit_id is not null) then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'FREQUENCY TYPE', null, new.frequency_type);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REPEAT ON', null, new.repeat_on);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REPEAT COUNT', null, new.repeat_count);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'START DATE', null, new.start_date);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'END DATE', null, new.end_date);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'START TIME', null, new.start_time);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'START TIME', null, new.end_time);
end if;

end;
//
create trigger application_scheduler_upd_trig after update on application_scheduler for each row
begin
if(new.audit_id is not null) then
if((new.frequency_type <> old.frequency_type) or (new.frequency_type is null and old.frequency_type is not null) or (new.frequency_type is not null and old.frequency_type is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'FREQUENCY TYPE', old.frequency_type, new.frequency_type);
end if;
if((new.repeat_on <> old.repeat_on) or (new.repeat_on is null and old.repeat_on is not null) or (new.repeat_on is not null and old.repeat_on is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REPEAT ON', old.repeat_on, new.repeat_on);
end if;
if((new.repeat_count <> old.repeat_count) or (new.repeat_count is null and old.repeat_count is not null) or (new.repeat_count is not null and old.repeat_count is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'REPEAT COUNT', old.repeat_count, new.repeat_count);
end if;
if((new.start_date <> old.start_date) or (new.start_date is null and old.start_date is not null) or (new.start_date is not null and old.start_date is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'START DATE', old.start_date, new.start_date);
end if;
if((new.end_date <> old.end_date) or (new.end_date is null and old.end_date is not null) or (new.end_date is not null and old.end_date is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'END DATE', old.end_date, new.end_date);
end if;
if((new.start_time <> old.start_time) or (new.start_time is null and old.start_time is not null) or (new.start_time is not null and old.start_time is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'START TIME', old.start_time, new.start_time);
end if;
if((new.end_time <> old.end_time) or (new.end_time is null and old.end_time is not null) or (new.end_time is not null and old.end_time is null))
then
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (new.audit_id, 'END TIME', old.end_time, new.end_time);
end if;
end if;

end;
//
create trigger application_scheduler_del_trig after delete on application_scheduler for each row
begin
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'FREQUENCY TYPE', old.frequency_type, null);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'REPEAT ON', old.repeat_on, null);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'REPEAT COUNT', old.repeat_count, null);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'START DATE', old.start_date, null);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'END DATE', old.end_date, null);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'START TIME', old.start_time, null);
insert into application_scheduler_log(audit_id, field, old_value, new_value) values (old.audit_id, 'START TIME', old.end_time, null);
end;
//

create trigger batch_details_ins_trig after insert on batch_details for each row
begin
if(new.audit_id is not null) then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH NUMBER', null, new.batch_number);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TITLE', null, new.batch_title);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', null, new.batch_desciption);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TYPE', null, new.batch_type);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_details_upd_trig after update on batch_details for each row
begin
if(new.audit_id is not null) then
if((new.batch_number <> old.batch_number) or (new.batch_number is null and old.batch_number is not null) or (new.batch_number is not null and old.batch_number is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH NUMBER', old.batch_number, new.batch_number);
end if;
if((new.batch_title <> old.batch_title) or (new.batch_title is null and old.batch_title is not null) or (new.batch_title is not null and old.batch_title is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TITLE', old.batch_title, new.batch_title);
end if;
if((new.batch_desciption <> old.batch_desciption) or (new.batch_desciption is null and old.batch_desciption is not null) or (new.batch_desciption is not null and old.batch_desciption is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', old.batch_desciption, new.batch_desciption);
end if;
if((new.batch_type <> old.batch_type) or (new.batch_type is null and old.batch_type is not null) or (new.batch_type is not null and old.batch_type is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TYPE', old.batch_type, new.batch_type);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into batch_details_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_details_del_trig after delete on batch_details for each row
begin
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH NUMBER', old.batch_number, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH TITLE', old.batch_title, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCRIPTION', old.batch_desciption, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH TYPE', old.batch_type, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into batch_details_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger batch_type_ins_trig after insert on batch_type for each row
begin
if(new.audit_id is not null) then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_type_upd_trig after update on batch_type for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into batch_type_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_type_del_trig after delete on batch_type for each row
begin
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into batch_type_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger batch_record_ins_trig after insert on batch_record for each row
begin
if(new.audit_id is not null) then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH DETAIL ID', null, new.batch_detail_id);
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS ID', null, new.status_id);
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOADED BY', null, new.uploaded_by);
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOADED DATE', null, new.uploaded_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_record_upd_trig after update on batch_record for each row
begin
if(new.audit_id is not null) then
if((new.batch_detail_id <> old.batch_detail_id) or (new.batch_detail_id is null and old.batch_detail_id is not null) or (new.batch_detail_id is not null and old.batch_detail_id is null))
then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH DETAIL ID', old.batch_detail_id, new.batch_detail_id);
end if;
if((new.status_id <> old.status_id) or (new.status_id is null and old.status_id is not null) or (new.status_id is not null and old.status_id is null))
then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS ID', old.status_id, new.status_id);
end if;
if((new.uploaded_date <> old.uploaded_date) or (new.uploaded_date is null and old.uploaded_date is not null) or (new.uploaded_date is not null and old.uploaded_date is null))
then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOADED DATE', old.uploaded_date, new.uploaded_date);
end if;
if((new.uploaded_by <> old.uploaded_by) or (new.uploaded_by is null and old.uploaded_by is not null) or (new.uploaded_by is not null and old.uploaded_by is null))
then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOADED BY', old.uploaded_by, new.uploaded_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_record_DEL_TRIG after delete on batch_record for each row
begin
insert into batch_record_log(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH DETAIL ID', old.batch_detail_id, null);
insert into batch_record_log(audit_id, field, old_value, new_value) values (old.audit_id, 'STATUS ID', old.status_id, null);
insert into batch_record_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPLOADED BY', old.uploaded_date, null);
insert into batch_record_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPLOADED DATE', old.uploaded_by, null);
end;
//

create trigger batch_team_ins_trig after insert on batch_team for each row
begin
if(new.audit_id is not null) then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH DETAIL ID', null, new.batch_detail_id);
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'MODULE ID', null, new.module_id);
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ROLE', null, new.role);
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ASSIGNED TO', null, new.assigned_to);
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_team_upd_trig after update on batch_team for each row
begin
if(new.audit_id is not null) then
if((new.batch_detail_id <> old.batch_detail_id) or (new.batch_detail_id is null and old.batch_detail_id is not null) or (new.batch_detail_id is not null and old.batch_detail_id is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH DETAIL ID', old.batch_detail_id, new.batch_detail_id);
end if;
if((new.module_id <> old.module_id) or (new.module_id is null and old.module_id is not null) or (new.module_id is not null and old.module_id is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'MODULE ID', old.module_id, new.module_id);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.role <> old.role) or (new.role is null and old.role is not null) or (new.role is not null and old.role is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ROLE', old.role, new.role);
end if;
if((new.assigned_to <> old.assigned_to) or (new.assigned_to is null and old.assigned_to is not null) or (new.assigned_to is not null and old.assigned_to is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'ASSIGNED TO', old.assigned_to, new.assigned_to);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into batch_team_log(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_team_del_trig after delete on batch_team for each row
begin
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH DETAIL ID', old.batch_detail_id, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'MODULE ID', old.module_id, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ROLE', old.role, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'ASSIGNED TO', old.assigned_to, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into batch_team_log(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger batch_record_sops_ins_trig after insert on batch_sops for each row
begin
if(new.audit_id is not null) then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH RECORD ID', null, new.batch_rec_id);
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP ID', null, new.sop_id);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_record_sops_upd_trig after update on batch_sops for each row
begin
if(new.audit_id is not null) then
if((new.batch_rec_id <> old.batch_rec_id) or (new.batch_rec_id is null and old.batch_rec_id is not null) or (new.batch_rec_id is not null and old.batch_rec_id is null))
then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH RECORD ID', old.batch_rec_id, new.batch_rec_id);
end if;
if((new.sop_id <> old.sop_id) or (new.sop_id is null and old.sop_id is not null) or (new.sop_id is not null and old.sop_id is null))
then
insert into batch_record_log(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP ID', old.sop_id, new.sop_id);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger batch_record_sops_del_trig after delete on batch_sops for each row
begin
insert into batch_record_log(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH RECORD ID', old.batch_rec_id, null);
insert into batch_record_log(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP ID', old.sop_id, null);
end;
//

DELIMITER ;



INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (1, 1, 13, 'CREATE', 1, sysdate());
INSERT INTO address(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (1, 2, 'Nizampet', NULL, 'Hyderabad', 'Telangana', '500090', 1);
INSERT INTO address(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (4, 2, 'Nizampet', NULL, 'Hyderabad', 'Telangana', '500090', 1);
INSERT INTO corporate(id, name, description, is_active, created_by, created_date, physical_address_id, invoice_address_id, audit_id) values (1, 'CIPLA', 'Pharma', 1, 1, sysdate(), 1, 4, 1);
UPDATE address set audit_id=NULL;
UPDATE corporate set audit_id=NULL;

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (2, 1, 14, 'CREATE', 1, sysdate());
INSERT INTO address(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (2, 2, 'Ameerpet', NULL, 'Hyderabad', 'Telangana', '500090', 2);
INSERT INTO departments(id, name, description, corporate_id, address_id, is_active, created_by, created_date, audit_id) values (1, 'QA', 'Pharma QA', 1, 2, 1, 1, sysdate(), 2);
UPDATE address set audit_id=NULL;
UPDATE departments set audit_id=NULL;

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (3, 1, 15, 'CREATE', 1, sysdate());
INSERT INTO address(id, country_id, address_line1, address_line2, city, state, pin, audit_id) values (3, 2, 'Ameerpet', NULL, 'Hyderabad', 'Telangana', '500090', 3);
INSERT INTO labs(id, name, description, department_id, address_id, is_active, created_by, created_date, audit_id) values (1, 'QA', 'Pharma QA', 1, 3, 1, 1, sysdate(), 3);
UPDATE address set audit_id=NULL;
UPDATE labs set audit_id=NULL;

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (4, 1, 5, 'CREATE', 1, sysdate());
INSERT INTO application_security_roles(id,audit_id, lab_id, created_by,created_date,description,is_active,name) values
(1, 4, 1, 1,sysdate(), 'Super Admin', 1, 'Super Admin');
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (3, 4, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (4, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (5, 4, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (6, 4, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (7, 4, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (8, 4, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (9, 4, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (10, 4, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (11, 4, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (12, 4, 12, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (13, 4, 13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (14, 4, 14, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (15, 4, 15, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
UPDATE application_security_roles set audit_id=NULL;
UPDATE application_security_roles_map set audit_id=NULL;

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (5, 1, 3, 'CREATE', 1, sysdate());
INSERT INTO application_user (id,audit_id,corporate_id,created_by,created_date,description,email,emp_id,first_name,is_active,is_temporary,last_login_date,last_logout_date,last_name,mobile_no,password,username) values 
(1,5,1,1,sysdate(),'SuperAdmin','aaaaaaaaaaaaaa@gmail.com',
'aaa11111','aaaaaaa',1,0,NULL,NULL,
'aaaaaaaaa','9878954578','password1','aaa11111');
INSERT INTO application_user_roles(id, audit_id, user_id, security_role_id) values (1, 5, 1, 1);
UPDATE application_user set audit_id=NULL;

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (153, 1, 3, 'CREATE', 1, sysdate());
INSERT INTO application_user (id,audit_id,corporate_id,created_by,created_date,description,email,emp_id,first_name,is_active,is_temporary,last_login_date,last_logout_date,last_name,mobile_no,password,username) values 
(5,5,1,1,sysdate(),'SuperAdmin','aaaaaaaaaaaaaa@gmail.com',
'aaa55555','aaa55555',1,0,NULL,NULL,
'aaaaaaaaa','9878954578','password1','aaa55555');
INSERT INTO application_user_roles(id, audit_id, user_id, security_role_id) values (1, 153, 5, 1);
UPDATE application_user set audit_id=NULL;

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (21, 2, 5, 'CREATE', 1, sysdate());
INSERT INTO application_security_roles(id,audit_id, lab_id, created_by,created_date,description,is_active,name) values
(2, 21, 1, 1,sysdate(), 'Lab Technician Author', 1, 'Lab Technician Author');
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (16, 21, 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (17, 21, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (22, 2, 5, 'CREATE', 1, sysdate());
INSERT INTO application_security_roles(id,audit_id, lab_id, created_by,created_date,description,is_active,name) values
(3, 22, 1, 1,sysdate(), 'Lab Technician Reviewer', 1, 'Lab Technician Reviewer');
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (18, 22, 1, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (19, 22, 2, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (23, 2, 5, 'CREATE', 1, sysdate());
INSERT INTO application_security_roles(id,audit_id, lab_id, created_by,created_date,description,is_active,name) values
(4, 23, 1, 1,sysdate(), 'Lab Technician Approver', 1, 'Lab Technician Approver');
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (20, 23, 1, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1);
INSERT INTO application_security_roles_map(id, audit_id, module_id, security_id, create_flag, edit_flag, view_flag, archive_flag, delete_flag, print_flag, run_flag, upload_flag, share_flag, author_flag, reviewer_flag, approver_flag) values (21, 23, 2, 4, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);


INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (24, 1, 3, 'CREATE', 1, sysdate());
INSERT INTO application_user (id,audit_id,corporate_id,created_by,created_date,description,email,emp_id,first_name,is_active,is_temporary,last_login_date,last_logout_date,last_name,mobile_no,password,username) values 
(2,24,1,1,sysdate(),'Technician','dddddddddddddddddd@gmail.com',
'ddd11111','dddddddd',1,0,NULL,NULL,
'dddddddd','8008801145','password1','ddd11111');
INSERT INTO application_user_roles(id, audit_id, user_id, security_role_id) values (2, 24, 2, 2);

INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (25, 1, 3, 'CREATE', 1, sysdate());
INSERT INTO application_user (id,audit_id,corporate_id,created_by,created_date,description,email,emp_id,first_name,is_active,is_temporary,last_login_date,last_logout_date,last_name,mobile_no,password,username) values 
(3,25,1,1,sysdate(),'Technician','bbbbbbbbbbbbbbbbb@gmail.com',
'bbb11111','bbbbbbbb',1,0,NULL,NULL,
'bbbbbbbb','8008801145','password1','bbb11111');
INSERT INTO application_user_roles(id, audit_id, user_id, security_role_id) values (3, 25, 3, 3);


INSERT INTO audit_log(id, entity_id, module_id, action_type, created_by, created_date) values (26, 1, 3, 'CREATE', 1, sysdate());
INSERT INTO application_user (id,audit_id,corporate_id,created_by,created_date,description,email,emp_id,first_name,is_active,is_temporary,last_login_date,last_logout_date,last_name,mobile_no,password,username) values 
(4,26,1,1,sysdate(),'Technician','cccccccccccccccc@gmail.com',
'ccc11111','cccccccc',1,0,NULL,NULL,
'ccccccc','8008801145','password1','ccc11111');
INSERT INTO application_user_roles(id, audit_id, user_id, security_role_id) values (4, 25, 3, 3);

UPDATE application_user set audit_id=NULL;




ALTER TABLE countries ADD CONSTRAINT FK_CTRY_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE countries ADD CONSTRAINT FK_CTRY_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_status ADD CONSTRAINT FK_APPL_STS_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_status ADD CONSTRAINT FK_APPL_STS_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_document_types ADD CONSTRAINT FK_APPL_DOC_TYP_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_document_types ADD CONSTRAINT FK_APPL_DOC_TYP_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_main_modules ADD CONSTRAINT FK_APPL_MMOD_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_main_modules ADD CONSTRAINT FK_APPL_MMOD_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_modules ADD CONSTRAINT FK_APPL_MOD_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_modules ADD CONSTRAINT FK_APPL_MOD_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_roles ADD CONSTRAINT FK_APPL_ROLE_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_roles ADD CONSTRAINT FK_APPL_ROLE_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE audit_log ADD CONSTRAINT FK_AUD_LOG_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);

ALTER TABLE corporate ADD CONSTRAINT FK_CORP_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE corporate ADD CONSTRAINT FK_CORP_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE departments ADD CONSTRAINT FK_DEPT_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE departments ADD CONSTRAINT FK_DEPT_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE labs ADD CONSTRAINT FK_LAB_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE labs ADD CONSTRAINT FK_LAB_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_security_roles ADD CONSTRAINT FK_APP_SEC_ROLE_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_security_roles ADD CONSTRAINT FK_APP_SEC_ROLE_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_user ADD CONSTRAINT FK_APP_USER_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);
ALTER TABLE application_user ADD CONSTRAINT FK_APP_USER_updated_BY FOREIGN KEY (updated_BY) REFERENCES application_user (ID);

ALTER TABLE application_module_steps ADD CONSTRAINT FK_APP_MOD_STEPS_CREATED_BY FOREIGN KEY (CREATED_BY) REFERENCES application_user (ID);


INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (8, 'INITIATION', 'Initiation', 7, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (9, 'INVESTIGATION', 'Investigation', 7, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (10, 'APPROVAL', 'Approval', 7, 1, sysdate());

INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (11, 'INITIATION', 'Initiation', 9, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (12, 'INVESTIGATION', 'Investigation', 9, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (13, 'REVIEW', 'In Review', 9, 1, sysdate());
INSERT into application_status (ID, enum_key, name, module_id, created_by, created_date) VALUES (14, 'APPROVAL', 'Approval', 9, 1, sysdate());

insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(7, 'INITIATION', 'Initiation', 1, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(7, 'INVESTIGATION', 'Investigation', 2, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(7, 'APPROVAL', 'Approval', 3, 1, 1, 1, sysdate());

insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(9, 'INITIATION', 'Initiation', 1, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(9, 'INVESTIGATION', 'Investigation', 2, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(9, 'REVIEW', 'Review', 3, 1, 1, 1, sysdate());
insert into application_module_steps(module_id, step_key, step_desc, step_order, enabled, clickable, created_by, created_date) values(9, 'APPROVAL', 'Approval', 4, 1, 1, 1, sysdate());

ALTER TABLE sop
ADD UNIQUE (name);

ALTER TABLE sop_type
ADD UNIQUE (name);