DELIMITER //

create trigger ADDRESS_INS_TRIG after insert on ADDRESS for each row
begin
if(new.audit_id is not null) then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'COUNTRY_ID', null, new.country_id);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS_LINE1', null, new.address_line1);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS_LINE2', null, new.address_line2);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CITY', null, new.city);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'STATE', null, new.state);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PIN', null, new.pin);
end if;

end;
//
create trigger ADDRESS_UPD_TRIG after update on ADDRESS for each row
begin
if(new.audit_id is not null) then
if((new.country_id <> old.country_id) or (new.country_id is null and old.country_id is not null) or (new.country_id is not null and old.country_id is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'COUNTRY_ID', old.country_id, new.country_id);
end if;
if((new.address_line1 <> old.address_line1) or (new.address_line1 is null and old.address_line1 is not null) or (new.address_line1 is not null and old.address_line1 is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE1', old.address_line1, new.address_line1);
end if;
if((new.address_line2 <> old.address_line2) or (new.address_line2 is null and old.address_line2 is not null) or (new.address_line2 is not null and old.address_line2 is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE2', old.address_line2, new.address_line1);
end if;
if((new.city <> old.city) or (new.city is null and old.city is not null) or (new.city is not null and old.city is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CITY', old.city, new.city);
end if;
if((new.state <> old.state) or (new.state is null and old.state is not null) or (new.state is not null and old.state is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'STATE', old.state, new.state);
end if;
if((new.pin <> old.pin) or (new.pin is null and old.pin is not null) or (new.pin is not null and old.pin is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PIN', old.pin, new.pin);
end if;
end if;

end;
//
create trigger ADDRESS_DEL_TRIG after delete on ADDRESS for each row
begin
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'COUNTRY_ID', old.country_id, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE1', old.address_line1, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE2', old.address_line2, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CITY', old.city, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'STATE', old.state, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PIN', old.pin, null);
end;
//


create trigger CROPORATE_INS_TRIG after insert on CORPORATE for each row
begin
if(new.audit_id is not null) then
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PHYSICAL ADDRESS ID', null, new.physical_address_id);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'INVOICE ADDRESS ID', null, new.invoice_address_id);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger CROPORATE_UPD_TRIG after update on CORPORATE for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger CROPORATE_DEL_TRIG after delete on CORPORATE for each row
begin
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PHYSICAL ADDRESS ID', old.physical_address_id, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'INVOICE ADDRESS ID', old.invoice_address_id, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into CORPORATE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//


create trigger DEPARTMENT_INS_TRIG after insert on DEPARTMENTS for each row
begin
if(new.audit_id is not null) then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', null, new.corporate_id);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS ID', null, new.address_id);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger DEPARTMENT_UPD_TRIG after update on DEPARTMENTS for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.corporate_id <> old.corporate_id) or (new.corporate_id is null and old.corporate_id is not null) or (new.corporate_id is not null and old.corporate_id is null))
then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', old.corporate_id, new.corporate_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//

create trigger DEPARTMENT_DEL_TRIG after update on DEPARTMENTS for each row
begin
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CORPORATE ID', old.corporate_id, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS ID', old.address_id, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into DEPARTMENTS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);

end;
//


create trigger LAB_INS_TRIG after insert on LABS for each row
begin
if(new.audit_id is not null) then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DEPARTMENT ID', null, new.department_id);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS ID', null, new.address_id);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger LAB_UPD_TRIG after update on LABS for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.department_id <> old.department_id) or (new.department_id is null and old.department_id is not null) or (new.department_id is not null and old.department_id is null))
then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DEPARTMENT ID', old.department_id, new.department_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into LABS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger LAB_DEL_TRIG after delete on LABS for each row
begin
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DEPARTMENT ID', old.department_id, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS ID', old.address_id, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into LABS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//


create trigger SECURITY_ROLE_INS_TRIG after insert on APPLICATION_SECURITY_ROLES for each row
begin
if(new.audit_id is not null) then
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SECURITY_ROLE_UPD_TRIG after update on APPLICATION_SECURITY_ROLES for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SECURITY_ROLE_DEL_TRIG after delete on APPLICATION_SECURITY_ROLES for each row
begin
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into SECURITY_ROLE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger APPLICATION_USER_INS_TRIG after insert on APPLICATION_USER for each row
begin
if(new.audit_id is not null) then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'FIRST NAME', null, new.first_name);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAST NAME', null, new.last_name);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'USERNAME', null, new.username);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PASSWORD', null, new.password);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMPLOYEE ID', null, new.emp_id);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMAIL', null, new.email);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'MOBILE No.', null, new.mobile_no);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', null, new.corporate_id);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//

create trigger APPLICATION_USER_UPD_TRIG after update on APPLICATION_USER for each row
begin
if(new.audit_id is not null) then
if((new.first_name <> old.first_name) or (new.first_name is null and old.first_name is not null) or (new.first_name is not null and old.first_name is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'FIRST NAME', old.first_name, new.first_name);
end if;
if((new.last_name <> old.last_name) or (new.last_name is null and old.last_name is not null) or (new.last_name is not null and old.last_name is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAST NAME', old.last_name, new.last_name);
end if;
if((new.username <> old.username) or (new.username is null and old.username is not null) or (new.username is not null and old.username is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'USERNAME', old.username, new.username);
end if;
if((new.password <> old.password) or (new.password is null and old.password is not null) or (new.password is not null and old.password is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PASSWORD', old.password, new.password);
end if;
if((new.emp_id <> old.emp_id) or (new.emp_id is null and old.emp_id is not null) or (new.emp_id is not null and old.emp_id is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMPLOYEE ID', old.emp_id, new.emp_id);
end if;
if((new.email <> old.email) or (new.email is null and old.email is not null) or (new.email is not null and old.email is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMAIL', old.email, new.email);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.mobile_no <> old.mobile_no) or (new.mobile_no is null and old.mobile_no is not null) or (new.mobile_no is not null and old.mobile_no is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'MOBILE No.', old.mobile_no, new.mobile_no);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.corporate_id <> old.corporate_id) or (new.corporate_id is null and old.corporate_id is not null) or (new.corporate_id is not null and old.corporate_id is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', old.corporate_id, new.corporate_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger APPLICATION_USER_DEL_TRIG after update on APPLICATION_USER for each row
begin
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'FIRST NAME', old.first_name, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAST NAME', old.last_name, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'USERNAME', old.username, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PASSWORD', old.password, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'EMPLOYEE ID', old.emp_id, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'EMAIL', old.email, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'MOBILE No.', old.mobile_no, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CORPORATE ID', old.corporate_id, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);

end;
//

create trigger SECURITY_ROLE_MAP_INS_TRIG after insert on APPLICATION_SECURITY_ROLES_MAP for each row
begin
if(new.audit_id is not null) then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SECURITY ID', null, new.security_id);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATE FLAG', null, new.create_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EDIT FLAG', null, new.edit_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ARCHIVE FLAG', null, new.archive_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VIEW FLAG', null, new.view_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DELETE FLAG', null, new.delete_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PRINT FLAG', null, new.print_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'RUN FLAG', null, new.run_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOAD FLAG', null, new.upload_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SHARE FLAG', null, new.share_flag);

end if;
end;
//
create trigger SECURITY_ROLE_MAP_UPD_TRIG after update on APPLICATION_SECURITY_ROLES_MAP for each row
begin
if(new.audit_id is not null) then
if((new.security_id <> old.security_id) or (new.security_id is null and old.security_id is not null) or (new.security_id is not null and old.security_id is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SECURITY ID', old.security_id, new.security_id);
end if;
if((new.create_flag <> old.create_flag) or (new.create_flag is null and old.create_flag is not null) or (new.create_flag is not null and old.create_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATE FLAG', old.create_flag, new.create_flag);
end if;
if((new.edit_flag <> old.edit_flag) or (new.edit_flag is null and old.edit_flag is not null) or (new.edit_flag is not null and old.edit_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EDIT FLAG', old.edit_flag, new.edit_flag);
end if;
if((new.view_flag <> old.view_flag) or (new.view_flag is null and old.view_flag is not null) or (new.view_flag is not null and old.view_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VIEW FLAG', old.view_flag, new.view_flag);
end if;
if((new.archive_flag <> old.archive_flag) or (new.archive_flag is null and old.archive_flag is not null) or (new.archive_flag is not null and old.archive_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ARCHIVE FLAG', old.archive_flag, new.archive_flag);
end if;
if((new.delete_flag <> old.delete_flag) or (new.delete_flag is null and old.delete_flag is not null) or (new.delete_flag is not null and old.delete_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DELETE FLAG', old.delete_flag, new.delete_flag);
end if;
if((new.print_flag <> old.print_flag) or (new.print_flag is null and old.print_flag is not null) or (new.print_flag is not null and old.print_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PRINT FLAG', old.print_flag, new.print_flag);
end if;
if((new.run_flag <> old.run_flag) or (new.run_flag is null and old.run_flag is not null) or (new.run_flag is not null and old.run_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'RUN FLAG', old.run_flag, new.run_flag);
end if;
if((new.upload_flag <> old.upload_flag) or (new.upload_flag is null and old.upload_flag is not null) or (new.upload_flag is not null and old.upload_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOAD FLAG', old.upload_flag, new.upload_flag);
end if;
if((new.share_flag <> old.share_flag) or (new.share_flag is null and old.share_flag is not null) or (new.share_flag is not null and old.share_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SHARE FLAG', old.share_flag, new.share_flag);
end if;
end if;
end;
//
create trigger SECURITY_ROLE_MAP_DEL_TRIG after update on APPLICATION_SECURITY_ROLES_MAP for each row
begin
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SECURITY ID', old.security_id, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATE FLAG', old.create_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'EDIT FLAG', old.edit_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ARCHIVE FLAG', old.archive_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'VIEW FLAG', old.view_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DELET FLAG', old.delete_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PRINT FLAG', old.print_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'RUN FLAG', old.run_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPLOAD FLAG', old.upload_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SHARE FLAG', old.share_flag, null);

end;
//

create trigger SOP_TYPE_INS_TRIG after insert on SOP_TYPE for each row
begin
if(new.audit_id is not null) then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_TYPE_UPD_TRIG after update on SOP_TYPE for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_TYPE_DEL_TRIG after delete on SOP_TYPE for each row
begin
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger SOP_INS_TRIG after insert on SOP for each row
begin
if(new.audit_id is not null) then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP NUMBER', null, new.sop_number);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TITLE', null, new.sop_title);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', null, new.sop_desciption);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TYPE', null, new.sop_type_id);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VERSION', null, new.version);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'REVISION', null, new.revision);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', null, new.comments);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS', null, new.status_id);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_UPD_TRIG after update on SOP for each row
begin
if(new.audit_id is not null) then
if((new.sop_number <> old.sop_number) or (new.sop_number is null and old.sop_number is not null) or (new.sop_number is not null and old.sop_number is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP NUMBER', old.sop_number, new.sop_number);
end if;
if((new.sop_title <> old.sop_title) or (new.sop_title is null and old.sop_title is not null) or (new.sop_title is not null and old.sop_title is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TITLE', old.sop_title, new.sop_title);
end if;
if((new.sop_desciption <> old.sop_desciption) or (new.sop_desciption is null and old.sop_desciption is not null) or (new.sop_desciption is not null and old.sop_desciption is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', old.sop_desciption, new.sop_desciption);
end if;
if((new.sop_type_id <> old.sop_type_id) or (new.sop_type_id is null and old.sop_type_id is not null) or (new.sop_type_id is not null and old.sop_type_id is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TYPE', old.sop_type_id, new.sop_type_id);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.version <> old.version) or (new.version is null and old.version is not null) or (new.version is not null and old.version is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VERSION', old.version, new.version);
end if;
if((new.revision <> old.revision) or (new.revision is null and old.revision is not null) or (new.revision is not null and old.revision is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'REVISION', old.revision, new.revision);
end if;
if((new.comments <> old.comments) or (new.comments is null and old.comments is not null) or (new.comments is not null and old.comments is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', old.comments, new.comments);
end if;
if((new.status_id <> old.status_id) or (new.status_id is null and old.status_id is not null) or (new.status_id is not null and old.status_id is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS', old.status_id, new.status_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_DEL_TRIG after delete on SOP for each row
begin
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP NUMBER', old.sop_number, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP TITLE', old.sop_title, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCRIPTION', old.sop_desciption, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP TYPE', old.sop_type_id, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'VERSION', old.version, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'REVISION', old.revision, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'COMMENTS', old.comments, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'STATUS', old.status_id, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//

create trigger BATCH_TYPE_INS_TRIG after insert on BATCH_TYPE for each row
begin
if(new.audit_id is not null) then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger BATCH_TYPE_UPD_TRIG after update on BATCH_TYPE for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger BATCH_TYPE_DEL_TRIG after delete on BATCH_TYPE for each row
begin
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into BATCH_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//


create trigger BATCH_DETAILS_INS_TRIG after insert on BATCH_DETAILS for each row
begin
if(new.audit_id is not null) then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH NUMBER', null, new.batch_number);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TITLE', null, new.batch_title);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', null, new.batch_desciption);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TYPE', null, new.batch_type);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger BATCH_DETAILS_UPD_TRIG after update on BATCH_DETAILS for each row
begin
if(new.audit_id is not null) then
if((new.batch_number <> old.batch_number) or (new.batch_number is null and old.batch_number is not null) or (new.batch_number is not null and old.batch_number is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH NUMBER', old.batch_number, new.batch_number);
end if;
if((new.batch_title <> old.batch_title) or (new.batch_title is null and old.batch_title is not null) or (new.batch_title is not null and old.batch_title is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TITLE', old.batch_title, new.batch_title);
end if;
if((new.batch_desciption <> old.batch_desciption) or (new.batch_desciption is null and old.batch_desciption is not null) or (new.batch_desciption is not null and old.batch_desciption is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', old.batch_desciption, new.batch_desciption);
end if;
if((new.batch_type <> old.batch_type) or (new.batch_type is null and old.batch_type is not null) or (new.batch_type is not null and old.batch_type is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'BATCH TYPE', old.batch_type, new.batch_type);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update AUDIT_LOG set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger BATCH_DETAILS_DEL_TRIG after delete on BATCH_DETAILS for each row
begin
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH NUMBER', old.batch_number, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH TITLE', old.batch_title, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCRIPTION', old.batch_desciption, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'BATCH TYPE', old.batch_type, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into BATCH_DETAILS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//