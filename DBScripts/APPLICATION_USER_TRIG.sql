create trigger APPLICATION_USER_INS_TRIG after insert on APPLICATION_USER for each row
begin
if(new.audit_id is not null) then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'FIRST NAME', null, new.first_name);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAST NAME', null, new.last_name);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'USERNAME', null, new.username);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PASSWORD', null, new.password);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMPLOYEE ID', null, new.emp_id);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMAIL', null, new.email);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'MOBILE No.', null, new.mobile_no);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', null, new.corporate_id);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//

create trigger APPLICATION_USER_UPD_TRIG after update on APPLICATION_USER for each row
begin
if(new.audit_id is not null) then
if((new.first_name <> old.first_name) or (new.first_name is null and old.first_name is not null) or (new.first_name is not null and old.first_name is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'FIRST NAME', old.first_name, new.first_name);
end if;
if((new.last_name <> old.last_name) or (new.last_name is null and old.last_name is not null) or (new.last_name is not null and old.last_name is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAST NAME', old.last_name, new.last_name);
end if;
if((new.username <> old.username) or (new.username is null and old.username is not null) or (new.username is not null and old.username is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'USERNAME', old.username, new.username);
end if;
if((new.password <> old.password) or (new.password is null and old.password is not null) or (new.password is not null and old.password is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PASSWORD', old.password, new.password);
end if;
if((new.emp_id <> old.emp_id) or (new.emp_id is null and old.emp_id is not null) or (new.emp_id is not null and old.emp_id is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMPLOYEE ID', old.emp_id, new.emp_id);
end if;
if((new.email <> old.email) or (new.email is null and old.email is not null) or (new.email is not null and old.email is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EMAIL', old.email, new.email);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.mobile_no <> old.mobile_no) or (new.mobile_no is null and old.mobile_no is not null) or (new.mobile_no is not null and old.mobile_no is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'MOBILE No.', old.mobile_no, new.mobile_no);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.corporate_id <> old.corporate_id) or (new.corporate_id is null and old.corporate_id is not null) or (new.corporate_id is not null and old.corporate_id is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CORPORATE ID', old.corporate_id, new.corporate_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger APPLICATION_USER_DEL_TRIG after update on APPLICATION_USER for each row
begin
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'FIRST NAME', old.first_name, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAST NAME', old.last_name, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'USERNAME', old.username, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PASSWORD', old.password, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'EMPLOYEE ID', old.emp_id, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'EMAIL', old.email, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'MOBILE No.', old.mobile_no, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CORPORATE ID', old.corporate_id, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into APPLICATION_USER_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);

end;
//
