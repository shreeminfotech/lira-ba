create trigger SOP_INS_TRIG after insert on SOP for each row
begin
if(new.audit_id is not null) then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP NUMBER', null, new.sop_number);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TITLE', null, new.sop_title);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', null, new.sop_desciption);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TYPE', null, new.sop_type_id);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VERSION', null, new.version);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'REVISION', null, new.revision);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', null, new.comments);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS', null, new.status_id);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_UPD_TRIG after update on SOP for each row
begin
if(new.audit_id is not null) then
if((new.sop_number <> old.sop_number) or (new.sop_number is null and old.sop_number is not null) or (new.sop_number is not null and old.sop_number is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP NUMBER', old.sop_number, new.sop_number);
end if;
if((new.sop_title <> old.sop_title) or (new.sop_title is null and old.sop_title is not null) or (new.sop_title is not null and old.sop_title is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TITLE', old.sop_title, new.sop_title);
end if;
if((new.sop_desciption <> old.sop_desciption) or (new.sop_desciption is null and old.sop_desciption is not null) or (new.sop_desciption is not null and old.sop_desciption is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCRIPTION', old.sop_desciption, new.sop_desciption);
end if;
if((new.sop_type_id <> old.sop_type_id) or (new.sop_type_id is null and old.sop_type_id is not null) or (new.sop_type_id is not null and old.sop_type_id is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SOP TYPE', old.sop_type_id, new.sop_type_id);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.version <> old.version) or (new.version is null and old.version is not null) or (new.version is not null and old.version is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VERSION', old.version, new.version);
end if;
if((new.revision <> old.revision) or (new.revision is null and old.revision is not null) or (new.revision is not null and old.revision is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'REVISION', old.revision, new.revision);
end if;
if((new.comments <> old.comments) or (new.comments is null and old.comments is not null) or (new.comments is not null and old.comments is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'COMMENTS', old.comments, new.comments);
end if;
if((new.status_id <> old.status_id) or (new.status_id is null and old.status_id is not null) or (new.status_id is not null and old.status_id is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'STATUS', old.status_id, new.status_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into SOP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_DEL_TRIG after delete on SOP for each row
begin
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP NUMBER', old.sop_number, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP TITLE', old.sop_title, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCRIPTION', old.sop_desciption, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SOP TYPE', old.sop_type_id, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'VERSION', old.version, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'REVISION', old.revision, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'COMMENTS', old.comments, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'STATUS', old.status_id, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into SOP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//
