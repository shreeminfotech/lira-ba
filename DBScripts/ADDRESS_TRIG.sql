create trigger ADDRESS_INS_TRIG after insert on ADDRESS for each row
begin
if(new.audit_id is not null) then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'COUNTRY_ID', null, new.country_id);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS_LINE1', null, new.address_line1);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ADDRESS_LINE2', null, new.address_line2);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CITY', null, new.city);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'STATE', null, new.state);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PIN', null, new.pin);
end if;

end;
//
create trigger ADDRESS_UPD_TRIG after update on ADDRESS for each row
begin
if(new.audit_id is not null) then
if((new.country_id <> old.country_id) or (new.country_id is null and old.country_id is not null) or (new.country_id is not null and old.country_id is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'COUNTRY_ID', old.country_id, new.country_id);
end if;
if((new.address_line1 <> old.address_line1) or (new.address_line1 is null and old.address_line1 is not null) or (new.address_line1 is not null and old.address_line1 is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE1', old.address_line1, new.address_line1);
end if;
if((new.address_line2 <> old.address_line2) or (new.address_line2 is null and old.address_line2 is not null) or (new.address_line2 is not null and old.address_line2 is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE2', old.address_line2, new.address_line1);
end if;
if((new.city <> old.city) or (new.city is null and old.city is not null) or (new.city is not null and old.city is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CITY', old.city, new.city);
end if;
if((new.state <> old.state) or (new.state is null and old.state is not null) or (new.state is not null and old.state is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'STATE', old.state, new.state);
end if;
if((new.pin <> old.pin) or (new.pin is null and old.pin is not null) or (new.pin is not null and old.pin is null))
then
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PIN', old.pin, new.pin);
end if;
end if;

end;
//
create trigger ADDRESS_DEL_TRIG after delete on ADDRESS for each row
begin
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'COUNTRY_ID', old.country_id, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE1', old.address_line1, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ADDRESS_LINE2', old.address_line2, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CITY', old.city, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'STATE', old.state, null);
insert into ADDRESS_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PIN', old.pin, null);
end;
//