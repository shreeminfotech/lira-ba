create trigger SOP_TYPE_INS_TRIG after insert on SOP_TYPE for each row
begin
if(new.audit_id is not null) then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', null, new.name);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', null, new.description);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', null, new.is_active);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', null, new.lab_id);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED BY', null, new.created_by);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATED DATE', null, new.created_date);
update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_TYPE_UPD_TRIG after update on SOP_TYPE for each row
begin
if(new.audit_id is not null) then
if((new.name <> old.name) or (new.name is null and old.name is not null) or (new.name is not null and old.name is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'NAME', old.name, new.name);
end if;
if((new.description <> old.description) or (new.description is null and old.description is not null) or (new.description is not null and old.description is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DESCIPTION', old.description, new.description);
end if;
if((new.is_active <> old.is_active) or (new.is_active is null and old.is_active is not null) or (new.is_active is not null and old.is_active is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ACTIVE FLAG', old.is_active, new.is_active);
end if;
if((new.lab_id <> old.lab_id) or (new.lab_id is null and old.lab_id is not null) or (new.lab_id is not null and old.lab_id is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'LAB ID', old.lab_id, new.lab_id);
end if;
if((new.updated_date <> old.updated_date) or (new.updated_date is null and old.updated_date is not null) or (new.updated_date is not null and old.updated_date is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED DATE', old.updated_date, new.updated_date);
end if;
if((new.updated_by <> old.updated_by) or (new.updated_by is null and old.updated_by is not null) or (new.updated_by is not null and old.updated_by is null))
then
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPDATED BY', old.updated_by, new.updated_by);
end if;

update audit_log set entity_id=new.id where id=new.audit_id;
end if;

end;
//
create trigger SOP_TYPE_DEL_TRIG after delete on SOP_TYPE for each row
begin
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'NAME', old.name, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DESCIPTION', old.description, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ACTIVE FLAG', old.is_active, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'LAB ID', old.lab_id, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED BY', old.created_by, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATED DATE', old.created_date, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED BY', old.updated_by, null);
insert into SOP_TYPE_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPDATED DATE', old.updated_date, null);
end;
//