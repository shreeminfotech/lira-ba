create trigger SECURITY_ROLE_MAP_INS_TRIG after insert on APPLICATION_SECURITY_ROLES_MAP for each row
begin
if(new.audit_id is not null) then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SECURITY ID', null, new.security_id);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATE FLAG', null, new.create_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EDIT FLAG', null, new.edit_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ARCHIVE FLAG', null, new.archive_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VIEW FLAG', null, new.view_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DELETE FLAG', null, new.delete_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PRINT FLAG', null, new.print_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'RUN FLAG', null, new.run_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOAD FLAG', null, new.upload_flag);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SHARE FLAG', null, new.share_flag);

end if;
end;
//
create trigger SECURITY_ROLE_MAP_UPD_TRIG after update on APPLICATION_SECURITY_ROLES_MAP for each row
begin
if(new.audit_id is not null) then
if((new.security_id <> old.security_id) or (new.security_id is null and old.security_id is not null) or (new.security_id is not null and old.security_id is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SECURITY ID', old.security_id, new.security_id);
end if;
if((new.create_flag <> old.create_flag) or (new.create_flag is null and old.create_flag is not null) or (new.create_flag is not null and old.create_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'CREATE FLAG', old.create_flag, new.create_flag);
end if;
if((new.edit_flag <> old.edit_flag) or (new.edit_flag is null and old.edit_flag is not null) or (new.edit_flag is not null and old.edit_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'EDIT FLAG', old.edit_flag, new.edit_flag);
end if;
if((new.view_flag <> old.view_flag) or (new.view_flag is null and old.view_flag is not null) or (new.view_flag is not null and old.view_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'VIEW FLAG', old.view_flag, new.view_flag);
end if;
if((new.archive_flag <> old.archive_flag) or (new.archive_flag is null and old.archive_flag is not null) or (new.archive_flag is not null and old.archive_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'ARCHIVE FLAG', old.archive_flag, new.archive_flag);
end if;
if((new.delete_flag <> old.delete_flag) or (new.delete_flag is null and old.delete_flag is not null) or (new.delete_flag is not null and old.delete_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'DELETE FLAG', old.delete_flag, new.delete_flag);
end if;
if((new.print_flag <> old.print_flag) or (new.print_flag is null and old.print_flag is not null) or (new.print_flag is not null and old.print_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'PRINT FLAG', old.print_flag, new.print_flag);
end if;
if((new.run_flag <> old.run_flag) or (new.run_flag is null and old.run_flag is not null) or (new.run_flag is not null and old.run_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'RUN FLAG', old.run_flag, new.run_flag);
end if;
if((new.upload_flag <> old.upload_flag) or (new.upload_flag is null and old.upload_flag is not null) or (new.upload_flag is not null and old.upload_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'UPLOAD FLAG', old.upload_flag, new.upload_flag);
end if;
if((new.share_flag <> old.share_flag) or (new.share_flag is null and old.share_flag is not null) or (new.share_flag is not null and old.share_flag is null))
then
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (new.audit_id, 'SHARE FLAG', old.share_flag, new.share_flag);
end if;
end if;
end;
//
create trigger SECURITY_ROLE_MAP_DEL_TRIG after update on APPLICATION_SECURITY_ROLES_MAP for each row
begin
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SECURITY ID', old.security_id, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'CREATE FLAG', old.create_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'EDIT FLAG', old.edit_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'ARCHIVE FLAG', old.archive_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'VIEW FLAG', old.view_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'DELET FLAG', old.delete_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'PRINT FLAG', old.print_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'RUN FLAG', old.run_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'UPLOAD FLAG', old.upload_flag, null);
insert into SECURITY_ROLE_MAP_LOG(audit_id, field, old_value, new_value) values (old.audit_id, 'SHARE FLAG', old.share_flag, null);

end;
//