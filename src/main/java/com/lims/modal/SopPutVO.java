package com.lims.modal;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SopPutVO {
	
	private Integer status;
	
	@NotEmpty(message = "Name is required")
	private String name;
	@NotEmpty(message = "Number is required")
	private String number;
	//private String description;

	//private Integer updatedBy;
	
	private SelectInputVO typeId;
	
	private Boolean signedFlag;
	private Integer signedBy;
	private String signedByName;
	
	private Boolean rejected;
	
	private List<DocumentVO> document;
	
	private List<SelectInputVO> author;
	private List<SelectInputVO> reviewer;
	private List<SelectInputVO> firstApprover;
	private List<SelectInputVO> secondApprover;
	
	private String comments;
	
	private SchedulerVO triggerData;
	
	private String version;
	private String revision;
	
	private Integer currentStep;
	private Integer completedStep;
	private Boolean initiatorFlag;
}
