package com.lims.modal;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SopGetVO extends Response{
	
	private int id;
	private int labId;
	private String name;
	private String number;
	private String labName;
	private String description;
	private SelectInputVO typeId;
	private Boolean isActive;
	
	private Integer currentStep;
	private Integer completedStep;
	

	private Integer createdBy;
	private Integer updatedBy;
	
	private String createdByUserName;
	private String updatedByUserName;
	
	private Date createdDate;
	private Date updatedDate;
	
	private Integer status;
	private String statusName;
	
	private Integer stepId;
	private String stepKey;
	
	private List<WorkflowVO> workflowList;
	
	
	private List<DocumentVO> document;
	
	private String revision;
	private String version;
	
	private SchedulerVO triggerData;
	
	private List<SelectInputVO> author;
	private List<SelectInputVO> reviewer;
	private List<SelectInputVO> firstApprover;
	private List<SelectInputVO> secondApprover;
	
	private Integer signedBy;
	private String signedByName;
	private Date signedDate;
	private Boolean signedFlag;
	
	private String comments;
	
	private Boolean initiatorFlag;
	
	private boolean editableFlag;
	
	private List<ApproversVO> approverList;
}
