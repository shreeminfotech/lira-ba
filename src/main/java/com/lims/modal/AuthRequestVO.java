package com.lims.modal;

import com.lims.enums.EntityTypes;
import com.lims.enums.StepKeys;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequestVO {

	private String username;
	private String password;
	
	private EntityTypes entityType;
	private StepKeys stepKey;
	
}
