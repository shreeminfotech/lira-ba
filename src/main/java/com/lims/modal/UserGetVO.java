package com.lims.modal;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGetVO {

	private int id;
	private String description;
	private String empId;
	private String email;
	private String mobileNo;
	private String firstName;
	private String lastName;
	
	private Boolean  isActive;
	private Boolean  isTemporary;
	
	private String username;
	
	private Integer createdBy;
	private Integer updatedBy;
	
	private String createdByUserName;
	private String updatedByUserName;
	
	private Date createdDate;
	private Date updatedDate;
	
	private Map<Integer, String> labs;
	private Date logintime;
	private Date lastlogintime;
	
	
}
