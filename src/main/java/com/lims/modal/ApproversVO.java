package com.lims.modal;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApproversVO {
	
	private Integer id;
	private String name;
	private String username;
	private String stepKey;
	private Timestamp signedDate; 

}
