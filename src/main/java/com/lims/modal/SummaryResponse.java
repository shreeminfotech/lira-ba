package com.lims.modal;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SummaryResponse<T> {

	private long totalRecords;
	private long totalPages;
	private List<T> entities;

}
