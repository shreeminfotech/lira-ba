package com.lims.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManagePageVO {
	
	private SearchVO search;	
	private SortingVO sort;
	private Pagination page;
	
}
