package com.lims.modal;

public enum VisibilityLevel {
	SELF, ALL, USERS
}
