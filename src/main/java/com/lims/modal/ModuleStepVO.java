package com.lims.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModuleStepVO {
	
	private int id;
	private int stepOrder;
	private String stepKey;
	private Boolean clickable;
	private String name;

}
