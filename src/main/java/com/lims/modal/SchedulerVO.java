package com.lims.modal;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SchedulerVO {

	@NotEmpty(message = "Repeat count is required")
	private Integer repeatCount;
	
	@NotNull(message = "Repeat on is required")
	@NotEmpty(message = "Repeat on is required")
	private List<String> repeatOn;
	private String repeatFrequency;
	
	@NotNull(message = "Start date is required")
	@NotEmpty(message = "Start date is required")
	private Date startDate;
	
	@NotNull(message = "End Date is required")
	@NotEmpty(message = "End Date is required")
	private Date endDate;
	
	private String startTime;
	private String endTime;
	
}
