package com.lims.modal;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SopTypeGetVO extends Response{
	
	private int id;
	private int labId;
	private String name;
	private String labName;
	private String description;
	private Boolean isActive;
	private Boolean systemDefault;
	private String colorCode;
	private Integer createdBy;
	private Integer updatedBy;
	
	private String createdByUserName;
	private String updatedByUserName;
	
	private Date createdDate;
	private Date updatedDate;
	
	private int sopcount;
	
	private VisibilityLevel visibilityLevel; //(SELF, ALL, USERS)
	private List<SelectInputVO> userList;

}
