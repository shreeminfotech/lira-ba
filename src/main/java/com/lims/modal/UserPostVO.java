package com.lims.modal;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPostVO {

	private String description;
	private String empId;
	private String email;
	private String mobileNo;
	private String firstName;
	private String lastName;

	private List<Integer> roleIds;

	private String username;

	private Integer createdBy;
	private Integer updatedBy;
}
