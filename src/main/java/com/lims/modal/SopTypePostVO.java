package com.lims.modal;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SopTypePostVO {
	
	@NotEmpty(message = "Name is required")
	private String name;
	private String description;
	private Boolean systemDefault;
	private String colorCode;
	private String visibilityLevel; //(SELF, ALL, USERS)
	private List<SelectInputVO> userList;
	private String comments;
	

}
