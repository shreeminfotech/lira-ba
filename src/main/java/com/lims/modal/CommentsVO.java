package com.lims.modal;

import java.util.Date;

import com.lims.enums.EntityTypes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentsVO {
	
	private EntityTypes entityType;
	
	private Integer entityId;
	
	private String comments;

	private String username;
	
	private Date updatedDate;
	
	private boolean freezed;
	
}
