package com.lims.modal;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowVO {
	
	private Boolean signedFlag;
	private int workFlowOrder;
	private Date signedDate;
	
	private Integer moduleStepId;
	private String stepKey;
	
	private Integer assignedUser;
	private String assignedUserName;

}
