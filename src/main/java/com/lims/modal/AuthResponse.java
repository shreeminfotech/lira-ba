package com.lims.modal;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthResponse {

	private String token;
	private Map<Integer, String> labs;
	private String username;
	private String firstname;
	private String lastname;
	private Date logintime;
	private Date lastlogintime;
}
