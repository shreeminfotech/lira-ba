package com.lims.modal;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SopPostVO {
	
	private Integer id;
	private Integer status;
	
	@NotNull(message = "Sop Type is required")
	private SelectInputVO typeId;
	
	@NotNull(message = "Name is required")
	@NotEmpty(message = "Name is required")
	private String name;
	
	@NotNull(message = "Number is required")
	@NotEmpty(message = "Number is required")
	private String number;
	
	@NotNull(message = "Please select Author(s)")
	@NotEmpty(message = "Please select Author(s)")
	private List<SelectInputVO> author;
	
	@NotNull(message = "Please select Reviewer(s)")
	@NotEmpty(message = "Please select Reviewer(s)")
	private List<SelectInputVO> reviewer;
	
	@NotNull(message = "Please select First Approver")
	@NotEmpty(message = "Please select First Approver")
	private List<SelectInputVO> firstApprover;
	
	@NotNull(message = "Please select Second Approver")
	@NotEmpty(message = "Please select Second Approver")
	private List<SelectInputVO> secondApprover;
	
	private Integer signedBy;
	private String signedByName;
	
	private Boolean signedFlag;
	
	private String comments;
	
	private SchedulerVO triggerData;
}
