package com.lims.modal;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocumentVO {
	
	private int id;
	private int labId;

	private String uploadedBy;
	private Date uploadedDate;
	private String documentType;
	private String name;
	
	private String entityType;
	private String entityId;
	
}
