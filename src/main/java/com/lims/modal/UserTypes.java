package com.lims.modal;

public enum UserTypes {
	
	initiator, author, reviewer, approver, visibility

}
