package com.lims.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lims.modal.SopPutVO;

public class SopUpdateConstraintValidator implements ConstraintValidator<ValidSopUpdate, SopPutVO> {

	//private String fieldName;

	//@Override
	public void initialize(ValidSopUpdate constraintAnnotation) {
		//fieldName = constraintAnnotation.fieldName();
	}

	@Override
	public boolean isValid(SopPutVO value, ConstraintValidatorContext context) {
		if (value == null || (value.getSignedFlag() == null || !value.getSignedFlag())) {
			return true;
		}
		// TODO Validation
		return false;
	}
}
