package com.lims.validators;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { SopUpdateConstraintValidator.class })
public @interface ValidSopUpdate {


	String message() default "test";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}