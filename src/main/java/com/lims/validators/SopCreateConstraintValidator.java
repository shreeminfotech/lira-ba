package com.lims.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.lims.modal.SopPostVO;

public class SopCreateConstraintValidator implements ConstraintValidator<ValidSopCreate, SopPostVO> {

	//private String fieldName;

	//@Override
	public void initialize(ValidSopCreate constraintAnnotation) {
		//fieldName = constraintAnnotation.fieldName();
	}

	@Override
	public boolean isValid(SopPostVO value, ConstraintValidatorContext context) {
		if (value == null || (value.getSignedFlag() == null || !value.getSignedFlag())) {
			return true;
		}
		// TODO Validation
		return false;
	}
}
