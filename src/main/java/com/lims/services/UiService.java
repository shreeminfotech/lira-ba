package com.lims.services;

import java.util.List;

import com.lims.enums.EntityTypes;
import com.lims.modal.DocumentCommentsVO;
import com.lims.modal.ModuleStepVO;

public interface UiService {

	public List<ModuleStepVO> geSteps(EntityTypes module) throws Throwable;

	public void updateDocumentComments(int labId, String username, EntityTypes entity, Integer id,
			List<DocumentCommentsVO> docCommVO) throws Throwable;

}
