package com.lims.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.lims.modal.CommentsVO;
import com.lims.modal.DocumentVO;
import com.lims.modal.SopGetVO;
import com.lims.modal.SopPostVO;
import com.lims.modal.SopPutVO;
import com.lims.modal.SummaryResponse;

public interface SopService {

	public SummaryResponse<SopGetVO> sopList(Integer labId, Integer typeId, String searchval, Pageable page, String username) throws Throwable;

	public List<SopGetVO> sopListByLabId(Integer id, String username) throws Throwable;

	public SopGetVO getSopById(Integer id, String username) throws Throwable;

	public SopGetVO createSop(Integer labId, SopPostVO SopTypes, String username) throws Throwable;

	public SopGetVO updateSop(Integer labId, Integer id, SopPutVO SopTypes ,Boolean isActive, String username) throws Throwable;

	public List<SopGetVO> sopListBySopTypeId(Integer labId, Integer typeid, String username) throws Throwable;

	public List<CommentsVO> sopCommentsById(int labid, Integer id) throws Throwable;

	public DocumentVO sopDocument(int labid, Integer id) throws Throwable;
	
	public SopGetVO saveComments(int labId, CommentsVO comments)
			throws Throwable;
	
}
