package com.lims.services;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.lims.enums.EntityTypes;
import com.lims.modal.AuthRequestVO;
import com.lims.modal.SelectInputVO;
import com.lims.modal.SummaryResponse;
import com.lims.modal.UserGetVO;
import com.lims.modal.UserPostVO;
import com.lims.modal.UserTypes;

public interface UserService extends UserDetailsService {
	
	public UserGetVO findByUsername(String username) throws Throwable;

	public List<SelectInputVO> usersDropdownByLabId(Integer id, EntityTypes entityType, UserTypes userType, Integer entityid, String username) throws Throwable;

	public UserGetVO findById(Integer id) throws Throwable;
	
	public SummaryResponse<UserGetVO> getUsers(Integer labId, String searchval, Pageable page) throws Throwable;
	
	public void createUser(UserPostVO user) throws Throwable;
	
	public void updateUser(int id, UserPostVO user, Boolean isActive) throws Throwable;

	public SelectInputVO signInforSignature(Integer labId, AuthRequestVO userVO) throws Throwable;

}
