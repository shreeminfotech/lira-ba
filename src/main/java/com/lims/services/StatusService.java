package com.lims.services;

import java.util.List;

import com.lims.modal.SelectInputVO;

public interface StatusService {

	public List<SelectInputVO> statusDropdownByModuleType(Integer labid, String module) throws Throwable;

}
