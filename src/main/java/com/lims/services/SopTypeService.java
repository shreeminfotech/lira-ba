package com.lims.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.lims.modal.SelectInputVO;
import com.lims.modal.SopTypeGetVO;
import com.lims.modal.SopTypePostVO;
import com.lims.modal.SummaryResponse;

public interface SopTypeService {

	public SummaryResponse<SopTypeGetVO> sopTypeList(Integer labId, String searchval, Pageable page, String username) throws Throwable ;
	
//	public List<SopTypeVO> sopTypeListByLabId(Integer id) throws Throwable;
	
	public SopTypeGetVO getSopTypeById(Integer id) throws Throwable;
	
	public SopTypeGetVO createSopType(Integer labId, SopTypePostVO sopType, String username) throws Throwable;
	
	public SopTypeGetVO updateSopType(Integer labId, int id, SopTypePostVO sopType, Boolean isActive, String username) throws Throwable;

	public List<SelectInputVO> sopTypeDropdownByLabId(Integer id, String username) throws Throwable;

}
