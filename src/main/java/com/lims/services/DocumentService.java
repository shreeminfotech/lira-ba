package com.lims.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.lims.dao.domain.ApplicationDocuments;
import com.lims.modal.DocumentVO;

public interface DocumentService {
	
	public List<DocumentVO> uploadDocuments(Integer labId, MultipartFile[] files, String username) throws Throwable;

	public ApplicationDocuments getFile(Integer id);
}
