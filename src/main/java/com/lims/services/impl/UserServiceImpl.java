package com.lims.services.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.lims.dao.domain.ApplicationUser;
import com.lims.dao.domain.ApplicationUserRole;
import com.lims.dao.domain.Lab;
import com.lims.dao.domain.SopType;
import com.lims.dao.domain.SopTypeVisibility;
import com.lims.enums.EntityTypes;
import com.lims.enums.StepKeys;
import com.lims.modal.AuthRequestVO;
import com.lims.modal.SelectInputVO;
import com.lims.modal.SummaryResponse;
import com.lims.modal.UserGetVO;
import com.lims.modal.UserPostVO;
import com.lims.modal.UserTypes;
import com.lims.repositories.SopTypesRepository;
import com.lims.repositories.UserRepository;
import com.lims.services.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private SopTypesRepository sopTypeRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);
		return new User(user.getUsername(), user.getPassword(), new ArrayList<>());
	}

	public List<SelectInputVO> usersDropdownByLabId(Integer id, EntityTypes entityType, UserTypes userType,
			Integer entityid, String username) throws Throwable {
		List<SelectInputVO> selectVoList = null;
		SopType sopType = null;
		List<Integer> userIdList = new ArrayList<Integer>();
		if (userType != null) {
			if (userType.equals(UserTypes.visibility)) {
				if (entityid == null || entityid == 0l) {
					ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);
					selectVoList = userRepository.sopTypeVisibilityUsers(id, entityType, true, true, user.getId());
				} else {
					SopType stype = sopTypeRepository.getById(entityid);

					selectVoList = userRepository.sopTypeVisibilityUsers(id, entityType, true, true,
							stype.getApplicationUser1().getId());
				}
			} else {
				sopType = sopTypeRepository.getById(entityid);
				if (userType.equals(UserTypes.initiator))
					selectVoList = userRepository.getInitiatorsList(id, entityType, true, true);
				else if (userType.equals(UserTypes.author))
					selectVoList = userRepository.getAuthorsList(id, entityType, true, true);
				else if (userType.equals(UserTypes.reviewer))
					selectVoList = userRepository.geReviewersList(id, entityType, true, true);
				else if (userType.equals(UserTypes.approver))
					selectVoList = userRepository.getApproversList(id, entityType, true, true);

				for (SelectInputVO obj : selectVoList) {
					userIdList.add(obj.getId());
				}
			}
		}
		if (sopType != null && (sopType.getVisibility() != null && sopType.getVisibility().equalsIgnoreCase("All"))) {
			return selectVoList;
		} else {
			if (sopType.getVisibility() != null && userIdList != null) {
				List<SelectInputVO> tempSelectVoList = new ArrayList<SelectInputVO>();
				for (SopTypeVisibility sio : sopType.getSopTypeVisibilities()) {
					if (userIdList.contains(sio.getApplicationUser().getId())) {
						SelectInputVO selvo = new SelectInputVO();
						selvo.setId(sio.getApplicationUser().getId());
						selvo.setName(
								sio.getApplicationUser().getFirstName() + " " + sio.getApplicationUser().getLastName());
						tempSelectVoList.add(selvo);
					}
				}
				return tempSelectVoList;
			}
		}
		/*
		 * if (objList != null) { selectVoList = new ArrayList<SelectInputVO>(); for
		 * (Object[] obj : objList) { SelectInputVO selectVo = new SelectInputVO();
		 * selectVo.setId((int) obj[0]); selectVo.setName((String) obj[1] + " " +
		 * (String) obj[2]); selectVoList.add(selectVo); } }
		 */
		return selectVoList;
	}

	@Override
	public UserGetVO findById(Integer id) throws Throwable {
		Optional<ApplicationUser> user = userRepository.findById(id);

		UserGetVO userVO = new UserGetVO();

		return userVO;
	}

	@Override
	public UserGetVO findByUsername(String username) throws Throwable {
		ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);

		UserGetVO userVO = new UserGetVO();
		userVO.setId(user.getId());
		userVO.setFirstName(user.getFirstName());
		userVO.setLastName(user.getLastName());
		userVO.setUsername(user.getUsername());
		userVO.setLastlogintime(user.getLastLoginDate());
		Map<Integer, String> labs = new LinkedHashMap<Integer, String>();
		for (ApplicationUserRole role : user.getApplicationUserRoles()) {
			if (role.getApplicationSecurityRole() != null) {
				Lab lab = role.getApplicationSecurityRole().getLab();
				labs.put(lab.getId(), lab.getName());
			}
		}
		userVO.setLabs(labs);
		return userVO;
	}

	@Override
	public SummaryResponse<UserGetVO> getUsers(Integer labId, String searchval, Pageable page) throws Throwable {
		SummaryResponse<UserGetVO> summary = new SummaryResponse<UserGetVO>();
		Page<ApplicationUser> pageObj = null;
		List<UserGetVO> userVOList = new ArrayList<UserGetVO>();

		// summary.setTotalRecords(sopTypesRepository.getSopTypesCount(manageObj));
		pageObj = userRepository.userList(labId, searchval, page);
		if (pageObj != null && pageObj.getTotalElements() > 0) {
			summary.setTotalRecords(pageObj.getTotalElements());
			summary.setTotalPages(pageObj.getTotalPages());
			for (ApplicationUser user : pageObj.getContent()) {
				UserGetVO userVo = new UserGetVO();
				// convertEntityToDTO(sopType, sopTypeVo);

				userVOList.add(userVo);
			}
			summary.setEntities(userVOList);
		}

		return summary;
	}

	@Override
	public void createUser(UserPostVO user) throws Throwable {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateUser(int id, UserPostVO user, Boolean isActive) throws Throwable {
		// TODO Auto-generated method stub

	}

	@Override
	public SelectInputVO signInforSignature(Integer labId, AuthRequestVO userVO) throws Throwable {
		Object[] obj = null;
		if (userVO.getStepKey() == StepKeys.INITIATOR || userVO.getStepKey() == StepKeys.TRAINING)
			obj = userRepository.initiatorSignIn(labId, userVO.getEntityType(), userVO.getUsername(),
					userVO.getPassword(), true, true);
		else if (userVO.getStepKey() == StepKeys.AUTHOR)
			obj = userRepository.authorSignIn(labId, userVO.getEntityType(), userVO.getUsername(), userVO.getPassword(),
					true, true);
		else if (userVO.getStepKey() == StepKeys.REVIEWER)
			obj = userRepository.reviewerSignIn(labId, userVO.getEntityType(), userVO.getUsername(),
					userVO.getPassword(), true, true);
		else if (userVO.getStepKey() == StepKeys.FIRST_APPROVER || userVO.getStepKey() == StepKeys.SECOND_APPROVER)
			obj = userRepository.approverSignIn(labId, userVO.getEntityType(), userVO.getUsername(),
					userVO.getPassword(), true, true);
		SelectInputVO selectVo = null;
		if (obj != null) {
			for (Object o : obj) {
				Object[] tempobj = (Object[]) o;
				selectVo = new SelectInputVO();

				selectVo.setId((int) tempobj[0]);
				selectVo.setName((String) tempobj[1] + " " + (String) tempobj[2]);
				break;
			}

		}
		return selectVo;
	}

}
