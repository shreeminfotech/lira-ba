package com.lims.services.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.hibernate.internal.util.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.lims.dao.domain.ApplicationComment;
import com.lims.dao.domain.ApplicationDocuments;
import com.lims.dao.domain.ApplicationModule;
import com.lims.dao.domain.ApplicationModuleStep;
import com.lims.dao.domain.ApplicationScheduler;
import com.lims.dao.domain.ApplicationStatus;
import com.lims.dao.domain.ApplicationUser;
import com.lims.dao.domain.AuditLog;
import com.lims.dao.domain.Lab;
import com.lims.dao.domain.Sop;
import com.lims.dao.domain.SopType;
import com.lims.dao.domain.WorkFlowMetrix;
import com.lims.enums.EntityTypes;
import com.lims.enums.StatusTypes;
import com.lims.enums.StepKeys;
import com.lims.exceptions.ResourceNotFoundException;
import com.lims.modal.ApproversVO;
import com.lims.modal.CommentsVO;
import com.lims.modal.DocumentVO;
import com.lims.modal.SchedulerVO;
import com.lims.modal.SelectInputVO;
import com.lims.modal.SopGetVO;
import com.lims.modal.SopPostVO;
import com.lims.modal.SopPutVO;
import com.lims.modal.SummaryResponse;
import com.lims.modal.WorkflowVO;
import com.lims.repositories.ApplicationDocumentsRepository;
import com.lims.repositories.ApplicationModuleRepository;
import com.lims.repositories.ApplicationModuleStepRepository;
import com.lims.repositories.ApplicationSchedulerRepository;
import com.lims.repositories.CommentsRepository;
import com.lims.repositories.SopRepository;
import com.lims.repositories.StatusRepository;
import com.lims.repositories.UserRepository;
import com.lims.repositories.WorkflowMetrixRepository;
import com.lims.services.SopService;

@Service
public class SopServiceImpl implements SopService {

	@Autowired
	private SopRepository sopRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ApplicationModuleStepRepository applicationModuleStepRepository;

	@Autowired
	private ApplicationSchedulerRepository applicationSchedulerRepository;

	@Autowired
	private ApplicationModuleRepository applicationModuleRepository;

	@Autowired
	WorkflowMetrixRepository workflowMetrixRepository;

	@Autowired
	StatusRepository statusRepository;

	@Autowired
	CommentsRepository commentsRepository;
	
	@Autowired
	private ApplicationDocumentsRepository applicationDocumentsRepository;
	
	@Autowired
    private Validator validator;

	@Override
	public SummaryResponse<SopGetVO> sopList(Integer labId, Integer typeId, String searchval, Pageable page,
			String username) throws Throwable {
		SummaryResponse<SopGetVO> summary = new SummaryResponse<SopGetVO>();
		List<SopGetVO> sopVOList = new ArrayList<SopGetVO>();

		Page<Sop> pageObj = null;
		if (typeId == null)
			pageObj = sopRepository.sopList(labId, searchval, page, username, true);
		else
			pageObj = sopRepository.sopListBySopType(labId, typeId, searchval, page, username, true);

		if (pageObj != null && pageObj.getTotalElements() > 0) {
			summary.setTotalRecords(pageObj.getTotalElements());
			summary.setTotalPages(pageObj.getTotalPages());
			for (Sop sop : pageObj.getContent()) {
				SopGetVO sopGetVO = new SopGetVO();
				convertEntityToDTO(sop, sopGetVO, username);

				sopVOList.add(sopGetVO);
			}
			summary.setEntities(sopVOList);
		}

		return summary;
	}

	@Override
	public SopGetVO getSopById(Integer id, String username) throws Throwable {
		Optional<Sop> sopData = sopRepository.findById(id);
		if (sopData.isPresent()) {
			SopGetVO sopVo = new SopGetVO();
			Sop sop = sopData.get();
			convertEntityToDTO(sop, sopVo, username);
			return sopVo;
		} else {
			return null;
		}
	}

	@Override
	public List<SopGetVO> sopListByLabId(Integer id, String username) throws Throwable {
		List<Sop> sopList = sopRepository.findByLabId(id, true);
		List<SopGetVO> sopVoList = new ArrayList<SopGetVO>();
		if (sopList != null && sopList.size() > 0) {
			for (Sop s : sopList) {
				SopGetVO sopVo = new SopGetVO();
				convertEntityToDTO(s, sopVo, username);
				sopVoList.add(sopVo);
			}
			return sopVoList;
		} else {
			return null;
		}
	}

	@Override
	public List<SopGetVO> sopListBySopTypeId(Integer labId, Integer typeid, String username) throws Throwable {
		List<Sop> sopList = sopRepository.findBySopTypeId(labId, typeid, true);
		List<SopGetVO> sopVoList = new ArrayList<SopGetVO>();
		if (sopList != null && sopList.size() > 0) {
			for (Sop s : sopList) {
				SopGetVO sopVo = new SopGetVO();
				convertEntityToDTO(s, sopVo, username);
				sopVoList.add(sopVo);
			}
			return sopVoList;
		} else {
			return null;
		}
	}

	@Override
	public SopGetVO createSop(Integer labId, SopPostVO sopVO, String username) throws Throwable {
		SopGetVO sopGetVO = new SopGetVO();
		List<String> errorMessage = new ArrayList<String>();
		if(sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
			 Set<ConstraintViolation<SopPostVO>> violations = validator.validate(sopVO);

		        if (!violations.isEmpty()) {
		            StringBuilder sb = new StringBuilder();
		            for (ConstraintViolation<SopPostVO> constraintViolation : violations) {
		                sb.append(constraintViolation.getMessage());
		            }
		            throw new ConstraintViolationException("Error occurred: " + sb.toString(), violations);
		        }
		}
		if(StringHelper.isNotEmpty(sopVO.getName())) {
			Sop sop = sopRepository.findByName(sopVO.getName());
			if(sop != null) {
				errorMessage.add("Name already exists.");
			}
		}
		if(StringHelper.isNotEmpty(sopVO.getNumber())) {
			Sop sop = sopRepository.findByNumber(sopVO.getNumber());
			if(sop != null) {
				errorMessage.add("Number already exists.");
			}
		}
		if(errorMessage.size() > 0) {
			sopGetVO.setErrorMessages(errorMessage);
			return sopGetVO;
		}
		Date dt = new Date();

		ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);
		AuditLog auditLog = new AuditLog();
		auditLog.setActionType("CREATE");

		ApplicationModule appModule = applicationModuleRepository.findByEnumKey(EntityTypes.SOP);

		auditLog.setApplicationModule(appModule);
		auditLog.setCreatedDate(dt);

		auditLog.setApplicationUser(user);

		Sop sop = new Sop();
		if(sopVO.getId() != null) {
			sop = sopRepository.getById(sopVO.getId());
		}

		sop.setName(sopVO.getName());
		sop.setNumber(sopVO.getNumber());
		sop.setCreatedDate(dt);
		sop.setAuditLog(auditLog);
		sop.setIsActive(true);

		SopType sopType = new SopType();
		if(sopVO.getTypeId() != null) {
			sopType.setId(sopVO.getTypeId().getId());
			sopType.setName(sopVO.getTypeId().getName());
		}
		sop.setSopType(sopType);

		sop.setApplicationUser1(user);

		Lab lab = new Lab();
		lab.setId(labId);
		sop.setLab(lab);
		
		if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
			sop.setCurrentStep(2);
			sop.setCompletedStep(1);
			sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.DRAFT));
		} else {
			sop.setCurrentStep(1);
			// sop.setCompletedStep(1);
			sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.INITIATE));
		}
		sop = sopRepository.save(sop);

		List<WorkFlowMetrix> workFlowList = new ArrayList<WorkFlowMetrix>();
		WorkFlowMetrix workFlow = new WorkFlowMetrix();
		ApplicationUser initiated = null;
		if (sopVO.getSignedBy() != null && sopVO.getSignedBy() != 0) {
			initiated = new ApplicationUser();
			initiated.setId(sopVO.getSignedBy());
			initiated.setFullName(sopVO.getSignedByName());
		}

		workFlow.setApplicationUser(initiated != null ? initiated : user);
		if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
			workFlow.setSignedDate(new Date());
			workFlow.setSignedFlag(true);
		}
		ApplicationModuleStep moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP,
				StepKeys.INITIATOR, true);
		workFlow.setApplicationModuleStep(moduleStep);
		workFlow.setAuditLog(auditLog);
		workFlow.setEntityId(sop.getId());
		workFlow.setEntityType(EntityTypes.SOP);
		ApplicationComment comment = new ApplicationComment();
		comment.setComments(sopVO.getComments());
		comment.setAuditLog(auditLog);
		comment.setUpdatedDate(dt);
		if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
			comment.setFreeze(true);
		} else {
			comment.setFreeze(false);
		}
		workFlow.addApplicationComment(comment);

		workFlowList.add(workFlow);
		

		if (sopVO.getAuthor() != null && sopVO.getAuthor().size() > 0) {
			for (SelectInputVO userVo : sopVO.getAuthor()) {
				workFlow = new WorkFlowMetrix();
				ApplicationUser author = new ApplicationUser();
				author.setId(userVo.getId());
				author.setFullName(userVo.getName());
				workFlow.setApplicationUser(author);
				moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP, StepKeys.AUTHOR,
						true);
				workFlow.setApplicationModuleStep(moduleStep);
				workFlow.setAuditLog(auditLog);
				workFlow.setEntityId(sop.getId());
				workFlow.setEntityType(EntityTypes.SOP);
				workFlowList.add(workFlow);
			}
		}

		if (sopVO.getReviewer() != null && sopVO.getReviewer().size() > 0) {
			for (SelectInputVO userVo : sopVO.getReviewer()) {
				workFlow = new WorkFlowMetrix();
				ApplicationUser reviewer = new ApplicationUser();
				reviewer.setId(userVo.getId());
				reviewer.setFullName(userVo.getName());
				workFlow.setApplicationUser(reviewer);
				moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP, StepKeys.REVIEWER,
						true);
				workFlow.setApplicationModuleStep(moduleStep);
				workFlow.setAuditLog(auditLog);
				workFlow.setEntityId(sop.getId());
				workFlow.setEntityType(EntityTypes.SOP);
				workFlowList.add(workFlow);
			}
		}

		if (sopVO.getFirstApprover() != null && sopVO.getFirstApprover().size() > 0) {
			for (SelectInputVO userVo : sopVO.getFirstApprover()) {
				workFlow = new WorkFlowMetrix();
				ApplicationUser firstApprover = new ApplicationUser();
				firstApprover.setId(userVo.getId());
				firstApprover.setFullName(userVo.getName());
				workFlow.setApplicationUser(firstApprover);
				moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP,
						StepKeys.FIRST_APPROVER, true);
				workFlow.setApplicationModuleStep(moduleStep);
				workFlow.setAuditLog(auditLog);
				workFlow.setEntityId(sop.getId());
				workFlow.setEntityType(EntityTypes.SOP);
				workFlowList.add(workFlow);
			}
		}


		if (sopVO.getSecondApprover() != null && sopVO.getSecondApprover().size() > 0) {
			for (SelectInputVO userVo : sopVO.getSecondApprover()) {
				workFlow = new WorkFlowMetrix();
				ApplicationUser secondApprover = new ApplicationUser();
				secondApprover.setId(userVo.getId());
				secondApprover.setFullName(userVo.getName());
				workFlow.setApplicationUser(secondApprover);
				moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP,
						StepKeys.SECOND_APPROVER, true);
				workFlow.setApplicationModuleStep(moduleStep);
				workFlow.setAuditLog(auditLog);
				workFlow.setEntityId(sop.getId());
				workFlow.setEntityType(EntityTypes.SOP);
				workFlowList.add(workFlow);
			}
		}
		
		moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP,
				StepKeys.TRAINING, true);
		
		workFlow = new WorkFlowMetrix();
		workFlow.setApplicationUser(initiated != null ? initiated : user);
		workFlow.setApplicationModuleStep(moduleStep);
		workFlow.setAuditLog(auditLog);
		workFlow.setEntityId(sop.getId());
		workFlow.setEntityType(EntityTypes.SOP);
		workFlowList.add(workFlow);
		
		workFlowList = workflowMetrixRepository.saveAll(workFlowList);

		if (sopVO.getTriggerData() != null && sopVO.getTriggerData().getRepeatFrequency() != null
				&& !sopVO.getTriggerData().getRepeatFrequency().equals("")) {
			ApplicationScheduler triggerDetails = new ApplicationScheduler();
			
			String repeatStr = "";
			for(String i: sopVO.getTriggerData().getRepeatOn()) {
				repeatStr = repeatStr + i +",";
			}
			triggerDetails.setRepeatOn(repeatStr.substring(0, repeatStr.lastIndexOf(",")));
			triggerDetails.setRepeatCount(sopVO.getTriggerData().getRepeatCount());
			triggerDetails.setFrequencyType(sopVO.getTriggerData().getRepeatFrequency());
			triggerDetails.setStartDate(sopVO.getTriggerData().getStartDate());
			triggerDetails.setEndDate(sopVO.getTriggerData().getEndDate());
			triggerDetails.setStartTime(sopVO.getTriggerData().getStartTime());
			triggerDetails.setEndTime(sopVO.getTriggerData().getEndTime());
			triggerDetails.setAuditLog(auditLog);
			triggerDetails.setEntityId(sop.getId());
			triggerDetails.setEntityType(EntityTypes.SOP);

			applicationSchedulerRepository.save(triggerDetails);
		}

		auditLog.setEntityId(sop.getId());
		sop.setAuditLog(null);
		for (WorkFlowMetrix wfm : workFlowList) {
			wfm.setAuditLog(null);
			if (wfm.getApplicationComments() != null) {
				for (ApplicationComment comment1 : wfm.getApplicationComments()) {
					comment1.setAuditLog(null);
				}
			}
			workflowMetrixRepository.save(wfm);
		}
		
		convertEntityToDTO(sop, sopGetVO, username);
		return sopGetVO;
	}

	@Override
	public SopGetVO updateSop(Integer labId, Integer id, SopPutVO sopVO, Boolean isActive, String username)
			throws Throwable {
		SopGetVO sopGetVO = new SopGetVO();
		List<String> errorMessage = new ArrayList<String>();
		if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()
				&& (sopVO.getRejected() == null || !sopVO.getRejected())) {
			Set<ConstraintViolation<SopPutVO>> violations = validator.validate(sopVO);
			StringBuilder sb = new StringBuilder();
			if (!violations.isEmpty()) {
				for (ConstraintViolation<SopPutVO> constraintViolation : violations) {
					sb.append(constraintViolation.getMessage());
				}
				if(sb.length() > 0) {
					throw new ConstraintViolationException("Error occurred: " + sb.toString(), violations);
				}
			}
			if (sopVO.getCurrentStep() == 1 || (sopVO.getInitiatorFlag() != null && sopVO.getInitiatorFlag())) {
				if(sopVO.getAuthor() == null || sopVO.getAuthor().size() == 0) {
					errorMessage.add("Please select Author(s)");
				}
				if(sopVO.getReviewer() == null || sopVO.getReviewer().size() == 0) {
					errorMessage.add("Please select Reviewer(s)");
				}
				if(sopVO.getFirstApprover() == null || sopVO.getFirstApprover().size() == 0) {
					errorMessage.add("Please select First Approver");
				}
				if(sopVO.getSecondApprover() == null || sopVO.getSecondApprover().size() == 0) {
					errorMessage.add("Please select Second Approver");
				}
				if(StringHelper.isNotEmpty(sopVO.getName())) {
					Sop sop = sopRepository.findByName(sopVO.getName());
					if(sop != null && sop.getId() != id) {
						errorMessage.add("Name already exists.");
					}
				}
				if(StringHelper.isNotEmpty(sopVO.getNumber())) {
					Sop sop = sopRepository.findByNumber(sopVO.getNumber());
					if(sop != null && sop.getId() != id) {
						errorMessage.add("Number already exists.");
					}
				}
				
			} else if ((sopVO.getCompletedStep() == 1 || sopVO.getCurrentStep() == 2)
					&& (sopVO.getInitiatorFlag() == null || !sopVO.getInitiatorFlag())) {
				if (StringHelper.isEmpty(sopVO.getRevision())) {
					errorMessage.add("Revision is required.");
				}
				if (StringHelper.isEmpty(sopVO.getVersion())) {
					errorMessage.add("Version is required.");
				}
			}
			if(errorMessage.size() > 0) {
				sopGetVO.setErrorMessages(errorMessage);
				return sopGetVO;
			}
		}
		Optional<Sop> sopData = sopRepository.findById(id);

		if (sopData.isPresent()) {

			ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);

			Date dt = new Date();

			AuditLog auditLog = new AuditLog();
			auditLog.setActionType("MODIFY");

			ApplicationModule appModule = applicationModuleRepository.findByEnumKey(EntityTypes.SOP);

			auditLog.setApplicationModule(appModule);
			auditLog.setCreatedDate(dt);

			auditLog.setApplicationUser(user);

			Sop sop = sopData.get();
			//Optional<ApplicationStatus> status = sopVO.getStatus() == null? null:statusRepository.findById(sopVO.getStatus());
			sop.setAuditLog(auditLog);
			if (isActive != null && isActive) {
				if (sopVO.getName() != null && !sopVO.getName().equals(""))
					sop.setName(sopVO.getName());
				if (sopVO.getNumber() != null && !sopVO.getNumber().equals(""))
					sop.setNumber(sopVO.getNumber());
				if (isActive != null)
					sop.setIsActive(isActive);

				sop.setApplicationUser2(user);
				sop.setUpdatedDate(dt);
				sop.setVersion(sopVO.getVersion());
				sop.setRevision(sopVO.getRevision());

				/*
				 * ApplicationStatus appStatus = null; if (status != null && status.isPresent())
				 * { appStatus = status.get(); } else { appStatus = sop.getApplicationStatus();
				 * }
				 */
				ApplicationStatus appStatus = sop.getApplicationStatus();
				if (appStatus != null && (appStatus.getEnumKey().equals(StatusTypes.INITIATE)
						|| (sopVO.getInitiatorFlag() == null || !sopVO.getInitiatorFlag()))) {
					if(sopVO.getRejected() != null && sopVO.getRejected()){
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCompletedStep(sop.getCurrentStep());
							sop.setCurrentStep(2);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.DRAFT));
						}
					} else if(appStatus.getEnumKey().equals(StatusTypes.INITIATE)) {
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCurrentStep(2);
							sop.setCompletedStep(1);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.DRAFT));
						}
					} else if (appStatus.getEnumKey().equals(StatusTypes.DRAFT)) {
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCompletedStep(sop.getCurrentStep());
							sop.setCurrentStep(3);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.IN_REVIEW));
						}
					} else if (appStatus.getEnumKey().equals(StatusTypes.IN_REVIEW)) {
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCompletedStep(sop.getCurrentStep());
							sop.setCurrentStep(4);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.FIRST_APPROVED));
						}
					} else if (appStatus.getEnumKey().equals(StatusTypes.FIRST_APPROVED)) {
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCompletedStep(sop.getCurrentStep());
							sop.setCurrentStep(5);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.SECOND_APPROVED));
						}
					} else if (appStatus.getEnumKey().equals(StatusTypes.SECOND_APPROVED)) {
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCompletedStep(sop.getCurrentStep());
							sop.setCurrentStep(6);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.TRAINING));
						}
					} else if (appStatus.getEnumKey().equals(StatusTypes.TRAINING)) {
						if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
							sop.setCompletedStep(sop.getCurrentStep());
							sop.setCurrentStep(7);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.ACTIVE));
						}
					}
				} 
				if (sopVO.getInitiatorFlag() != null && sopVO.getInitiatorFlag()) {
					if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
						if (sop.getCompletedStep() == null
								|| (sop.getCompletedStep() != null && sop.getCompletedStep() <= 1)) {
							sop.setCurrentStep(2);
							sop.setCompletedStep(1);
							sop.setApplicationStatus(statusRepository.findByEnumKey(StatusTypes.DRAFT));
						}
					}
				}
				sop = sopRepository.save(sop);

				List<WorkFlowMetrix> workFlowList = workflowMetrixRepository.findByEntityIdAndEntityType(sop.getId(), EntityTypes.SOP);
				
				List<WorkFlowMetrix> removedworkFlowList = new ArrayList<WorkFlowMetrix>();
				if (appStatus.getEnumKey().equals(StatusTypes.INITIATE)
						|| (sopVO.getInitiatorFlag() != null && sopVO.getInitiatorFlag())) {
					//workFlowList = workflowMetrixRepository.findByEntityIdAndEntityType(sop.getId(), EntityTypes.SOP);
					// List<Integer> existingInitiatorList = new ArrayList<Integer>();
					List<Integer> existingAuthorList = new ArrayList<Integer>();
					List<Integer> existingReviewerList = new ArrayList<Integer>();
					List<Integer> existingFirstApproverList = new ArrayList<Integer>();
					List<Integer> existingSecondApproverList = new ArrayList<Integer>();

					for (WorkFlowMetrix wfm : workFlowList) {

						if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.INITIATOR)) {
							Integer initiatedBy = (sopVO.getSignedBy() != null && user.getId() != sopVO.getSignedBy())
									? sopVO.getSignedBy()
									: user.getId();
							if (initiatedBy != wfm.getApplicationUser().getId()) {
								ApplicationUser tempUser = new ApplicationUser();
								tempUser.setId(initiatedBy);
								wfm.setApplicationUser(tempUser);
							}
							if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
								wfm.setSignedFlag(true);
								wfm.setSignedDate(dt);
							}
						}

						if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.AUTHOR))
							existingAuthorList.add(wfm.getApplicationUser().getId());
						if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.REVIEWER))
							existingReviewerList.add(wfm.getApplicationUser().getId());
						if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.FIRST_APPROVER))
							existingFirstApproverList.add(wfm.getApplicationUser().getId());
						if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.SECOND_APPROVER))
							existingSecondApproverList.add(wfm.getApplicationUser().getId());
						if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.TRAINING))
							existingSecondApproverList.add(wfm.getApplicationUser().getId());


					}

					List<Integer> addedAuthorList = null;
					if (sopVO.getAuthor() != null && sopVO.getAuthor().size() > 0) {
						addedAuthorList = new ArrayList<Integer>();
						for (SelectInputVO selVo : sopVO.getAuthor()) {
							addedAuthorList.add(selVo.getId());
						}
						addedAuthorList.removeAll(existingAuthorList);
					}
					List<Integer> addedReviewerList = null;
					if (sopVO.getReviewer() != null && sopVO.getReviewer().size() > 0) {
						addedReviewerList = new ArrayList<Integer>();
						for (SelectInputVO selVo : sopVO.getReviewer()) {
							addedReviewerList.add(selVo.getId());
						}
						addedReviewerList.removeAll(existingReviewerList);
					}
					List<Integer> addedFirstApproverList = null;
					if (sopVO.getFirstApprover() != null && sopVO.getFirstApprover().size() > 0) {
						addedFirstApproverList = new ArrayList<Integer>();
						for (SelectInputVO selVo : sopVO.getFirstApprover()) {
							addedFirstApproverList.add(selVo.getId());
						}
						addedFirstApproverList.removeAll(existingFirstApproverList);
					}
					List<Integer> addedSecondApproverList = null;
					if (sopVO.getSecondApprover() != null && sopVO.getSecondApprover().size() > 0) {
						addedSecondApproverList = new ArrayList<Integer>();
						for (SelectInputVO selVo : sopVO.getSecondApprover()) {
							addedSecondApproverList.add(selVo.getId());
						}
						addedSecondApproverList.removeAll(existingSecondApproverList);
					}

					List<Integer> removedAuthorList = new ArrayList<Integer>();
					if (sopVO.getAuthor() != null && sopVO.getAuthor().size() > 0) {
						removedAuthorList = new ArrayList<Integer>(existingAuthorList);
						for (SelectInputVO selVo : sopVO.getAuthor()) {
							removedAuthorList.remove(selVo.getId());
						}
					}
					List<Integer> removedReviewerList = new ArrayList<Integer>();
					if (sopVO.getReviewer() != null && sopVO.getReviewer().size() > 0) {
						removedReviewerList = new ArrayList<Integer>(existingReviewerList);
						for (SelectInputVO selVo : sopVO.getReviewer()) {
							removedReviewerList.remove(selVo.getId());
						}
					}
					List<Integer> removedFirstApproverList = new ArrayList<Integer>();
					if (sopVO.getFirstApprover() != null && sopVO.getFirstApprover().size() > 0) {
						removedFirstApproverList = new ArrayList<Integer>(existingFirstApproverList);
						for (SelectInputVO selVo : sopVO.getFirstApprover()) {
							removedFirstApproverList.remove(selVo.getId());
						}
					}
					List<Integer> removedSecondApproverList = new ArrayList<Integer>();
					if (sopVO.getSecondApprover() != null && sopVO.getSecondApprover().size() > 0) {
						removedSecondApproverList = new ArrayList<Integer>(existingSecondApproverList);
						for (SelectInputVO selVo : sopVO.getSecondApprover()) {
							removedSecondApproverList.remove(selVo.getId());
						}
					}

					List<WorkFlowMetrix> tempworkFlowList = new ArrayList<WorkFlowMetrix>();

					for (WorkFlowMetrix wfm : workFlowList) {
						if ((wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.AUTHOR)
								&& removedAuthorList.contains(wfm.getApplicationUser().getId()))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.REVIEWER)
										&& removedReviewerList.contains(wfm.getApplicationUser().getId()))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.FIRST_APPROVER)
										&& removedFirstApproverList.contains(wfm.getApplicationUser().getId()))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.SECOND_APPROVER)
										&& removedSecondApproverList.contains(wfm.getApplicationUser().getId()))) {
							removedworkFlowList.add(wfm);
						} else {
							tempworkFlowList.add(wfm);
						}

					}
					if (removedworkFlowList != null && removedworkFlowList.size() > 0) {
						for (WorkFlowMetrix wfm : removedworkFlowList) {
							wfm.setAuditLog(auditLog);
						}
					}
					workFlowList.clear();
					workFlowList.addAll(tempworkFlowList);

					WorkFlowMetrix workFlow = new WorkFlowMetrix();

					if (sopVO.getAuthor() != null && sopVO.getAuthor().size() >= 0) {
						for (SelectInputVO userVo : sopVO.getAuthor()) {
							if (!existingAuthorList.contains(userVo.getId())) {
								workFlow = new WorkFlowMetrix();
								ApplicationUser author = new ApplicationUser();
								author.setId(userVo.getId());
								workFlow.setApplicationUser(author);
								ApplicationModuleStep moduleStep = applicationModuleStepRepository
										.getByModuleIdAndStepKey(EntityTypes.SOP, StepKeys.AUTHOR, true);
								workFlow.setApplicationModuleStep(moduleStep);
								workFlow.setAuditLog(auditLog);
								workFlow.setEntityId(sop.getId());
								workFlow.setEntityType(EntityTypes.SOP);
								workFlowList.add(workFlow);
							}
						}
					}

					if (sopVO.getReviewer() != null && sopVO.getReviewer().size() >= 0) {
						for (SelectInputVO userVo : sopVO.getReviewer()) {
							if (!existingReviewerList.contains(userVo.getId())) {
								workFlow = new WorkFlowMetrix();
								ApplicationUser reviewer = new ApplicationUser();
								reviewer.setId(userVo.getId());
								workFlow.setApplicationUser(reviewer);
								ApplicationModuleStep moduleStep = applicationModuleStepRepository
										.getByModuleIdAndStepKey(EntityTypes.SOP, StepKeys.REVIEWER, true);
								workFlow.setApplicationModuleStep(moduleStep);
								workFlow.setAuditLog(auditLog);
								workFlow.setEntityId(sop.getId());
								workFlow.setEntityType(EntityTypes.SOP);
								workFlowList.add(workFlow);
							}
						}
					}

					if (sopVO.getFirstApprover() != null && sopVO.getFirstApprover().size() >= 0) {
						for (SelectInputVO userVo : sopVO.getFirstApprover()) {
							if (!existingFirstApproverList.contains(userVo.getId())) {
								workFlow = new WorkFlowMetrix();
								ApplicationUser firstApprover = new ApplicationUser();
								firstApprover.setId(userVo.getId());
								workFlow.setApplicationUser(firstApprover);
								ApplicationModuleStep moduleStep = applicationModuleStepRepository
										.getByModuleIdAndStepKey(EntityTypes.SOP, StepKeys.FIRST_APPROVER, true);
								workFlow.setApplicationModuleStep(moduleStep);
								workFlow.setAuditLog(auditLog);
								workFlow.setEntityId(sop.getId());
								workFlow.setEntityType(EntityTypes.SOP);
								workFlowList.add(workFlow);
							}
						}
					}

					if (sopVO.getSecondApprover() != null && sopVO.getSecondApprover().size() >= 0) {
						for (SelectInputVO userVo : sopVO.getSecondApprover()) {
							if (!existingSecondApproverList.contains(userVo.getId())) {
								workFlow = new WorkFlowMetrix();
								ApplicationUser secondApprover = new ApplicationUser();
								secondApprover.setId(userVo.getId());
								workFlow.setApplicationUser(secondApprover);
								ApplicationModuleStep moduleStep = applicationModuleStepRepository
										.getByModuleIdAndStepKey(EntityTypes.SOP, StepKeys.SECOND_APPROVER, true);
								workFlow.setApplicationModuleStep(moduleStep);
								workFlow.setAuditLog(auditLog);
								workFlow.setEntityId(sop.getId());
								workFlow.setEntityType(EntityTypes.SOP);
								workFlowList.add(workFlow);
							}
						}
					}
				} else {
					for (WorkFlowMetrix wfm : workFlowList) {
						if ((wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.INITIATOR)
								&& appStatus.getEnumKey().equals(StatusTypes.INITIATE))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.AUTHOR)
										&& appStatus.getEnumKey().equals(StatusTypes.DRAFT))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.REVIEWER)
										&& appStatus.getEnumKey().equals(StatusTypes.IN_REVIEW))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.FIRST_APPROVER)
										&& appStatus.getEnumKey().equals(StatusTypes.FIRST_APPROVED))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.SECOND_APPROVER)
										&& appStatus.getEnumKey().equals(StatusTypes.SECOND_APPROVED))
								|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.TRAINING)
										&& appStatus.getEnumKey().equals(StatusTypes.TRAINING))) {
							if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()
									&& wfm.getApplicationUser().getId() == sopVO.getSignedBy()) {
								wfm.setSignedFlag(true);
								wfm.setSignedDate(dt);
								wfm.setAuditLog(auditLog);
								break;
							}
						}
					}
				}
				if (sopVO.getComments() != null && !sopVO.getComments().equals("")) {
					for (WorkFlowMetrix wfm : workFlowList) {

						if (wfm.getApplicationUser().getId() == user.getId() || wfm.getApplicationUser()
								.getId() == (sopVO.getSignedBy() == null ? 0 : sopVO.getSignedBy())) {
							if ((wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.INITIATOR)
									&& appStatus.getEnumKey().equals(StatusTypes.INITIATE))
									|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.AUTHOR)
											&& appStatus.getEnumKey().equals(StatusTypes.DRAFT))
									|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.REVIEWER)
											&& appStatus.getEnumKey().equals(StatusTypes.IN_REVIEW))
									|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.FIRST_APPROVER)
											&& appStatus.getEnumKey().equals(StatusTypes.FIRST_APPROVED))
									|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.SECOND_APPROVER)
											&& appStatus.getEnumKey().equals(StatusTypes.SECOND_APPROVED))
									|| (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.TRAINING)
											&& appStatus.getEnumKey().equals(StatusTypes.TRAINING))) {

								wfm.setAuditLog(auditLog);

								if (wfm.getApplicationComments() != null && wfm.getApplicationComments().size() > 0) {
									boolean iscommentFreezed = false;
									for (ApplicationComment comment : wfm.getApplicationComments()) {
										if (comment.getFreeze() == null || !comment.getFreeze()) {
											comment.setComments(sopVO.getComments());
											if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
												comment.setFreeze(true);
											} else {
												comment.setFreeze(false);
											}
											iscommentFreezed = false;
											break;
										} else {
											iscommentFreezed = true;
										}
									}
									if (iscommentFreezed) {
										ApplicationComment comment = new ApplicationComment();
										comment.setComments(sopVO.getComments());
										comment.setAuditLog(auditLog);
										comment.setUpdatedDate(dt);
										if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
											comment.setFreeze(true);
										} else {
											comment.setFreeze(false);
										}
										wfm.addApplicationComment(comment);
									}

								} else {
									ApplicationComment comment = new ApplicationComment();
									comment.setComments(sopVO.getComments());
									comment.setAuditLog(auditLog);
									comment.setUpdatedDate(dt);
									if (sopVO.getSignedFlag() != null && sopVO.getSignedFlag()) {
										comment.setFreeze(true);
									} else {
										comment.setFreeze(false);
									}
									wfm.addApplicationComment(comment);
								}

								break;
							}
						}
					}
				}

				auditLog.setEntityId(sop.getId());
				sop.setAuditLog(null);
				workFlowList = workflowMetrixRepository.saveAll(workFlowList);
				if (removedworkFlowList != null && removedworkFlowList.size() > 0) {
					workflowMetrixRepository.deleteAll(removedworkFlowList);
				}
				if (sopVO.getTriggerData() != null && sopVO.getTriggerData().getRepeatFrequency() != null
						&& !sopVO.getTriggerData().getRepeatFrequency().equals("")) {
					ApplicationScheduler triggerDetails = applicationSchedulerRepository
							.getSchedulerDetails(EntityTypes.SOP, sop.getId());
					
					if(triggerDetails == null) {
						triggerDetails = new ApplicationScheduler();
					}
					String repeatStr = "";
					for(String i: sopVO.getTriggerData().getRepeatOn()) {
						repeatStr = repeatStr + i +",";
					}
					triggerDetails.setRepeatOn(repeatStr.substring(0, repeatStr.lastIndexOf(",")));
					triggerDetails.setRepeatCount(sopVO.getTriggerData().getRepeatCount());
					triggerDetails.setFrequencyType(sopVO.getTriggerData().getRepeatFrequency());
					triggerDetails.setStartDate(sopVO.getTriggerData().getStartDate());
					triggerDetails.setEndDate(sopVO.getTriggerData().getEndDate());
					triggerDetails.setStartTime(sopVO.getTriggerData().getStartTime());
					triggerDetails.setEndTime(sopVO.getTriggerData().getEndTime());
					triggerDetails.setAuditLog(auditLog);
					triggerDetails.setEntityId(sop.getId());
					triggerDetails.setEntityType(EntityTypes.SOP);

					applicationSchedulerRepository.save(triggerDetails);
				}
				auditLog.setEntityId(sop.getId());
				sop.setAuditLog(null);
				for (WorkFlowMetrix wfm : workFlowList) {
					wfm.setAuditLog(null);
					if (wfm.getApplicationComments() != null) {
						for (ApplicationComment comment : wfm.getApplicationComments()) {
							comment.setAuditLog(null);
						}
					}
					workflowMetrixRepository.save(wfm);
				}
				if (sopVO.getRejected() == null || !sopVO.getRejected()) {
					if (sopVO.getDocument() != null && sopVO.getDocument().size() > 0 && sop.getCompletedStep() != null
							&& sop.getCompletedStep() == 2) {
						for (DocumentVO doc : sopVO.getDocument()) {
							ApplicationDocuments ad = applicationDocumentsRepository.getById(doc.getId());
							ad.setEntityId(sop.getId());
							ad.setEntityType(EntityTypes.SOP);
							applicationDocumentsRepository.save(ad);
						}
					}
				}
			} else {
				sop.setIsActive(isActive);

				sop.setApplicationUser2(user);
				sop.setUpdatedDate(dt);
				sop = sopRepository.save(sop);
			}

			convertEntityToDTO(sop, sopGetVO, username);
		} else {
			throw new ResourceNotFoundException("Sop type not found for this id :: " + id);
		}
		
		return sopGetVO;
	}

	private void convertEntityToDTO(Sop sop, SopGetVO sopVo, String username) {
		sopVo.setId(sop.getId());
		sopVo.setName(sop.getName());
		sopVo.setIsActive(sop.getIsActive());
		sopVo.setNumber(sop.getNumber());
		SelectInputVO ivo = new SelectInputVO();
		ivo.setId(sop.getSopType().getId());
		ivo.setName(sop.getSopType().getName());
		sopVo.setTypeId(ivo);
		sopVo.setCreatedDate(sop.getCreatedDate());
		sopVo.setUpdatedDate(sop.getUpdatedDate());
		sopVo.setCreatedBy(sop.getApplicationUser1().getId());
		sopVo.setCreatedByUserName(
				sop.getApplicationUser1().getFirstName() + " " + sop.getApplicationUser1().getLastName());
		sopVo.setUpdatedBy(sop.getApplicationUser2() != null ? sop.getApplicationUser2().getId() : null);
		sopVo.setUpdatedByUserName(sop.getApplicationUser2() != null
				? (sop.getApplicationUser2().getFirstName() + " " + sop.getApplicationUser2().getLastName())
				: null);
		sopVo.setLabId(sop.getLab().getId());
		sopVo.setLabName(sop.getLab().getName());
		
		List<Object[]> objList = applicationDocumentsRepository.getAppdocList(sopVo.getLabId(), EntityTypes.SOP,
				sop.getId(), true);
		List<DocumentVO> selectVoList = null;
		if (objList != null) {
			selectVoList = new ArrayList<DocumentVO>();
			for (Object[] obj : objList) {
				DocumentVO selectVo = new DocumentVO();
				selectVo.setId((int) obj[0]);
				selectVo.setName((String) obj[1]);
				selectVoList.add(selectVo);
			}
		}
		sopVo.setDocument(selectVoList);
		//sopVo.setDocumentName(sop.getDocumentName());
		sopVo.setVersion(sop.getVersion());
		sopVo.setRevision(sop.getRevision());
		sopVo.setCurrentStep(sop.getCurrentStep());
		sopVo.setCompletedStep(sop.getCompletedStep());
		StepKeys stepKey = StepKeys.INITIATOR;
		if (sop.getApplicationStatus() != null) {
			sopVo.setStatus(sop.getApplicationStatus().getId());
			sopVo.setStatusName(sop.getApplicationStatus().getName());

			if (sop.getApplicationStatus().getEnumKey().equals(StatusTypes.INITIATE)) {
				stepKey = StepKeys.INITIATOR;
			} else if (sop.getApplicationStatus().getEnumKey().equals(StatusTypes.DRAFT)) {
				stepKey = StepKeys.AUTHOR;
			} else if (sop.getApplicationStatus().getEnumKey().equals(StatusTypes.IN_REVIEW)) {
				stepKey = StepKeys.REVIEWER;
			} else if (sop.getApplicationStatus().getEnumKey().equals(StatusTypes.FIRST_APPROVED)) {
				stepKey = StepKeys.FIRST_APPROVER;
			} else if (sop.getApplicationStatus().getEnumKey().equals(StatusTypes.SECOND_APPROVED)) {
				stepKey = StepKeys.SECOND_APPROVER;
			} else if (sop.getApplicationStatus().getEnumKey().equals(StatusTypes.TRAINING)) {
				stepKey = StepKeys.TRAINING;
			}
		} else {
			stepKey = StepKeys.INITIATOR;
		}
		ApplicationModuleStep moduleStep = applicationModuleStepRepository.getByModuleIdAndStepKey(EntityTypes.SOP,
				stepKey, true);

		sopVo.setStepId(moduleStep.getId());
		sopVo.setStepKey(moduleStep.getStepKey().toString());

		List<WorkflowVO> workflowList = new ArrayList<WorkflowVO>();
		List<WorkFlowMetrix> workFlowList = workflowMetrixRepository.findByEntityIdAndEntityType(sop.getId(),
				EntityTypes.SOP);
		objList = workflowMetrixRepository.getApproverList(EntityTypes.SOP.toString(), sop.getId(), true);
		
		List<ApproversVO> approverList = null;
		Map<Integer, String> approversIdList = null;
		if (objList != null) {
			approversIdList = new LinkedHashMap<Integer, String>();
			approverList = new ArrayList<ApproversVO>();
			for (Object[] obj : objList) {
				ApproversVO selectVo = new ApproversVO();
				selectVo.setId((int) obj[0]);
				approversIdList.put((int) obj[0], (String) obj[5]);
				selectVo.setUsername((String) obj[1]);
				selectVo.setName((String) obj[2] + " " + (String) obj[3]);
				selectVo.setSignedDate((Timestamp) obj[4]);
				selectVo.setStepKey((String) obj[5]);
				approverList.add(selectVo);
			}
		}
		sopVo.setApproverList(approverList);
		
		List<SelectInputVO> author = new ArrayList<SelectInputVO>();
		List<SelectInputVO> reviewer = new ArrayList<SelectInputVO>();
		List<SelectInputVO> firstApprover = new ArrayList<SelectInputVO>();
		List<SelectInputVO> secondApprover = new ArrayList<SelectInputVO>();
		for (WorkFlowMetrix wfm : workFlowList) {
			SelectInputVO selectVo = new SelectInputVO();
			WorkflowVO wfvo = new WorkflowVO();
			wfvo.setAssignedUser(wfm.getApplicationUser().getId());
			wfvo.setAssignedUserName((wfm.getApplicationUser().getFullName() != null
					&& !wfm.getApplicationUser().getFullName().equals("")) ? wfm.getApplicationUser().getFullName()
							: wfm.getApplicationUser().getFirstName() + " " + wfm.getApplicationUser().getLastName());
			wfvo.setModuleStepId(wfm.getApplicationModuleStep().getId());
			wfvo.setStepKey(wfm.getApplicationModuleStep().getStepKey().toString());
			wfvo.setSignedDate(wfm.getSignedDate());
			wfvo.setSignedFlag(wfm.getSignedFlag());
			if (wfm.getSignedFlag() != null && wfm.getSignedFlag()) {
				sopVo.setSignedBy(wfm.getApplicationUser().getId());
				sopVo.setSignedByName(
						wfm.getApplicationUser().getFirstName() + " " + wfm.getApplicationUser().getLastName());
				sopVo.setSignedDate(wfm.getSignedDate());
			}
			//wfvo.setWorkFlowOrder(wfm.getWorkFlowOrder());
			if (wfm.getApplicationModuleStep().getStepKey().equals(stepKey)
					&& username.equalsIgnoreCase(wfm.getApplicationUser().getUsername())) {
				sopVo.setEditableFlag(true);
			}
			if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.INITIATOR)) {
				String comments = "";
				for (ApplicationComment comment : wfm.getApplicationComments()) {
					comments = comments + comment.getComments()+"\n";
				}
				sopVo.setComments(comments);
			} else if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.AUTHOR)) {
				selectVo.setId(wfm.getApplicationUser().getId());
				selectVo.setName((wfm.getApplicationUser().getFullName() != null
						&& !wfm.getApplicationUser().getFullName().equals("")) ? wfm.getApplicationUser().getFullName()
								: wfm.getApplicationUser().getFirstName() + " "
										+ wfm.getApplicationUser().getLastName());
				;
				author.add(selectVo);
			} else if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.REVIEWER)) {
				selectVo.setId(wfm.getApplicationUser().getId());
				selectVo.setName((wfm.getApplicationUser().getFullName() != null
						&& !wfm.getApplicationUser().getFullName().equals("")) ? wfm.getApplicationUser().getFullName()
								: wfm.getApplicationUser().getFirstName() + " "
										+ wfm.getApplicationUser().getLastName());
				reviewer.add(selectVo);
			} else if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.FIRST_APPROVER)) {
				selectVo.setId(wfm.getApplicationUser().getId());
				selectVo.setName((wfm.getApplicationUser().getFullName() != null
						&& !wfm.getApplicationUser().getFullName().equals("")) ? wfm.getApplicationUser().getFullName()
								: wfm.getApplicationUser().getFirstName() + " "
										+ wfm.getApplicationUser().getLastName());
				firstApprover.add(selectVo);
			} else if (wfm.getApplicationModuleStep().getStepKey().equals(StepKeys.SECOND_APPROVER)) {
				selectVo.setId(wfm.getApplicationUser().getId());
				selectVo.setName((wfm.getApplicationUser().getFullName() != null
						&& !wfm.getApplicationUser().getFullName().equals("")) ? wfm.getApplicationUser().getFullName()
								: wfm.getApplicationUser().getFirstName() + " "
										+ wfm.getApplicationUser().getLastName());
				secondApprover.add(selectVo);
			}
			if (!approversIdList.keySet().contains(wfm.getApplicationUser().getId())) {
				ApproversVO approverVo = new ApproversVO();
				approverVo.setId(wfm.getApplicationUser().getId());
				approverVo.setUsername(wfm.getApplicationUser().getUsername());
				approverVo.setName(
						wfm.getApplicationUser().getFirstName() + " " + wfm.getApplicationUser().getLastName());
				approverVo.setStepKey(wfm.getApplicationModuleStep().getStepKey().toString());
				if (sopVo.getApproverList() != null)
					sopVo.getApproverList().add(approverVo);
			} else if (approversIdList.keySet().contains(wfm.getApplicationUser().getId())
					&& !(wfm.getApplicationModuleStep().getStepKey().toString()
							.equals(approversIdList.get(wfm.getApplicationUser().getId())))) {
				ApproversVO approverVo = new ApproversVO();
				approverVo.setId(wfm.getApplicationUser().getId());
				approverVo.setUsername(wfm.getApplicationUser().getUsername());
				approverVo.setName(
						wfm.getApplicationUser().getFirstName() + " " + wfm.getApplicationUser().getLastName());
				approverVo.setStepKey(wfm.getApplicationModuleStep().getStepKey().toString());
				if (sopVo.getApproverList() != null)
					sopVo.getApproverList().add(approverVo);
			}
			workflowList.add(wfvo);
		}
		sopVo.setAuthor(author);
		sopVo.setReviewer(reviewer);
		sopVo.setFirstApprover(firstApprover);
		sopVo.setSecondApprover(secondApprover);
		ApplicationScheduler triggerDetails = applicationSchedulerRepository.getSchedulerDetails(EntityTypes.SOP,
				sop.getId());
		if (triggerDetails != null) {
			SchedulerVO sVO = new SchedulerVO();
			sVO.setRepeatFrequency(triggerDetails.getFrequencyType());
			if(triggerDetails.getRepeatOn() != null && !triggerDetails.getRepeatOn().equals("")) {
				String[] strarr = triggerDetails.getRepeatOn().split(",");
				sVO.setRepeatOn(Arrays.asList(strarr));
			}
			
			sVO.setRepeatCount(triggerDetails.getRepeatCount());
			sVO.setStartDate(triggerDetails.getStartDate());
			sVO.setEndDate(triggerDetails.getEndDate());
			sVO.setStartTime(triggerDetails.getStartTime());
			sVO.setEndTime(triggerDetails.getEndTime());

			sopVo.setTriggerData(sVO);

		}
		sopVo.setWorkflowList(workflowList);
	}

	@Override
	public List<CommentsVO> sopCommentsById(int labid, Integer id) throws Throwable {
		List<CommentsVO> cVOList = new ArrayList<CommentsVO>();
		List<ApplicationComment> commentsList = commentsRepository.getCommentsBySopId(labid, id,
				EntityTypes.SOP, true);

		for (ApplicationComment ac : commentsList) {
			CommentsVO cVO = new CommentsVO();
			cVO.setComments(ac.getComments());
			cVO.setUpdatedDate(ac.getUpdatedDate());
			cVO.setFreezed(ac.getFreeze()==null?false:ac.getFreeze());
			cVO.setUsername(ac.getWorkFlowMetrix().getApplicationUser().getFirstName() + " "
					+ ac.getWorkFlowMetrix().getApplicationUser().getLastName());
			cVOList.add(cVO);
		}

		return cVOList;
	}

	@Override
	public DocumentVO sopDocument(int labid, Integer id) throws Throwable {
		DocumentVO dvo = new DocumentVO();
		Sop sop = sopRepository.findByIdAndLabId(id, labid, true);
		dvo.setId(sop.getId());
		dvo.setLabId(sop.getLab().getId());

		return dvo;
	}
	
	public SopGetVO saveComments(int labId, CommentsVO comments)
			throws Throwable {
		List<WorkFlowMetrix> wfmList = workflowMetrixRepository.findByEntityIdAndEntityType(comments.getEntityId(),
				comments.getEntityType());
		for (WorkFlowMetrix wfm : wfmList) {
			if (wfm.getApplicationUser().getUsername().equalsIgnoreCase(comments.getUsername())) {
				Date dt = new Date();
				ApplicationUser user = userRepository.findByUsernameIgnoreCase(comments.getUsername());
				AuditLog auditLog = new AuditLog();
				auditLog.setActionType("CREATE");

				ApplicationModule appModule = applicationModuleRepository.findByEnumKey(comments.getEntityType());

				auditLog.setApplicationModule(appModule);
				auditLog.setCreatedDate(dt);

				auditLog.setApplicationUser(user);

				ApplicationComment appComment = new ApplicationComment();
				appComment.setAuditLog(auditLog);
				appComment.setComments(comments.getComments());
				appComment.setWorkFlowMetrix(wfm);
				appComment.setFreeze(true);
				appComment.setUpdatedDate(dt);

				commentsRepository.save(appComment);

				break;
			}
		}
		
		return getSopById(comments.getEntityId(), comments.getUsername());
	}
	
}
