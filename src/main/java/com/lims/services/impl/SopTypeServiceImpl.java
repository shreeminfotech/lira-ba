package com.lims.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.lims.dao.domain.ApplicationModule;
import com.lims.dao.domain.ApplicationUser;
import com.lims.dao.domain.AuditLog;
import com.lims.dao.domain.Lab;
import com.lims.dao.domain.SopType;
import com.lims.dao.domain.SopTypeVisibility;
import com.lims.exceptions.ResourceNotFoundException;
import com.lims.modal.SelectInputVO;
import com.lims.modal.SopTypeGetVO;
import com.lims.modal.SopTypePostVO;
import com.lims.modal.SummaryResponse;
import com.lims.modal.VisibilityLevel;
import com.lims.repositories.SopTypesRepository;
import com.lims.repositories.UserRepository;
import com.lims.services.SopTypeService;

@Service
public class SopTypeServiceImpl implements SopTypeService {

	@Autowired
	private SopTypesRepository sopTypesRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public SummaryResponse<SopTypeGetVO> sopTypeList(Integer labId, String searchval, Pageable page, String username) throws Throwable {
		SummaryResponse<SopTypeGetVO> summary = new SummaryResponse<SopTypeGetVO>();
		Page<SopType> pageObj = null;
		List<SopTypeGetVO> sopTypeVOList = new ArrayList<SopTypeGetVO>();

		// summary.setTotalRecords(sopTypesRepository.getSopTypesCount(manageObj));
		pageObj = sopTypesRepository.sopTypeList(labId, searchval, page, username, true);
		if (pageObj != null && pageObj.getTotalElements() > 0) {
			summary.setTotalRecords(pageObj.getTotalElements());
			summary.setTotalPages(pageObj.getTotalPages());
			for (SopType sopType : pageObj.getContent()) {
				SopTypeGetVO sopTypeVo = new SopTypeGetVO();
				convertEntityToDTO(sopType, sopTypeVo);

				sopTypeVOList.add(sopTypeVo);
			}
			summary.setEntities(sopTypeVOList);
		}

		return summary;
	}

	@Override
	public SopTypeGetVO getSopTypeById(Integer id) throws Throwable {
		Optional<SopType> SopTypesData = sopTypesRepository.findById(id);
		if (SopTypesData.isPresent()) {
			SopTypeGetVO sopTypeVo = new SopTypeGetVO();
			SopType sopType = SopTypesData.get();
			convertEntityToDTO(sopType, sopTypeVo);
			return sopTypeVo;
		} else {
			return null;
		}
	}

	/*
	 * @Override public List<SopTypeVO> sopTypeListByLabId(Integer id) throws
	 * Throwable { List<SopType> sopTypesList = sopTypesRepository.findByLabId(id,
	 * true); List<SopTypeVO> sopTypeVoList = new ArrayList<SopTypeVO>(); if
	 * (sopTypesList != null && sopTypesList.size() > 0) { for (SopType s :
	 * sopTypesList) { SopTypeVO sopTypeVo = new SopTypeVO(); convertEntityToDTO(s,
	 * sopTypeVo); sopTypeVoList.add(sopTypeVo); } return sopTypeVoList; } else {
	 * return null; } }
	 */

	@Override
	public SopTypeGetVO createSopType(Integer labId, SopTypePostVO sopTypes, String username) throws Throwable {
		Date dt = new Date();

		AuditLog auditLog = new AuditLog();
		auditLog.setActionType("CREATE");

		ApplicationModule appModule = new ApplicationModule();
		appModule.setId(2);

		auditLog.setApplicationModule(appModule);
		auditLog.setCreatedDate(dt);

		ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);

		auditLog.setApplicationUser(user);

		SopType sopType = new SopType();

		sopType.setName(sopTypes.getName());
		sopType.setDescription(sopTypes.getDescription());
		sopType.setColorCode(sopTypes.getColorCode());
		sopType.setSystemDefault(sopTypes.getSystemDefault());
		sopType.setVisibility(sopTypes.getVisibilityLevel());
		
		SopTypeVisibility sopv = new SopTypeVisibility();
		sopv.setApplicationUser(user);
		sopv.setAuditLog(auditLog);
		sopType.addSopTypeVisibility(sopv);

		if (sopTypes.getVisibilityLevel() != null && sopTypes.getVisibilityLevel().equalsIgnoreCase("USERS")) {
			if (sopTypes.getUserList() != null && sopTypes.getUserList().size() > 0) {
				for (SelectInputVO userVo : sopTypes.getUserList()) {
					sopv = new SopTypeVisibility();
					ApplicationUser user1 = new ApplicationUser();
					user1.setId(userVo.getId());
					sopv.setApplicationUser(user1);
					sopv.setAuditLog(auditLog);
					sopType.addSopTypeVisibility(sopv);
				}
			}
		}
		sopType.setApplicationUser1(user);
		sopType.setCreatedDate(dt);
		sopType.setAuditLog(auditLog);
		sopType.setIsActive(true);

		Lab lab = new Lab();
		lab.setId(labId);
		sopType.setLab(lab);
		sopType = sopTypesRepository.save(sopType);
		auditLog.setEntityId(sopType.getId());
		sopType.setAuditLog(null);
		SopTypeGetVO sopTypeVo = new SopTypeGetVO();
		Integer id = sopType.getId();
		sopTypesRepository.flush();
		SopType tempSopType = sopTypesRepository.getById(id);
		convertEntityToDTO(tempSopType, sopTypeVo);
		return sopTypeVo;
	}

	@Override
	public SopTypeGetVO updateSopType(Integer labId, int id, SopTypePostVO sopTypeVo, Boolean isActive, String username)
			throws Throwable {
		Optional<SopType> sopTypeData = sopTypesRepository.findById(id);
		SopTypeGetVO sopTypeGetVo = new SopTypeGetVO();
		if (sopTypeData.isPresent()) {
			
			Date dt = new Date();

			AuditLog auditLog = new AuditLog();
			auditLog.setActionType("MODIFY");

			ApplicationModule appModule = new ApplicationModule();
			appModule.setId(2);

			auditLog.setApplicationModule(appModule);
			auditLog.setCreatedDate(dt);

			ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);

			auditLog.setApplicationUser(user);

			SopType sopType = sopTypeData.get();
			if (isActive) {
				if(StringUtils.hasLength(sopTypeVo.getName()))
					sopType.setName(sopTypeVo.getName());
				if(StringUtils.hasLength(sopTypeVo.getDescription()))
					sopType.setDescription(sopTypeVo.getDescription());
				if(StringUtils.hasLength(sopTypeVo.getColorCode()))
					sopType.setColorCode(sopTypeVo.getColorCode());
				if(sopType.getSystemDefault() != sopTypeVo.getSystemDefault())
					sopType.setSystemDefault(sopTypeVo.getSystemDefault());
				if (StringUtils.hasLength(sopTypeVo.getVisibilityLevel())
						&& sopTypeVo.getVisibilityLevel().equalsIgnoreCase("USERS")) {
					List<Integer> existingUsers = new ArrayList<Integer>();
					if(sopType.getSopTypeVisibilities() != null && sopType.getSopTypeVisibilities().size() > 0) {
						for (SopTypeVisibility stv : sopType.getSopTypeVisibilities()) {
							existingUsers.add(stv.getApplicationUser().getId());
						}
					}
					if (sopTypeVo.getUserList() != null) {
						SelectInputVO userVo = new SelectInputVO();
						userVo.setId(user.getId());
						userVo.setName(user.getFirstName() + " " + user.getLastName());
						sopTypeVo.getUserList().add(userVo);
					}
					List<Integer> addedUsers = new ArrayList<Integer>();
					List<Integer> removedUsers = new ArrayList<Integer>();
					List<Integer> tempUsers = new ArrayList<Integer>();
					tempUsers.addAll(existingUsers);
					
					List<Integer> usersSent = new ArrayList<Integer>();
					for (SelectInputVO tepuserVo : sopTypeVo.getUserList()) {
						usersSent.add(tepuserVo.getId());
					}
					
					if (sopTypeVo.getUserList() != null && sopTypeVo.getUserList().size() > 0) {
						existingUsers.removeAll(usersSent);
						removedUsers.addAll(existingUsers);

						usersSent.removeAll(tempUsers);
						for (Integer userId : usersSent) {
							addedUsers.add(userId);
						}
					}
					
					if (addedUsers != null && addedUsers.size() > 0) {
						for (SelectInputVO sopVO : sopTypeVo.getUserList()) {
							if(addedUsers.contains(sopVO.getId())) {
							SopTypeVisibility sopv = new SopTypeVisibility();
							ApplicationUser user1 = new ApplicationUser();
							user1.setId(sopVO.getId());
							user1.setFullName(sopVO.getName());
							sopv.setApplicationUser(user1);
							sopv.setAuditLog(auditLog);
							sopType.addSopTypeVisibility(sopv);
							}
						}
					}
					List<SopTypeVisibility> removedSTVs = new ArrayList<SopTypeVisibility>();
					if(removedUsers != null && removedUsers.size() > 0) {
						for(SopTypeVisibility stv: sopType.getSopTypeVisibilities()) {
							if(removedUsers.contains(stv.getApplicationUser().getId())) {
								removedSTVs.add(stv);
							}
						}
					}
					for(SopTypeVisibility temp: removedSTVs) {
						sopType.removeSopTypeVisibility(temp);
					}
				} else {
					sopType.getSopTypeVisibilities().clear();
				}
				if(StringUtils.hasLength(sopTypeVo.getVisibilityLevel()))
						sopType.setVisibility(sopTypeVo.getVisibilityLevel());
				else {
					sopType.setVisibility("ALL");
				}


			}
			
			sopType.setIsActive(isActive);
			sopType.setAuditLog(auditLog);
			
			sopType.setApplicationUser2(user);
			sopType.setUpdatedDate(dt);
			sopType = sopTypesRepository.save(sopType);
			auditLog.setEntityId(sopType.getId());
			sopType.setAuditLog(null);
			convertEntityToDTO(sopType, sopTypeGetVo);
		} else {
			throw new ResourceNotFoundException("Sop type not found for this id :: " + id);
		}
		return sopTypeGetVo;
	}

	private void convertEntityToDTO(SopType sopType, SopTypeGetVO sopTypeVo) {
		sopTypeVo.setId(sopType.getId());
		sopTypeVo.setName(sopType.getName());
		sopTypeVo.setIsActive(sopType.getIsActive());
		sopTypeVo.setDescription(sopType.getDescription());
		if(StringUtils.hasLength(sopType.getVisibility()))
			sopTypeVo.setVisibilityLevel(VisibilityLevel.valueOf(sopType.getVisibility()));
		sopTypeVo.setColorCode(sopType.getColorCode());
		sopTypeVo.setSystemDefault(sopType.getSystemDefault());
		if (sopType.getVisibility() != null && sopType.getVisibility().equalsIgnoreCase("USERS")) {
			if (sopType.getSopTypeVisibilities() != null && sopType.getSopTypeVisibilities().size() > 0) {
				List<SelectInputVO> userList = new ArrayList<SelectInputVO>();
				for (SopTypeVisibility sov : sopType.getSopTypeVisibilities()) {
					if (sopType.getApplicationUser1().getId() != sov.getApplicationUser().getId()) {
						SelectInputVO sivo = new SelectInputVO();
						sivo.setId(sov.getApplicationUser().getId());
						sivo.setName((sov.getApplicationUser().getFullName() != null
								&& !sov.getApplicationUser().getFullName().equals(""))
										? sov.getApplicationUser().getFullName()
										: sov.getApplicationUser().getFirstName() + " "
												+ sov.getApplicationUser().getLastName());

						userList.add(sivo);
					}
				}
				sopTypeVo.setUserList(userList);
			}
		}
		if(sopType.getSops() != null)
		sopTypeVo.setSopcount(sopType.getSops().size());
		sopTypeVo.setCreatedDate(sopType.getCreatedDate());
		sopTypeVo.setUpdatedDate(sopType.getUpdatedDate());
		sopTypeVo.setCreatedBy(sopType.getApplicationUser1().getId());
		sopTypeVo.setCreatedByUserName(sopType.getApplicationUser1().getUsername());
		sopTypeVo.setUpdatedBy(sopType.getApplicationUser2() != null ? sopType.getApplicationUser2().getId() : null);
		sopTypeVo.setUpdatedByUserName(sopType.getApplicationUser1().getUsername());
		sopTypeVo.setLabId(sopType.getLab().getId());
		sopTypeVo.setLabName(sopType.getLab().getName());
	}

	@Override
	public List<SelectInputVO> sopTypeDropdownByLabId(Integer id, String username) throws Throwable {
		List<Object[]> objList = sopTypesRepository.sopTypeDropdownByLabId(id, true, username);
		List<SelectInputVO> selectVoList = new ArrayList<SelectInputVO>();
		for (Object[] obj : objList) {
			SelectInputVO selectVo = new SelectInputVO();
			selectVo.setId((int) obj[0]);
			selectVo.setName((String) obj[1]);
			selectVoList.add(selectVo);
		}
		return selectVoList;
	}
}
