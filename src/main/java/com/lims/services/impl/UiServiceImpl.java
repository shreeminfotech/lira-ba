package com.lims.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lims.dao.domain.ApplicationComment;
import com.lims.dao.domain.ApplicationDocumentCommentsLog;
import com.lims.dao.domain.ApplicationModule;
import com.lims.dao.domain.ApplicationModuleStep;
import com.lims.dao.domain.ApplicationUser;
import com.lims.dao.domain.AuditLog;
import com.lims.dao.domain.WorkFlowMetrix;
import com.lims.enums.EntityTypes;
import com.lims.modal.CommentsVO;
import com.lims.modal.DocumentCommentsVO;
import com.lims.modal.ModuleStepVO;
import com.lims.repositories.ApplicationModuleRepository;
import com.lims.repositories.ApplicationModuleStepRepository;
import com.lims.repositories.CommentsRepository;
import com.lims.repositories.DocumentCommentsRepository;
import com.lims.repositories.UserRepository;
import com.lims.repositories.WorkflowMetrixRepository;
import com.lims.services.UiService;

@Service
public class UiServiceImpl implements UiService {

	@Autowired
	private ApplicationModuleStepRepository applicationModuleStepRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ApplicationModuleRepository applicationModuleRepository;
	
	@Autowired
	private DocumentCommentsRepository documentCommentsRepository;
	
	@Autowired
	private WorkflowMetrixRepository workflowMetrixRepository;

	@Override
	public List<ModuleStepVO> geSteps(EntityTypes module) throws Throwable {
		List<ApplicationModuleStep> objList = applicationModuleStepRepository.geSteps(module, true);
		List<ModuleStepVO> selectVoList = new ArrayList<ModuleStepVO>();
		for (ApplicationModuleStep a : objList) {
			ModuleStepVO selectVo = new ModuleStepVO();
			selectVo.setId(a.getId());
			selectVo.setStepOrder(a.getStepOrder());
			selectVo.setStepKey(a.getStepKey().toString());
			selectVo.setName(a.getStepDesc());
			selectVo.setClickable(a.getClickable());
			selectVoList.add(selectVo);
		}
		return selectVoList;
	}

	@Override
	public void updateDocumentComments(int labId, String username, EntityTypes entity, Integer id,
			List<DocumentCommentsVO> docCommVO) throws Throwable {
		Date dt = new Date();

		ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);
		AuditLog auditLog = new AuditLog();
		auditLog.setActionType("CREATE");

		ApplicationModule appModule = applicationModuleRepository.findByEnumKey(entity);

		auditLog.setApplicationModule(appModule);
		auditLog.setCreatedDate(dt);

		auditLog.setApplicationUser(user);

		List<ApplicationDocumentCommentsLog> commentsList = new ArrayList<ApplicationDocumentCommentsLog>();
		for(DocumentCommentsVO comVo: docCommVO) {
			ApplicationDocumentCommentsLog comDao = new ApplicationDocumentCommentsLog();
			comDao.setAuditLog(auditLog);
			comDao.setOldValue(comVo.getOldValue());
			comDao.setNewValue(comVo.getNewValue());
			comDao.setField("COMMENTS");
			
			commentsList.add(comDao);
		}
		if(commentsList.size() > 0) {
			documentCommentsRepository.saveAll(commentsList);
		}

	}
	
	
}
