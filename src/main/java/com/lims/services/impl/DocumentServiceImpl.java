package com.lims.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lims.dao.domain.ApplicationDocuments;
import com.lims.dao.domain.ApplicationUser;
import com.lims.dao.domain.Lab;
import com.lims.modal.DocumentVO;
import com.lims.repositories.ApplicationDocumentsRepository;
import com.lims.repositories.UserRepository;
import com.lims.services.DocumentService;

@Service
public class DocumentServiceImpl implements DocumentService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ApplicationDocumentsRepository applicationDocumentsRepository;
	
	public List<DocumentVO> uploadDocuments(Integer labId, MultipartFile[] files, String username) throws Throwable {
		
		List<DocumentVO> docVOList = new ArrayList<DocumentVO>();
		Date dt = new Date();
		
		Lab lab = new Lab();
		lab.setId(labId);
		ApplicationUser user = userRepository.findByUsernameIgnoreCase(username);
	      Arrays.asList(files).stream().forEach(file -> {
	    	  try {
	    		  ApplicationDocuments doc = new ApplicationDocuments();
				  doc.setContent(file.getBytes());
				  doc.setDocumentName(file.getOriginalFilename());
				  doc.setDocumentType(file.getContentType());
				  doc.setFilesize(file.getSize());
				  doc.setLab(lab);
				  doc.setUploadedBy(user);
				  doc.setUploadedDate(dt);
				  doc.setIsActive(true);
				  doc.setIsDeleted(false);
				  doc = applicationDocumentsRepository.save(doc);
				  
				  DocumentVO docVO = new DocumentVO();
				  docVO.setId(doc.getId());
	    		  docVO.setName(file.getOriginalFilename());
	    		  docVO.setDocumentType(file.getContentType());
	    		  docVO.setUploadedBy(user.getFirstName() + " " + user.getLastName());
	    		  docVO.setLabId(labId);
	    		  docVO.setUploadedDate(dt);
	    		  docVOList.add(docVO);
	    		  
			} catch (IOException e) {
				e.printStackTrace();
			}
	      });
		return docVOList;
	}
	
	public ApplicationDocuments getFile(Integer id) {
	    return applicationDocumentsRepository.findById(id).get();
	  }
	  
	  public Stream<ApplicationDocuments> getAllFiles() {
	    return applicationDocumentsRepository.findAll().stream();
	  }
	  
}
