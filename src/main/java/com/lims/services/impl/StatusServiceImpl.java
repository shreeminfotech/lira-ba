package com.lims.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lims.modal.SelectInputVO;
import com.lims.repositories.StatusRepository;
import com.lims.services.StatusService;

@Service
public class StatusServiceImpl implements StatusService{
	
	@Autowired
	private StatusRepository statusRepository;

	@Override
	public List<SelectInputVO> statusDropdownByModuleType(Integer labid, String module) throws Throwable {
		List<Object[]> objList = statusRepository.statusDropdownByModuleType(module);
		List<SelectInputVO> selectVoList = new ArrayList<SelectInputVO>();
		for (Object[] obj : objList) {
			SelectInputVO selectVo = new SelectInputVO();
			selectVo.setId((int) obj[0]);
			selectVo.setName((String) obj[1]);
			selectVoList.add(selectVo);
		}
		return selectVoList;
	}

}
