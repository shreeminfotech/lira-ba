package com.lims.controller;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lims.dao.domain.ApplicationDocuments;
import com.lims.enums.EntityTypes;
import com.lims.modal.DocumentCommentsVO;
import com.lims.modal.DocumentVO;
import com.lims.modal.Response;
import com.lims.services.DocumentService;
import com.lims.services.UiService;
import com.syncfusion.docio.WordDocument;
import com.syncfusion.ej2.wordprocessor.FormatType;
import com.syncfusion.ej2.wordprocessor.WordProcessorHelper;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/documents")
public class DocumentController {
	//List<DictionaryData> spellDictionary;
	//String personalDictPath;

	public DocumentController() throws Exception {

		//String jsonFilePath = "src/main/resources/spellcheck.json";
		//File file = ResourceUtils.getFile("classpath:spellcheck.json");

		//String jsonContent = new String(Files.readAllBytes(file.toPath()));//new String(Files.readAllBytes(Paths.get(jsonFilePath)), StandardCharsets.UTF_8);
		//JsonArray spellDictionaryItems = new Gson().fromJson(jsonContent, JsonArray.class);
		//personalDictPath = "src/main/resources/customDict.dic";
		/*spellDictionary = new ArrayList<DictionaryData>();
		for (int i = 0; i < spellDictionaryItems.size(); i++) {
			JsonObject spellCheckerInfo = spellDictionaryItems.get(i).getAsJsonObject();
			DictionaryData dict = new DictionaryData();

			if (spellCheckerInfo.has("LanguadeID"))
				dict.setLanguadeID(spellCheckerInfo.get("LanguadeID").getAsInt());
			if (spellCheckerInfo.has("DictionaryPath"))
				dict.setDictionaryPath("src/main/resources/" + spellCheckerInfo.get("DictionaryPath").getAsString());
			if (spellCheckerInfo.has("AffixPath"))
				dict.setAffixPath("src/main/resources/" + spellCheckerInfo.get("AffixPath").getAsString());
			spellDictionary.add(dict);
		}*/

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping("/wordeditor/test")
	public String test() {
		System.out.println("==== in test ====");
		return "{\"sections\":[{\"blocks\":[{\"inlines\":[{\"text\":\"Hello World\"}]}]}]}";
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/Import")
	public String importFile(@RequestParam("files") MultipartFile file) throws Exception {
		try {
			return WordProcessorHelper.load(file.getInputStream(), FormatType.Docx);
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"sections\":[{\"blocks\":[{\"inlines\":[{\"text\":" + e.getMessage() + "}]}]}]}";
		}
	}
	
	@Autowired
	UiService uiService;
	
	@PutMapping("/wordeditor/{entity}/{id}")
	public String updatecommentsLog(@RequestHeader("labid") int labid, @PathVariable("entity") EntityTypes entity,
			@PathVariable("id") int id, @RequestBody List<DocumentCommentsVO> docVO) throws Exception {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			uiService.updateDocumentComments(labid, username, entity, id, docVO);
		} catch (Throwable e) {
			e.printStackTrace();
			return "Update Failed...";
		}
		return "Sucessfully updated...";
	}
	
	
/*
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/SpellCheck")
	public String spellCheck(@RequestBody SpellCheckJsonData spellChecker) throws Exception {
		try {
			SpellChecker spellCheck = new SpellChecker(spellDictionary, personalDictPath);
			String data = spellCheck.getSuggestions(spellChecker.languageID, spellChecker.texttoCheck,
					spellChecker.checkSpelling, spellChecker.checkSuggestion, spellChecker.addWord);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"SpellCollection\":[],\"HasSpellingError\":false,\"Suggestions\":null}";
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/SpellCheckByPage")
	public String spellCheckByPage(@RequestBody SpellCheckJsonData spellChecker) throws Exception {
		try {
			SpellChecker spellCheck = new SpellChecker(spellDictionary, personalDictPath);
			String data = spellCheck.checkSpelling(spellChecker.languageID, spellChecker.texttoCheck);
			return data;
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"SpellCollection\":[],\"HasSpellingError\":false,\"Suggestions\":null}";
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/RestrictEditing")
	public String[] restrictEditing(@RequestBody CustomRestrictParameter param) throws Exception {
		if (param.passwordBase64 == "" && param.passwordBase64 == null)
			return null;
		return WordProcessorHelper.computeHash(param.passwordBase64, param.saltBase64, param.spinCount);
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/SystemClipboard")
	public String systemClipboard(@RequestBody CustomParameter param) {
		if (param.content != null && param.content != "") {
			try {
				return WordProcessorHelper.loadString(param.content, GetFormatType(param.type.toLowerCase()));
			} catch (Exception e) {
				return "";
			}
		}
		return "";
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/Save")
	public void save(@RequestBody SaveParameter data) throws Exception {
		try {
			String name = data.getFileName();
			String format = retrieveFileType(name);
			if (name == null || name.isEmpty()) {
				name = "Document1.docx";
			}
			WordDocument document = WordProcessorHelper.save(data.getContent());
			FileOutputStream fileStream = new FileOutputStream(name);
			document.save(fileStream, getWFormatType(format));
			fileStream.close();
			document.close();
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/ExportSFDT")
	public ResponseEntity<Resource> exportSFDT(@RequestBody SaveParameter data) throws Exception {
		try {
			String name = data.getFileName();
			String format = retrieveFileType(name);
			if (name == null || name.isEmpty()) {
				name = "Document1.docx";
			}
			WordDocument document = WordProcessorHelper.save(data.getContent());
			return saveDocument(document, format);
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping("/wordeditor/Export")
	public ResponseEntity<Resource> export(@RequestParam("data") MultipartFile data, String fileName) throws Exception {
		try {
			String name = fileName;
			String format = retrieveFileType(name);
			if (name == null || name.isEmpty()) {
				name = "Document1";
			}
			WordDocument document = new WordDocument(data.getInputStream(), com.syncfusion.docio.FormatType.Docx);
			return saveDocument(document, format);
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}
*/
	private ResponseEntity<Resource> saveDocument(WordDocument document, String format) throws Exception {
		String contentType = "";
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		com.syncfusion.docio.FormatType type = getWFormatType(format);
		switch (type.toString()) {
		case "Rtf":
			contentType = "application/rtf";
			break;
		case "WordML":
			contentType = "application/xml";
			break;
		case "Dotx":
			contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
			break;
		case "Html":
			contentType = "application/html";
			break;
		}
		document.save(outStream, type);
		ByteArrayResource resource = new ByteArrayResource(outStream.toByteArray());
		outStream.close();
		document.close();

		return ResponseEntity.ok().contentLength(resource.contentLength())
				.contentType(MediaType.parseMediaType(contentType)).body(resource);
	}

	private String retrieveFileType(String name) {
		int index = name.lastIndexOf('.');
		String format = index > -1 && index < name.length() - 1 ? name.substring(index) : ".docx";
		return format;
	}

	static com.syncfusion.docio.FormatType getWFormatType(String format) throws Exception {
		if (format == null || format.trim().isEmpty())
			throw new Exception("EJ2 WordProcessor does not support this file format.");
		switch (format.toLowerCase()) {
		case ".dotx":
			return com.syncfusion.docio.FormatType.Dotx;
		case ".docm":
			return com.syncfusion.docio.FormatType.Docm;
		case ".dotm":
			return com.syncfusion.docio.FormatType.Dotm;
		case ".docx":
			return com.syncfusion.docio.FormatType.Docx;
		case ".rtf":
			return com.syncfusion.docio.FormatType.Rtf;
		case ".html":
			return com.syncfusion.docio.FormatType.Html;
		case ".txt":
			return com.syncfusion.docio.FormatType.Txt;
		case ".xml":
			return com.syncfusion.docio.FormatType.WordML;
		default:
			throw new Exception("EJ2 WordProcessor does not support this file format.");
		}
	}

	static FormatType GetFormatType(String format) {
		switch (format) {
		case ".dotx":
		case ".docx":
		case ".docm":
		case ".dotm":
			return FormatType.Docx;
		case ".dot":
		case ".doc":
			return FormatType.Doc;
		case ".rtf":
			return FormatType.Rtf;
		case ".txt":
			return FormatType.Txt;
		case ".xml":
			return FormatType.WordML;
		case ".html":
			return FormatType.Html;
		default:
			return FormatType.Docx;
		}
	}
	
	 @Autowired
	  DocumentService documentService;
	 
	  @PostMapping("/upload")
	  public ResponseEntity<Object> uploadFiles(@RequestHeader("labid") int labid, @RequestParam("files") MultipartFile[] files) {
	    String message = "";
	    try {
	    	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
	     /* List<String> fileNames = new ArrayList<>();
	      Arrays.asList(files).stream().forEach(file -> {
	        storageService.save(file);
	        fileNames.add(file.getOriginalFilename());
	      });*/
			List<DocumentVO> documentVoList = documentService.uploadDocuments(labid, files, username);
	     // message = "Uploaded the files successfully: " + fileNames;
	      return ResponseEntity.status(HttpStatus.OK).body(documentVoList);
	    } catch (Throwable e) {
	    	e.printStackTrace();
	    	List<String> errorMsgs = new ArrayList<String>();
			errorMsgs.add("Fail to upload files!");
			Response errResp = new Response();
			errResp.setErrorMessages(errorMsgs);
	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(errResp);
	    }
	  }
	/*  @GetMapping("/files")
	  public ResponseEntity<List<FileInfo>> getListFiles() {
	    List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
	      String filename = path.getFileName().toString();
	      String url = MvcUriComponentsBuilder
	          .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
	      return new FileInfo(filename, url);
	    }).collect(Collectors.toList());
	    return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
	  }
	  */
	@GetMapping("/download/{id}")
	public ResponseEntity<byte[]> getFile(@PathVariable Integer id) {
		ApplicationDocuments appDoc = documentService.getFile(id);
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + appDoc.getDocumentName() + "\"")
				.body(appDoc.getContent());
	}
}
