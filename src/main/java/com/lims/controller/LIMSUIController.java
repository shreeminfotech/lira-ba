package com.lims.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lims.enums.EntityTypes;
import com.lims.modal.ModuleStepVO;
import com.lims.services.UiService;

@RestController
@RequestMapping("/ui")
public class LIMSUIController {
	
	@Autowired
	UiService uiService;
	
	@GetMapping("/workflow/step/module/{entityType}")
	public ResponseEntity<List<ModuleStepVO>> sopTypeList(@PathVariable("entityType") EntityTypes entityType) {
		try {
			List<ModuleStepVO> summaryResponse = uiService.geSteps(entityType);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
