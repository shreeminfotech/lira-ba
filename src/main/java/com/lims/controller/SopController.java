package com.lims.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lims.exceptions.ResourceNotFoundException;
import com.lims.modal.CommentsVO;
import com.lims.modal.DocumentVO;
import com.lims.modal.SopGetVO;
import com.lims.modal.SopPostVO;
import com.lims.modal.SopPutVO;
import com.lims.modal.SummaryResponse;
import com.lims.services.SopService;

@RestController
@RequestMapping("/api")
public class SopController {

	@Autowired
	SopService sopService;

	@GetMapping("/sop/soptype/{typeid}")
	public ResponseEntity<List<SopGetVO>> sopTypeList(@RequestHeader("labid") int labid,
			@PathVariable("typeid") Integer id) {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			List<SopGetVO> summaryResponse = sopService.sopListBySopTypeId(labid, id, username);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/sop/{id}/document")
	public ResponseEntity<DocumentVO> getDocument(@RequestHeader("labid") int labid,
			@PathVariable("id") Integer id) {
		try {
			DocumentVO summaryResponse = sopService.sopDocument(labid, id);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/sop/{id}/comments")
	public ResponseEntity<List<CommentsVO>> sopComments(@RequestHeader("labid") int labid,
			@PathVariable("id") Integer id) {
		try {
			List<CommentsVO> summaryResponse = sopService.sopCommentsById(labid, id);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private Sort.Direction getSortDirection(String direction) {
		if (direction.equals("asc")) {
			return Sort.Direction.ASC;
		} else if (direction.equals("desc")) {
			return Sort.Direction.DESC;
		}
		return Sort.Direction.ASC;
	}

	@GetMapping("/sop/manage")
	public ResponseEntity<SummaryResponse<SopGetVO>> sopList(@RequestHeader("labid") int labid,
			@RequestParam(required = false) Integer soptype, @RequestParam(required = false) String searchval,
			@RequestParam(required = false) Integer offset, @RequestParam(required = false) Integer size,
			@RequestParam(defaultValue = "id,desc") String[] sort) {
		try {
			List<Order> orders = new ArrayList<Order>();
			if (sort[0].contains(",")) {
				// will sort more than 2 fields
				// sortOrder="field, direction"
				for (String sortOrder : sort) {
					String[] _sort = sortOrder.split(",");
					orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
				}
			} else {
				// sort=[field, direction]
				orders.add(new Order(getSortDirection(sort[1]), sort[0]));
			}
			Pageable pagingSort = null;
			if (offset != null)
				pagingSort = PageRequest.of(offset, size, Sort.by(orders));
			
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			SummaryResponse<SopGetVO> summaryResponse = sopService.sopList(labid, soptype, searchval, pagingSort, username);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/sop/{id}")
	public ResponseEntity<SopGetVO> getSopById(@PathVariable("id") Integer id) {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			SopGetVO sop = sopService.getSopById(id, username);

			if (sop != null) {
				return new ResponseEntity<>(sop, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Throwable e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/sop")
	public ResponseEntity<SopGetVO> createSop(@RequestHeader("labid") int labid, @RequestBody SopPostVO sop) {
		List<String> errorMsgs = new ArrayList<String>();
		SopGetVO sopGetVO = new SopGetVO();
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			sopGetVO = sopService.createSop(labid, sop, username);
			if(sopGetVO != null && sopGetVO.getErrorMessages() != null && sopGetVO.getErrorMessages().size() > 0) {
				return new ResponseEntity<>(sopGetVO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(sopGetVO, HttpStatus.CREATED);
		} catch (Throwable e) {
			e.printStackTrace();
			if (e instanceof SQLException) {
				errorMsgs.add(e.getMessage());
			} else if (!e.getMessage().contains("Validation Error")) {
				errorMsgs.add("Internal error...");
			}
			sopGetVO.setErrorMessages(errorMsgs);
			return new ResponseEntity<>(sopGetVO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/sop/{id}")
	public ResponseEntity<SopGetVO> updateSop(@RequestHeader("labid") int labid, @PathVariable("id") int id,
			@RequestBody SopPutVO sop) {
		List<String> errorMsgs = new ArrayList<String>();
		SopGetVO sopGetVO = new SopGetVO();
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			sopGetVO = sopService.updateSop(labid, id, sop, true, username);
			if(sopGetVO != null && sopGetVO.getErrorMessages() != null && sopGetVO.getErrorMessages().size() > 0) {
				return new ResponseEntity<>(sopGetVO, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(sopGetVO, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			if (e instanceof SQLException || e instanceof ResourceNotFoundException) {
				errorMsgs.add(e.getMessage());
			} else if (!e.getMessage().contains("Validation Error")) {
				errorMsgs.add("Internal error...");
			}
			sopGetVO.setErrorMessages(errorMsgs);
			return new ResponseEntity<>(sopGetVO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/sop/{id}")
	public ResponseEntity<String> deleteSop(@RequestHeader("labid") int labid, @PathVariable("id") int id,
			@RequestBody SopPutVO sop) throws Throwable {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			sopService.updateSop(labid, id, sop, false, username);
			return new ResponseEntity<>("Deleted Successfully...", HttpStatus.OK);
		} catch (Throwable e) {
			if (e instanceof ResourceNotFoundException) {
				throw e;
			} else {
				return new ResponseEntity<>("Delete Failed...", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	/*
	 * @DeleteMapping("/sop") public ResponseEntity<HttpStatus> deleteSops() { try {
	 * // SopTypesRepository.deleteAll(); return new
	 * ResponseEntity<>(HttpStatus.NO_CONTENT); } catch (Exception e) { return new
	 * ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); }
	 * 
	 * }
	 */
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	 
	    ex.getBindingResult().getFieldErrors().forEach(error -> 
	        errors.put(error.getField(), error.getDefaultMessage()));
	     
	    return errors;
	}
	
	@PostMapping("/sop/comments")
	public ResponseEntity<SopGetVO> saveComments(@RequestHeader("labid") int labid,
			@RequestBody CommentsVO comments) {
		List<String> errorMsgs = new ArrayList<String>();
		SopGetVO sopGetVO = new SopGetVO();
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			comments.setUsername(username);
			sopGetVO = sopService.saveComments(labid, comments);
			if (sopGetVO != null) {
				return new ResponseEntity<>(sopGetVO, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Throwable e) {
			if (e instanceof SQLException || e instanceof ResourceNotFoundException) {
				errorMsgs.add(e.getMessage());
			} else if (!e.getMessage().contains("Validation Error")) {
				errorMsgs.add("Internal error...");
			}
			sopGetVO.setErrorMessages(errorMsgs);
			return new ResponseEntity<>(sopGetVO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
