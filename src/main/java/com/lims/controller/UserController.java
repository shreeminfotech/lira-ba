package com.lims.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lims.enums.EntityTypes;
import com.lims.modal.AuthRequestVO;
import com.lims.modal.AuthResponse;
import com.lims.modal.Response;
import com.lims.modal.SelectInputVO;
import com.lims.modal.SummaryResponse;
import com.lims.modal.UserGetVO;
import com.lims.modal.UserPostVO;
import com.lims.modal.UserTypes;
import com.lims.services.UserService;
import com.lims.util.JwtUtil;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@PostMapping(value = "/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> authenticate(@RequestBody AuthRequestVO req) throws Exception {
		AuthResponse authResp = new AuthResponse();
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(req.getUsername().toLowerCase(), req.getPassword()));
			authResp.setToken(jwtUtil.generateToken(req.getUsername().toLowerCase()));
			UserGetVO user = userService.findByUsername(req.getUsername());
			authResp.setFirstname(user.getFirstName());
			authResp.setLastname(user.getLastName());
			authResp.setUsername(user.getUsername());
			authResp.setLastlogintime(user.getLastlogintime());
			authResp.setLabs(user.getLabs());
		} catch (Throwable e) {
			e.printStackTrace();
			Response errResp = new Response();
			List<String> errorMsgs = new ArrayList<String>();
			errorMsgs.add("Invalid Username or Password...");
			errResp.setErrorMessages(errorMsgs);
			return new ResponseEntity<>(errResp, HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<>(authResp, HttpStatus.OK);

	}

	private Sort.Direction getSortDirection(String direction) {
		if (direction.equals("asc")) {
			return Sort.Direction.ASC;
		} else if (direction.equals("desc")) {
			return Sort.Direction.DESC;
		}
		return Sort.Direction.ASC;
	}

	@GetMapping("/users/manage")
	public ResponseEntity<SummaryResponse<UserGetVO>> getUsers(@RequestHeader("labid") int labid,
			@RequestParam(required = false) String searchval, @RequestParam(required = false) Integer offset,
			@RequestParam(required = false) Integer size, @RequestParam(defaultValue = "id,desc") String[] sort) {
		try {
			List<Order> orders = new ArrayList<Order>();
			if (sort[0].contains(",")) {
				// will sort more than 2 fields
				// sortOrder="field, direction"
				for (String sortOrder : sort) {
					String[] _sort = sortOrder.split(",");
					orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
				}
			} else {
				// sort=[field, direction]
				orders.add(new Order(getSortDirection(sort[1]), sort[0]));
			}
			Pageable pagingSort = PageRequest.of(offset, size, Sort.by(orders));
			SummaryResponse<UserGetVO> summaryResponse = userService.getUsers(labid, searchval, pagingSort);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);

		} catch (

		Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/list")
	public ResponseEntity<Object> getUsers(@RequestHeader("labid") int labid,
			@RequestParam(required = false) EntityTypes entitytype, @RequestParam(required = false) Integer entityid,
			@RequestParam(required = false) UserTypes usertype) {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			List<SelectInputVO> summaryResponse = userService.usersDropdownByLabId(labid, entitytype, usertype,
					entityid, username);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (

		Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<UserGetVO> getUser(@PathVariable Integer id) {
		try {
			UserGetVO user = userService.findById(id);
			if (user != null) {
				return new ResponseEntity<>(user, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Throwable e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/users")
	public ResponseEntity<String> createUser(@Valid @RequestBody UserPostVO user) {
		try {
			userService.createUser(user);
			return new ResponseEntity<>("Created successfully...", HttpStatus.CREATED);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>("Create Failed...", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/users/{id}")
	public ResponseEntity<String> updateUser(@PathVariable("id") int id, @RequestBody UserPostVO user) {
		try {
			userService.updateUser(id, user, true);
			return new ResponseEntity<>("Updated Successfully...", HttpStatus.OK);
		} catch (Throwable e) {
			return new ResponseEntity<>("Updated failed...", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable("id") int id) {
		try {
			UserPostVO user = new UserPostVO();
			userService.updateUser(id, user, false);
			return new ResponseEntity<>("Deleted successfully...", HttpStatus.OK);
		} catch (Throwable e) {
			return new ResponseEntity<>("Delete Failed...", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/users")
	public ResponseEntity<HttpStatus> deleteAllUsers() {
		try {
			// SopTypesRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PostMapping("/users/signin")
	public ResponseEntity<SelectInputVO> signInforSignature(@RequestHeader("labId") Integer labId,
			@RequestBody AuthRequestVO userVO) {
		try {
			SelectInputVO user = userService.signInforSignature(labId, userVO);
			if (user != null) {
				return new ResponseEntity<>(user, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Throwable e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/dashboard")
	public ResponseEntity<String> dashboard() {
		try {
			String DASHBOARD_JSON = "{\"status\":200,\"data\":{\"sliders\":[{\"iconimg\":\"assets/static/media/slider.e2e16e59.png\",\"title\":\"WorkFlow\",\"items\":[{\"openitems\":4,\"countsp\":\"WF\"}],\"actiontarget\":\"/workflow\",\"tilebackgroundcolor\":\"tile-1-bg\"},{\"iconimg\":\"assets/static/media/slider.e2e16e59.png\",\"title\":\"SOP\",\"items\":[{\"openitems\":24,\"countsp\":\"sops\"}],\"actiontarget\":\"/sop\",\"tilebackgroundcolor\":\"tile-1-bg\"},{\"iconimg\":\"assets/static/media/slider.e2e16e59.png\",\"title\":\"SOP Type\",\"items\":[{\"openitems\":7,\"countsp\":\"sop types\"}],\"actiontarget\":\"/sop\",\"tilebackgroundcolor\":\"tile-2-bg\"},{\"iconimg\":\"assets/static/media/slider.e2e16e59.png\",\"title\":\"Security\",\"items\":[{\"openitems\":24,\"countsp\":\"Users\"}],\"actiontarget\":\"/security\",\"tilebackgroundcolor\":\"tile-3-bg\"},{\"iconimg\":\"assets/static/media/slider.e2e16e59.png\",\"title\":\"Change Request\",\"items\":[{\"openitems\":6,\"countsp\":\"Items\"}],\"actiontarget\":\"/cr\",\"tilebackgroundcolor\":\"tile-3-bg\"},{\"iconimg\":\"assets/static/media/slider.e2e16e59.png\",\"title\":\"Trainings\",\"items\":[{\"openitems\":7,\"countsp\":\"trainings\"}],\"actiontarget\":\"/trainings\",\"tilebackgroundcolor\":\"tile-3-bg\"}],\"changereqest\":{\"title\":\"ChangeRequest\",\"iconimg\":\"assets/static/media/change-image.png\",\"tilebackgroundcolor\":\"tile-3-bg\",\"status\":[{\"title\":\"To-Do\",\"count\":2,\"order\":0,\"id\":\"todo1\"},{\"title\":\"Active\",\"count\":3,\"order\":1,\"id\":\"active1\"},{\"title\":\"Done\",\"count\":1,\"order\":2,\"id\":\"done1\"}]},\"teammembers\":{\"title\":\"Team Members\",\"tilebgimg\":\"./assets/TeamMembers.jpeg\",\"tilebackgroundcolor\":\"tile-4-bg\",\"teamMembers\":[{\"id\":\"l001u001\",\"status\":\"active\",\"sub\":\"47ee0406-7570-40cb-a040-956fec49ddd2\",\"nickname\":\"Admin\",\"preferred_username\":\"L001U001\",\"given_name\":\"L001U001\",\"family_name\":\"AdminLastName\"},{\"id\":\"l001u002\",\"status\":\"active\",\"sub\":\"2e63ded8-7134-4955-b341-c978bbc52843\",\"nickname\":\"HOD\",\"preferred_username\":\"L001U002\",\"given_name\":\"L001U002\",\"family_name\":\"HODLastName\"},{\"id\":\"l001u003\",\"status\":\"active\",\"sub\":\"0c440097-b60a-4a86-b51e-ab3c71c28679\",\"nickname\":\"Manager\",\"preferred_username\":\"L001U003\",\"given_name\":\"L001U003\",\"family_name\":\"ManagerLastName\"},{\"id\":\"l001u004\",\"status\":\"active\",\"sub\":\"159586f7-8b75-431f-9aba-ebf8fe18997f\",\"nickname\":\"Auditor\",\"preferred_username\":\"L001U004\",\"given_name\":\"L001U004\",\"family_name\":\"AuditorLastName\"},{\"id\":\"l001u005\",\"status\":\"active\",\"sub\":\"5298fa22-9744-4a29-93a4-0f30790340a5\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L001U005\",\"given_name\":\"L001U005\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l001u006\",\"status\":\"active\",\"sub\":\"46b90194-11e3-49e3-972c-e53996f1ef90\",\"nickname\":\"Programmer\",\"preferred_username\":\"L001U006\",\"given_name\":\"L001U006\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l001u007\",\"status\":\"active\",\"sub\":\"557b1407-cc3c-44aa-91a3-b50bbd44949a\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L001U007\",\"given_name\":\"L001U007\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l001u008\",\"status\":\"active\",\"sub\":\"c8f57ff9-6a48-417d-9c9d-3b6c6be422db\",\"nickname\":\"Programmer\",\"preferred_username\":\"L001U008\",\"given_name\":\"L001U008\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l001u009\",\"status\":\"active\",\"sub\":\"e3c3571d-8c7a-4558-89e2-0e04b513037f\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L001U009\",\"given_name\":\"L001U009\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l001u010\",\"status\":\"active\",\"sub\":\"60cfe1ad-f02f-4c04-9ac5-ddaec6d3f273\",\"nickname\":\"Programmer\",\"preferred_username\":\"L001U010\",\"given_name\":\"L001U010\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l001u011\",\"status\":\"active\",\"sub\":\"f4238a67-a53c-47eb-807f-e57dd50b3e5f\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L001U011\",\"given_name\":\"L001U011\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l001u012\",\"status\":\"active\",\"sub\":\"5f301d47-7b88-4e7f-a219-aa44c34a6dc0\",\"nickname\":\"Programmer\",\"preferred_username\":\"L001U012\",\"given_name\":\"L001U012\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l002u001\",\"status\":\"active\",\"sub\":\"f282b7e2-f4d3-49a8-8f4f-c0b05d8b45e0\",\"nickname\":\"Admin\",\"preferred_username\":\"L002U001\",\"given_name\":\"L002U001\",\"family_name\":\"AdminLastName\"},{\"id\":\"l002u002\",\"status\":\"active\",\"sub\":\"ebbf94c9-083e-44b1-9995-a7b968d59037\",\"nickname\":\"HOD\",\"preferred_username\":\"L002U002\",\"given_name\":\"L002U002\",\"family_name\":\"HODLastName\"},{\"id\":\"l002u003\",\"status\":\"active\",\"sub\":\"a9755916-f78d-4013-bb84-fbe6b325927b\",\"nickname\":\"Manager\",\"preferred_username\":\"L002U003\",\"given_name\":\"L002U003\",\"family_name\":\"ManagerLastName\"},{\"id\":\"l002u004\",\"status\":\"active\",\"sub\":\"ef165408-0727-4049-880a-0f0bc7fec4f0\",\"nickname\":\"Auditor\",\"preferred_username\":\"L002U004\",\"given_name\":\"L002U004\",\"family_name\":\"AuditorLastName\"},{\"id\":\"l002u005\",\"status\":\"active\",\"sub\":\"7699f0e0-208b-4349-a791-1bac97990607\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L002U005\",\"given_name\":\"L002U005\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l002u006\",\"status\":\"active\",\"sub\":\"df80e581-3d22-489f-922a-80b86fd37984\",\"nickname\":\"Programmer\",\"preferred_username\":\"L002U006\",\"given_name\":\"L002U006\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l002u007\",\"status\":\"active\",\"sub\":\"ca191553-b462-4eb0-a4ec-fef12279ac49\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L002U007\",\"given_name\":\"L002U007\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l002u008\",\"status\":\"active\",\"sub\":\"4ca846f5-621c-42f7-847e-a84c5515c70e\",\"nickname\":\"Programmer\",\"preferred_username\":\"L002U008\",\"given_name\":\"L002U008\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l002u009\",\"status\":\"active\",\"sub\":\"078a60ba-1ff5-4ca6-9c53-c3b6e355462e\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L002U009\",\"given_name\":\"L002U009\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l002u010\",\"status\":\"active\",\"sub\":\"58fd39b2-d11a-4173-81fd-4e39efce9ff3\",\"nickname\":\"Programmer\",\"preferred_username\":\"L002U010\",\"given_name\":\"L002U010\",\"family_name\":\"ProgrammerLastName\"},{\"id\":\"l002u011\",\"status\":\"active\",\"sub\":\"d19285f5-8239-4445-ae50-20953170e298\",\"nickname\":\"Lab Technician\",\"preferred_username\":\"L002U011\",\"given_name\":\"L002U011\",\"family_name\":\"LabTechLastName\"},{\"id\":\"l002u012\",\"status\":\"active\",\"sub\":\"52c8e274-051c-4e15-af18-70ef5e3383c1\",\"nickname\":\"Programmer\",\"preferred_username\":\"L002U012\",\"given_name\":\"L002U012\",\"family_name\":\"ProgrammerLastName\"}]},\"trainingschedhule\":{\"title\":\"Training Schedule\",\"subtitle\":\"Today\",\"schedhule\":[{\"projectId\":\"PRJ000001\",\"trainingId\":\"PRJ1TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Blue\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/11/2021\",\"trainingName\":\"Project Blue Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59001\"},{\"projectId\":\"PRJ000002\",\"trainingId\":\"PRJ2TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Green\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/10/2021\",\"trainingName\":\"Project Green Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59002\"},{\"projectId\":\"PRJ000003\",\"trainingId\":\"PRJ3TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Yellow\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/09/2021\",\"trainingName\":\"Project Yellow Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59003\"},{\"projectId\":\"PRJ000003\",\"trainingId\":\"PRJ3TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Yellow\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/08/2021\",\"trainingName\":\"Project Yellow Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59004\"},{\"projectId\":\"PRJ000003\",\"trainingId\":\"PRJ3TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Yellow\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/07/2021\",\"trainingName\":\"Project Yellow Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59005\"},{\"projectId\":\"PRJ000003\",\"trainingId\":\"PRJ3TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Yellow\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/06/2021\",\"trainingName\":\"Project Yellow Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59006\"},{\"projectId\":\"PRJ000003\",\"trainingId\":\"PRJ3TR0000001\",\"trainingStartTime\":\"9:25:01 PM\",\"projectName\":\"Project Yellow\",\"TrainingLink\":\"#\",\"TrainingDurationType\":\"H\",\"date\":\"12/05/2021\",\"trainingName\":\"Project Yellow Training 1\",\"TrainingDuration\":4.5,\"kind\":\"lims#training\",\"trainingEndTime\":\"11:25:01 PM\",\"id\":\"ebbf94c9-083e-44b1-9995-a7b968d59007\"}]}}}";
			return new ResponseEntity<>(DASHBOARD_JSON, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
