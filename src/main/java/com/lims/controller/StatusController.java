package com.lims.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lims.modal.SelectInputVO;
import com.lims.services.StatusService;

@RestController
@RequestMapping("/api")
public class StatusController {

	@Autowired
	StatusService statusService;

	@GetMapping("/status")
	public ResponseEntity<Object> getStatuses(@RequestHeader("labid") int labid,
			@RequestParam(required = false) String moduletype) {
		try {
			List<SelectInputVO> summaryResponse = statusService.statusDropdownByModuleType(labid, moduletype);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
