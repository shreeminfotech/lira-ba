package com.lims.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.lims.enums.EntityTypes;
import com.lims.exceptions.ResourceNotFoundException;
import com.lims.modal.SelectInputVO;
import com.lims.modal.SopTypeGetVO;
import com.lims.modal.SopTypePostVO;
import com.lims.modal.SummaryResponse;
import com.lims.modal.UserTypes;
import com.lims.services.SopTypeService;
import com.lims.services.UserService;

@RestController
@RequestMapping("/api")
public class SopTypeController {

	@Autowired
	SopTypeService sopTypeService;

	private Sort.Direction getSortDirection(String direction) {
		if (direction.equals("asc")) {
			return Sort.Direction.ASC;
		} else if (direction.equals("desc")) {
			return Sort.Direction.DESC;
		}
		return Sort.Direction.ASC;
	}

	@GetMapping("/soptype/manage")
	public ResponseEntity<SummaryResponse<SopTypeGetVO>> getSopTypes(@RequestHeader("labid") int labid,
			@RequestParam(required = false) String searchval, @RequestParam(required = false) Integer offset,
			@RequestParam(required = false) Integer size, @RequestParam(defaultValue = "id,desc") String[] sort) {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			List<Order> orders = new ArrayList<Order>();
			if (sort[0].contains(",")) {
				// will sort more than 2 fields
				// sortOrder="field, direction"
				for (String sortOrder : sort) {
					String[] _sort = sortOrder.split(",");
					orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
				}
			} else {
				// sort=[field, direction]
				orders.add(new Order(getSortDirection(sort[1]), sort[0]));
			}
			Pageable pagingSort = null;
			if(offset != null)
				pagingSort = PageRequest.of(offset, size, Sort.by(orders));
			SummaryResponse<SopTypeGetVO> summaryResponse = sopTypeService.sopTypeList(labid, searchval, pagingSort, username);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/soptype/list")
	public ResponseEntity<List<SelectInputVO>> getSopTypes(@RequestHeader("labid") int labid) {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			List<SelectInputVO> summaryResponse = sopTypeService.sopTypeDropdownByLabId(labid, username);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * @GetMapping("/sopType/home/lab/{id}") public ResponseEntity<List<SopTypeVO>>
	 * sopTypeList(@PathVariable("id") Integer id) { try { List<SopTypeVO>
	 * summaryResponse = sopTypeService.sopTypeListByLabId(id);
	 * 
	 * if (summaryResponse == null) { return new
	 * ResponseEntity<>(HttpStatus.NO_CONTENT); } return new
	 * ResponseEntity<>(summaryResponse, HttpStatus.OK); } catch (Throwable e) {
	 * e.printStackTrace(); return new
	 * ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); } }
	 */

	@GetMapping("/soptype/{id}")
	public ResponseEntity<SopTypeGetVO> getSopTypeById(@PathVariable("id") Integer id) {
		try {
			SopTypeGetVO sopType = sopTypeService.getSopTypeById(id);

			if (sopType != null) {
				return new ResponseEntity<>(sopType, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Throwable e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * @PostMapping("/sopType/list") public
	 * ResponseEntity<SummaryResponse<SopTypeVO>> sopTypeList(@RequestBody
	 * ManagePageVO manageObj) { try { SummaryResponse<SopTypeVO> summaryResponse =
	 * sopTypeService.sopTypeList(manageObj);
	 * 
	 * if (summaryResponse == null) { return new
	 * ResponseEntity<>(HttpStatus.NO_CONTENT); } return new
	 * ResponseEntity<>(summaryResponse, HttpStatus.OK); } catch (Throwable e) {
	 * return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); } }
	 */

	@PostMapping("/soptype")
	public ResponseEntity<Object> createSopType(@RequestHeader("labid") int labid,
			@Valid @RequestBody SopTypePostVO sopType) {
		SopTypeGetVO sopGetType = new SopTypeGetVO();
		List<String> errorMsgs = new ArrayList<String>();
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			if (sopType.getName() == null || sopType.getName().equals("")) {
				errorMsgs.add("Name is required.");
			}
			if (sopType.getColorCode() == null || sopType.getColorCode().equals("")) {
				errorMsgs.add("Color code is required.");
			}
			if (sopType.getVisibilityLevel() != null && sopType.getVisibilityLevel().equals("USERS")
					&& (sopType.getUserList() == null || sopType.getUserList().size() == 0)) {
				errorMsgs.add("Please select visibility users.");
			}
			if (errorMsgs.size() <= 0) {
				sopGetType = sopTypeService.createSopType(labid, sopType, username);
				return new ResponseEntity<>(sopGetType, HttpStatus.OK);
			} else {
				throw new Exception("Validation Error");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			if (e instanceof SQLException) {
				errorMsgs.add(e.getMessage());
			} else if (!e.getMessage().contains("Validation Error")) {
				errorMsgs.add("Internal error...");
			}
			sopGetType.setErrorMessages(errorMsgs);
			return new ResponseEntity<>(sopGetType, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/soptype/{id}")
	public ResponseEntity<SopTypeGetVO> updateSopType(@RequestHeader("labid") int labid, @PathVariable("id") int id,
			@Valid @RequestBody SopTypePostVO sopType) {
		SopTypeGetVO sopGetType = new SopTypeGetVO();
		List<String> errorMsgs = new ArrayList<String>();
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			sopGetType = sopTypeService.updateSopType(labid, id, sopType, true, username);
			return new ResponseEntity<>(sopGetType, HttpStatus.OK);

		} catch (Throwable e) {
			e.printStackTrace();
			if (e instanceof SQLException || e instanceof ResourceNotFoundException) {
				errorMsgs.add(e.getMessage());
			} else if (!e.getMessage().contains("Validation Error")) {
				errorMsgs.add("Internal error...");
			}
			sopGetType.setErrorMessages(errorMsgs);
			return new ResponseEntity<>(sopGetType, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/soptype/{id}")
	public ResponseEntity<String> deleteSopTypes(@RequestHeader("labid") int labid, @PathVariable("id") int id,
			@RequestBody SopTypePostVO sopType) throws Throwable{
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			sopTypeService.updateSopType(labid, id, sopType, false, username);
			return new ResponseEntity<>("Deleted successfully...", HttpStatus.OK);
		} catch (Throwable e) {
			if (e instanceof ResourceNotFoundException) {
				throw e;
			} else {
				return new ResponseEntity<>("Delete Failed...", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@Autowired
	UserService userService;

	@GetMapping("soptype/visibility/users")
	public ResponseEntity<Object> getUsers(@RequestHeader("labid") int labid,
			@RequestParam(required = false) Integer entityid) {
		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			String username = userDetails.getUsername();
			List<SelectInputVO> summaryResponse = userService.usersDropdownByLabId(labid, EntityTypes.SOP_Type,
					UserTypes.visibility, entityid, username);

			if (summaryResponse == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(summaryResponse, HttpStatus.OK);
		} catch (

		Throwable e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	 
	    ex.getBindingResult().getFieldErrors().forEach(error -> 
	        errors.put(error.getField(), error.getDefaultMessage()));
	     
	    return errors;
	}
}
