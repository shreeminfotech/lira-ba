package com.lims.enums;

public enum StepKeys {
	
	INITIATOR, AUTHOR, REVIEWER, FIRST_APPROVER, SECOND_APPROVER, TRAINING

}
