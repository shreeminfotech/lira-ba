package com.lims.enums;

public enum StatusTypes {
	INITIATE, DRAFT, IN_REVIEW, FIRST_APPROVED, SECOND_APPROVED, TRAINING, ACTIVE
}
