package com.lims.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.WorkFlowMetrix;
import com.lims.enums.EntityTypes;

@Repository
@Transactional
public interface WorkflowMetrixRepository extends JpaRepository<WorkFlowMetrix, Integer> {

	List<WorkFlowMetrix> findByEntityIdAndEntityType(Integer entityId, EntityTypes entityType);

	@Query(value = "SELECT u.id, u.username, u.first_name, u.last_name, n.signed_date, ams.step_key "
			+ " FROM work_flow_metrix n INNER JOIN ( "
			+ "  SELECT module_step_id, MAX(signed_date) AS signed_date "
			+ "  FROM work_flow_metrix where entity_type=:entityType and entity_id=:entityId and "
			+ "  signed_flag=:signedFlag GROUP BY module_step_id ) AS max USING (module_step_id, signed_date) "
			+ "inner join application_user u inner join application_module_steps ams "
			+ "where entity_type=:entityType and entity_id=:entityId and signed_flag=:signedFlag  and ams.id=module_step_id "
			+ "and u.id=n.assigned_to", nativeQuery = true)
	List<Object[]> getApproverList(@Param("entityType") String entityType, @Param("entityId") Integer entityId,
			@Param("signedFlag") Boolean signedFlag);

}
