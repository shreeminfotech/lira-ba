package com.lims.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.util.StringUtils;

import com.lims.dao.domain.Sop;
import com.lims.modal.ManagePageVO;
import com.lims.repositories.common.CustomSopRepository;

public class SopRepositoryImpl implements CustomSopRepository {

	@PersistenceContext
	private EntityManager entityManager;

		
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Sop> sopList(ManagePageVO manageObj) throws Throwable {
		StringBuilder sb = new StringBuilder();
		sb.append(" FROM Sop st WHERE st.isActive=:isActive ");
		if (manageObj != null && manageObj.getSearch() != null) {
			if (StringUtils.hasLength(manageObj.getSearch().getName().trim())
					&& manageObj.getSearch().getName().equalsIgnoreCase("name")) {
				sb.append(" AND st.name =:name ");
			}
			if (manageObj.getSort() != null) {

				if (StringUtils.hasLength(manageObj.getSort().getColumn())
						&& StringUtils.hasLength(manageObj.getSort().getOrder())) {
					if (manageObj.getSort().getColumn().equalsIgnoreCase("name")) {
						sb.append(" order by st.name " + manageObj.getSort().getOrder());
					} else {
						sb.append(" order by st.createdDate desc");
					}
				} else {
					sb.append(" order by st.createdDate desc");
				}
			} else {
				sb.append(" order by st.createdDate desc");
			}
		}
		Query query = entityManager.createQuery(sb.toString());
		query.setParameter("isActive", true);
		if (manageObj != null && manageObj.getSearch() != null) {
			if (StringUtils.hasLength(manageObj.getSearch().getName().trim())
					&& manageObj.getSearch().getName().equalsIgnoreCase("name")) {
				query.setParameter("name", manageObj.getSearch().getValue().trim());
			}

			if (manageObj.getPage() != null) {
				if (manageObj.getPage().getOffset() > 0) {
					query.setFirstResult(manageObj.getPage().getOffset());
				}
				if (manageObj.getPage().getSize() > 0) {
					query.setMaxResults(manageObj.getPage().getSize());
				}
			}
		}
		return query.getResultList();
	}

	@Override
	@Transactional
	public int getSopCount(ManagePageVO manageObj) throws Throwable {

		int count = 0;
		StringBuilder sb = new StringBuilder();
		sb.append(" select count(*) from Sop st where st.isActive=:isActive ");
		if (manageObj != null && manageObj.getSearch() != null) {
			if (!StringUtils.hasLength(manageObj.getSearch().getName().trim())&& manageObj.getSearch().getName().equalsIgnoreCase("name")) {
				sb.append(" AND st.name =:name ");
			}
		}
		Query query = entityManager.createQuery(sb.toString());
		if (manageObj != null && manageObj.getSearch() != null) {
			if (StringUtils.hasLength(manageObj.getSearch().getName().trim())
					&& manageObj.getSearch().getName().equalsIgnoreCase("name")) {
				query.setParameter("name", manageObj.getSearch().getValue().trim());
			}
		}
		query.setParameter("isActive", true);
		Object obj = query.getSingleResult();
	    if (obj != null) {
	    	count = Integer.parseInt(obj.toString());
	    }

		return count;
	}

}
