package com.lims.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationDocuments;
import com.lims.enums.EntityTypes;

@Repository
@Transactional
public interface ApplicationDocumentsRepository extends JpaRepository<ApplicationDocuments, Integer> {

	List<ApplicationDocuments> findByIsActive(boolean isActive);

	List<ApplicationDocuments> findByDocumentName(String name);

	@Query(value = "select distinct ad.id, ad.documentName from ApplicationDocuments ad"
			+ " where ad.lab.id=:labId and ad.entityType=:entityType and ad.entityId=:entityId and ad.isActive=:isActive")
	List<Object[]> getAppdocList(Integer labId, EntityTypes entityType, Integer entityId, Boolean isActive);

}
