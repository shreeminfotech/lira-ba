package com.lims.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationComment;
import com.lims.enums.EntityTypes;

@Repository
@Transactional
public interface CommentsRepository extends JpaRepository<ApplicationComment, Integer> {
	@Query(value = "select a from ApplicationComment a inner join a.workFlowMetrix wfm inner join Sop s on wfm.entityId=s.id"
			+ " where s.isActive=:isActive and s.lab.id=:labId and s.id=:sopId and wfm.entityType=:entityType order by a.updatedDate asc")
	List<ApplicationComment> getCommentsBySopId(@Param("labId") Integer labId, @Param("sopId") Integer sopId,
			@Param("entityType") EntityTypes entityType, @Param("isActive") Boolean isActive);
}
