package com.lims.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.SopTypeVisibility;
import com.lims.modal.SelectInputVO;

@Repository
@Transactional
public interface SopTypeVisibilityRepository extends JpaRepository<SopTypeVisibility, Integer> {

	@Query(value = "select new com.lims.modal.SelectInputVO(s.applicationUser.id as id, concat(s.applicationUser.firstName,' ', s.applicationUser.lastName) as name) from "
			+ "SopTypeVisibility s where s.sopType.id=:sopTypeId and s.applicationUser.isActive=:isActive")
	List<SelectInputVO> getUsersBySopTypeId(@Param("sopTypeId") Integer sopTypeId,@Param("isActive") Boolean isActive);
}
