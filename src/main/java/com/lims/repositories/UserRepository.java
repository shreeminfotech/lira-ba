package com.lims.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationUser;
import com.lims.enums.EntityTypes;
import com.lims.modal.SelectInputVO;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<ApplicationUser, Integer> {

	ApplicationUser findByUsernameIgnoreCase(String username);
	
	@Query(value = "select new com.lims.modal.SelectInputVO(au.id as id, concat(au.firstName,' ', au.lastName) as name) from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.initiatorFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive")
	List<SelectInputVO> getInitiatorsList(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag);

	@Query(value = "select new com.lims.modal.SelectInputVO(au.id as id, concat(au.firstName,' ', au.lastName) as name) from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.authorFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive")
	List<SelectInputVO> getAuthorsList(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag);

	@Query(value = "select new com.lims.modal.SelectInputVO(au.id as  id, concat(au.firstName,' ', au.lastName) as name) from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.reviewerFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive")
	List<SelectInputVO> geReviewersList(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag);

	@Query(value = "select new com.lims.modal.SelectInputVO(au.id as id, concat(au.firstName,' ', au.lastName) as name) from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.approverFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive")
	List<SelectInputVO> getApproversList(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag);

	@Query(value = "SELECT distinct au FROM ApplicationUser au "
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr "
			+ " WHERE asr.lab.id=:labId and (:inputString is null or (au.firstName like %:inputString%"
			+ " or au.lastName like %:inputString%))")
	Page<ApplicationUser> userList(Integer labId, String inputString, Pageable pageable);

	@Query(value = "select new com.lims.modal.SelectInputVO(au.id as id, concat(au.firstName,' ', au.lastName) as name) from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.viewFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive")
	List<SelectInputVO> sopTypeVisibilityUsers(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag);
	
	@Query(value = "select new com.lims.modal.SelectInputVO(au.id as id, concat(au.firstName,' ', au.lastName) as name) from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.viewFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive and au.id!=:userId")
	List<SelectInputVO> sopTypeVisibilityUsers(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag, Integer userId);

	@Query(value = "select distinct au.id, au.firstName," + " au.lastName from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.approverFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive and lower(au.username)=lower(:username) and au.password=:password")
	Object[] initiatorSignIn(Integer labId, EntityTypes entityType, String username, String password,
			Boolean isActive, Boolean srtyRoleFlag);

	@Query(value = "select distinct au.id, au.firstName," + " au.lastName from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.authorFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive and lower(au.username)=lower(:username) and au.password=:password")
	Object[] authorSignIn(Integer labId, EntityTypes entityType, String username, String password,
			Boolean isActive, Boolean srtyRoleFlag);
	
	@Query(value = "select au.id, au.firstName," + " au.lastName from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.reviewerFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive and lower(au.username)=lower(:username) and au.password=:password")
	Object[] reviewerSignIn(Integer labId, EntityTypes entityType, String username, String password,
			Boolean isActive, Boolean srtyRoleFlag);
	
	@Query(value = "select distinct au.id, au.firstName," + " au.lastName from ApplicationUser au"
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr"
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.approverFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive and lower(au.username)=lower(:username) and au.password=:password")
	Object[] approverSignIn(Integer labId, EntityTypes entityType, String username, String password,
			Boolean isActive, Boolean srtyRoleFlag);

}
