package com.lims.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationDocumentCommentsLog;

@Repository
@Transactional
public interface DocumentCommentsRepository extends JpaRepository<ApplicationDocumentCommentsLog, Integer> {
}
