package com.lims.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationStatus;
import com.lims.enums.StatusTypes;

@Repository
@Transactional
public interface StatusRepository extends JpaRepository<ApplicationStatus, Integer> {
	

	@Query(value="select s.id, s.name from ApplicationStatus s where s.applicationModule.name=:moduleName ")
	List<Object[]> statusDropdownByModuleType(@Param("moduleName") String moduleName);
	
	ApplicationStatus findByEnumKey(StatusTypes enumKey);

}
