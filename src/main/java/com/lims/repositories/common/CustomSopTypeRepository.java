package com.lims.repositories.common;

import java.util.List;

import com.lims.dao.domain.SopType;
import com.lims.modal.ManagePageVO;

public interface CustomSopTypeRepository {

	public List<SopType> sopTypeList(ManagePageVO manageObj) throws Throwable;

	public int getSopTypesCount(ManagePageVO manageObj) throws Throwable;

}
