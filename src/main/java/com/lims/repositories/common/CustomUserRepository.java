package com.lims.repositories.common;

import java.util.List;

import com.lims.dao.domain.ApplicationUser;
import com.lims.modal.ManagePageVO;

public interface CustomUserRepository {

	public List<ApplicationUser> userList(ManagePageVO manageObj) throws Throwable;

	public int getUserCount(ManagePageVO manageObj) throws Throwable;

}
