package com.lims.repositories.common;

import java.util.List;

import com.lims.dao.domain.Sop;
import com.lims.modal.ManagePageVO;

public interface CustomSopRepository {

	public List<Sop> sopList(ManagePageVO manageObj) throws Throwable;

	public int getSopCount(ManagePageVO manageObj) throws Throwable;

}
