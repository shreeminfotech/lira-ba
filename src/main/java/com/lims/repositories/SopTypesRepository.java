package com.lims.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.SopType;
import com.lims.enums.EntityTypes;
import com.lims.repositories.common.CustomSopTypeRepository;

@Repository
@Transactional
public interface SopTypesRepository extends JpaRepository<SopType, Integer>, CustomSopTypeRepository {

	List<SopType> findByIsActive(boolean isActive);

	List<SopType> findByNameContaining(String name);

	@Query(value = "select s from SopType s where s.isActive=:isActive and s.lab.id=:labId")
	List<SopType> findByLabId(@Param("labId") Integer labId, @Param("isActive") Boolean isActive);

	@Query(value = "select s.id, s.name from sop_type s left outer join application_user su on s.created_by=su.id "
			+ "left outer join sop_type_visibility sv on s.id=sv.sop_type_id"
			+ " where s.is_active=:isActive and s.lab_id=:labId and s.visibility = 'ALL'"
			+ " or (s.visibility = 'SELF' and lower(su.username)=lower(:username)) or (s.visibility = 'USERS' and sv.user_id=s.created_by)", nativeQuery = true)
	List<Object[]> sopTypeDropdownByLabId(@Param("labId") Integer labId, @Param("isActive") Boolean isActive,
			@Param("username") String username);

	@Query(value = "SELECT s FROM SopType s left join s.applicationUser1 su"
			+ " left join s.sopTypeVisibilities sv left join sv.applicationUser svu "
			+ " WHERE s.isActive=:isActive and s.lab.id=:labId and (:inputString is null or (s.name like %:inputString%"
			+ " or s.description like %:inputString%)) " + " and (((s.visibility = 'SELF' and lower(su.username)=lower(:username))"
			+ " or (s.visibility = 'USERS' and lower(svu.username)=lower(:username))) or s.visibility = 'ALL') order by s.systemDefault desc, s.name asc")
	Page<SopType> sopTypeList(Integer labId, String inputString, Pageable pageable, @Param("username") String username,
			@Param("isActive") Boolean isActive);
	
	@Query(value = "select distinct au.id, au.firstName," + " au.lastName from SopType st inner join "
			+ " st.sopTypeVisibilities stv inner join stv.applicationUser au "
			+ " inner join au.applicationUserRoles aur inner join aur.applicationSecurityRole asr "
			+ " inner join asr.applicationSecurityRolesMaps asrm " + " where asr.lab.id=:labId and "
			+ " asrm.applicationModule.enumKey=:entityType and " + "asrm.viewFlag=:srtyRoleFlag and "
			+ " au.isActive=:isActive and st.id=:entityId")
	List<Object[]> sopTypeVisibilityUsers(Integer labId, EntityTypes entityType, Boolean isActive, Boolean srtyRoleFlag, Integer entityId);

}
