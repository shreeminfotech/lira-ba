package com.lims.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationScheduler;
import com.lims.enums.EntityTypes;

@Repository
@Transactional
public interface ApplicationSchedulerRepository extends JpaRepository<ApplicationScheduler, Integer> {

	@Query(value = "select ams from ApplicationScheduler ams where ams.entityType=:entityType and ams.entityId=:entityId")
	ApplicationScheduler getSchedulerDetails(EntityTypes entityType, Integer entityId);

}
