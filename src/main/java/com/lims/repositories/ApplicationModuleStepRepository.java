package com.lims.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationModuleStep;
import com.lims.enums.EntityTypes;
import com.lims.enums.StepKeys;

@Repository
@Transactional
public interface ApplicationModuleStepRepository extends JpaRepository<ApplicationModuleStep, Integer> {

	@Query(value = "select ams from ApplicationModuleStep ams where ams.enabled=:enabled and ams.applicationModule.enumKey=:entityType")
	List<ApplicationModuleStep> geSteps(EntityTypes entityType, Boolean enabled);

	@Query(value = "select ams from ApplicationModuleStep ams where ams.enabled=:enabled and ams.applicationModule.enumKey=:entityType"
			+ " and ams.stepKey=:stepKey")
	ApplicationModuleStep getByModuleIdAndStepKey(EntityTypes entityType, StepKeys stepKey, Boolean enabled);

}
