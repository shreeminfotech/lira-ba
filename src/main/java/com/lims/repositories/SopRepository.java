package com.lims.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.Sop;
import com.lims.repositories.common.CustomSopRepository;

@Repository
@Transactional
public interface SopRepository extends JpaRepository<Sop, Integer>, CustomSopRepository {

	List<Sop> findByIsActive(boolean isActive);

	List<Sop> findByNameContaining(String name);
	
	Sop findByName(String name);
	
	Sop findByNumber(String number);

	@Query(value = "select s from Sop s where s.isActive=:isActive and s.lab.id=:labId")
	List<Sop> findByLabId(@Param("labId") Integer labId, @Param("isActive") Boolean isActive);

	@Query(value = "select s from Sop s where s.isActive=:isActive and s.lab.id=:labId and s.id=:id")
	Sop findByIdAndLabId(@Param("id") Integer id, @Param("labId") Integer labId, @Param("isActive") Boolean isActive);

	@Query(value = "SELECT s FROM Sop s inner join s.applicationStatus sts "
			+ " WHERE s.isActive=:isActive and s.lab.id=:labId and s.sopType.id=:typeId and "
			+ "(:inputString is null or (s.name like %:inputString%"
			+ " or s.number like %:inputString%)) "
			+ " and (sts.enumKey='ACTIVE' or s.id in "
			+ "(select wfm.entityId from WorkFlowMetrix wfm where wfm.entityType='SOP' and "
			+ "lower(wfm.applicationUser.username)=lower(:username))) order by s.createdDate desc")
	Page<Sop> sopListBySopType(Integer labId, Integer typeId, String inputString, Pageable pageable, String username,
			@Param("isActive") Boolean isActive);

	@Query(value = "SELECT s FROM Sop s inner join s.applicationStatus sts "
			+ " WHERE s.isActive=:isActive and s.lab.id=:labId and (:inputString is null or (s.name like %:inputString%"
			+ " or s.number like %:inputString%)) "
			+ " and (sts.enumKey='ACTIVE' or s.id in (select wfm.entityId from WorkFlowMetrix wfm where wfm.entityType='SOP'"
			+ " and lower(wfm.applicationUser.username)=lower(:username)))  order by s.createdDate desc")
	Page<Sop> sopList(Integer labId, String inputString, Pageable pageable, String username,
			@Param("isActive") Boolean isActive);

	@Query(value = "select s from Sop s where s.isActive=:isActive and s.lab.id=:labId and s.sopType.id=:sopTypeId")
	List<Sop> findBySopTypeId(@Param("labId") Integer labId, @Param("sopTypeId") Integer sopTypeId,
			@Param("isActive") Boolean isActive);

}
