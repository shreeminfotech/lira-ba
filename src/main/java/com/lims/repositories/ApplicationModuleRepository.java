package com.lims.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lims.dao.domain.ApplicationModule;
import com.lims.enums.EntityTypes;

@Repository
@Transactional
public interface ApplicationModuleRepository extends JpaRepository<ApplicationModule, Integer>{

	ApplicationModule findByName(String name);
	
	ApplicationModule findByEnumKey(EntityTypes enumKey);
}
