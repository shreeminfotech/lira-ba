package com.lims.dao.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.EntityTypes;


/**
 * The persistent class for the work_flow_metrix database table.
 * 
 */
@Entity
@Table(name="work_flow_metrix")
@NamedQuery(name="WorkFlowMetrix.findAll", query="SELECT w FROM WorkFlowMetrix w")
public class WorkFlowMetrix implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String conclusion;

	@Lob
	private byte[] content;

	@Column(name="content_type")
	private String contentType;

	@Lob
	@Column(name="corrective_action")
	private String correctiveAction;

	@Column(name="document_name")
	private String documentName;

	@Column(name="entity_id")
	private int entityId;

	@Column(name="entity_type")
	@Enumerated(EnumType.STRING)
	private EntityTypes entityType;

	private int filesize;

	@Lob
	private String recomendation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="signed_date")
	private Date signedDate;

	@Column(name="signed_flag")
	private Boolean signedFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="uploaded_date")
	private Date uploadedDate;

	@Column(name="work_flow_order")
	private int workFlowOrder;

	//bi-directional many-to-one association to ApplicationComment
	@OneToMany(mappedBy="workFlowMetrix", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ApplicationComment> applicationComments;

	//bi-directional many-to-one association to ApplicationModuleStep
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="module_step_id")
	private ApplicationModuleStep applicationModuleStep;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="assigned_to")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	public WorkFlowMetrix() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConclusion() {
		return this.conclusion;
	}

	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}

	public byte[] getContent() {
		return this.content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getCorrectiveAction() {
		return this.correctiveAction;
	}

	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public EntityTypes getEntityType() {
		return this.entityType;
	}

	public void setEntityType(EntityTypes entityType) {
		this.entityType = entityType;
	}

	public int getFilesize() {
		return this.filesize;
	}

	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}

	public String getRecomendation() {
		return this.recomendation;
	}

	public void setRecomendation(String recomendation) {
		this.recomendation = recomendation;
	}

	public Date getSignedDate() {
		return this.signedDate;
	}

	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}

	public Boolean getSignedFlag() {
		return this.signedFlag;
	}

	public void setSignedFlag(Boolean signedFlag) {
		this.signedFlag = signedFlag;
	}

	public Date getUploadedDate() {
		return this.uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public int getWorkFlowOrder() {
		return this.workFlowOrder;
	}

	public void setWorkFlowOrder(int workFlowOrder) {
		this.workFlowOrder = workFlowOrder;
	}

	public List<ApplicationComment> getApplicationComments() {
		return this.applicationComments;
	}

	public void setApplicationComments(List<ApplicationComment> applicationComments) {
		this.applicationComments = applicationComments;
	}

	public void addApplicationComment(ApplicationComment applicationComment) {
		if(this.applicationComments == null)
			this.applicationComments = new ArrayList<ApplicationComment>();
		this.applicationComments.add(applicationComment);
		applicationComment.setWorkFlowMetrix(this);
	}

	public void removeApplicationComment(ApplicationComment applicationComment) {
		this.applicationComments.remove(applicationComment);
		applicationComment.setWorkFlowMetrix(null);
	}

	public ApplicationModuleStep getApplicationModuleStep() {
		return this.applicationModuleStep;
	}

	public void setApplicationModuleStep(ApplicationModuleStep applicationModuleStep) {
		this.applicationModuleStep = applicationModuleStep;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}