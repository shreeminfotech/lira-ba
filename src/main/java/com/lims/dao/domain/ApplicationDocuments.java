package com.lims.dao.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.EntityTypes;

/**
 * The persistent class for the application_documents database table.
 * 
 */
@Entity
@Table(name = "application_documents")
@NamedQuery(name = "ApplicationDocuments.findAll", query = "SELECT a FROM ApplicationDocuments a")
public class ApplicationDocuments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Lob
	private byte[] content;

	@Column(name = "document_name")
	private String documentName;

	@Column(name = "document_type")
	private String documentType;

	@Column(name = "entity_id")
	private int entityId;

	@Column(name="entity_type")
	@Enumerated(EnumType.STRING)
	private EntityTypes entityType;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Lab lab;

	private long filesize;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "isdeleted")
	private Boolean isDeleted;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="uploaded_by")
	private ApplicationUser  uploadedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "uploaded_date")
	private Date uploadedDate;

	public ApplicationDocuments() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getContent() {
		return this.content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public EntityTypes getEntityType() {
		return this.entityType;
	}

	public void setEntityType(EntityTypes entityType) {
		this.entityType = entityType;
	}

	public long getFilesize() {
		return this.filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public ApplicationUser getUploadedBy() {
		return this.uploadedBy;
	}

	public void setUploadedBy(ApplicationUser uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public Date getUploadedDate() {
		return this.uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public Lab getLab() {
		return lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

}