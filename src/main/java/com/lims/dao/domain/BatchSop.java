package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the batch_sops database table.
 * 
 */
@Entity
@Table(name="batch_sops")
@NamedQuery(name="BatchSop.findAll", query="SELECT b FROM BatchSop b")
public class BatchSop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to BatchRecord
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="batch_rec_id")
	private BatchRecord batchRecord;

	//bi-directional many-to-one association to Sop
	@ManyToOne(fetch=FetchType.LAZY)
	private Sop sop;

	public BatchSop() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public BatchRecord getBatchRecord() {
		return this.batchRecord;
	}

	public void setBatchRecord(BatchRecord batchRecord) {
		this.batchRecord = batchRecord;
	}

	public Sop getSop() {
		return this.sop;
	}

	public void setSop(Sop sop) {
		this.sop = sop;
	}

}