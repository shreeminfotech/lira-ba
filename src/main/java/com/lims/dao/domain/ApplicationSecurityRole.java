package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the application_security_roles database table.
 * 
 */
@Entity
@Table(name="application_security_roles")
@NamedQuery(name="ApplicationSecurityRole.findAll", query="SELECT a FROM ApplicationSecurityRole a")
public class ApplicationSecurityRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to Lab
	@ManyToOne(fetch=FetchType.LAZY)
	private Lab lab;

	//bi-directional many-to-one association to ApplicationSecurityRolesMap
	@OneToMany(mappedBy="applicationSecurityRole")
	private List<ApplicationSecurityRolesMap> applicationSecurityRolesMaps;

	//bi-directional many-to-one association to ApplicationUserRole
	@OneToMany(mappedBy="applicationSecurityRole")
	private List<ApplicationUserRole> applicationUserRoles;

	public ApplicationSecurityRole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Lab getLab() {
		return this.lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public List<ApplicationSecurityRolesMap> getApplicationSecurityRolesMaps() {
		return this.applicationSecurityRolesMaps;
	}

	public void setApplicationSecurityRolesMaps(List<ApplicationSecurityRolesMap> applicationSecurityRolesMaps) {
		this.applicationSecurityRolesMaps = applicationSecurityRolesMaps;
	}

	public ApplicationSecurityRolesMap addApplicationSecurityRolesMap(ApplicationSecurityRolesMap applicationSecurityRolesMap) {
		getApplicationSecurityRolesMaps().add(applicationSecurityRolesMap);
		applicationSecurityRolesMap.setApplicationSecurityRole(this);

		return applicationSecurityRolesMap;
	}

	public ApplicationSecurityRolesMap removeApplicationSecurityRolesMap(ApplicationSecurityRolesMap applicationSecurityRolesMap) {
		getApplicationSecurityRolesMaps().remove(applicationSecurityRolesMap);
		applicationSecurityRolesMap.setApplicationSecurityRole(null);

		return applicationSecurityRolesMap;
	}

	public List<ApplicationUserRole> getApplicationUserRoles() {
		return this.applicationUserRoles;
	}

	public void setApplicationUserRoles(List<ApplicationUserRole> applicationUserRoles) {
		this.applicationUserRoles = applicationUserRoles;
	}

	public ApplicationUserRole addApplicationUserRole(ApplicationUserRole applicationUserRole) {
		getApplicationUserRoles().add(applicationUserRole);
		applicationUserRole.setApplicationSecurityRole(this);

		return applicationUserRole;
	}

	public ApplicationUserRole removeApplicationUserRole(ApplicationUserRole applicationUserRole) {
		getApplicationUserRoles().remove(applicationUserRole);
		applicationUserRole.setApplicationSecurityRole(null);

		return applicationUserRole;
	}

}