package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the user_notification_status database table.
 * 
 */
@Entity
@Table(name="user_notification_status")
@NamedQuery(name="UserNotificationStatus.findAll", query="SELECT u FROM UserNotificationStatus u")
public class UserNotificationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="read_flag")
	private int readFlag;

	@Column(name="send_flag")
	private int sendFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationNotification
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="notification_id")
	private ApplicationNotification applicationNotification;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser3;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	public UserNotificationStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getReadFlag() {
		return this.readFlag;
	}

	public void setReadFlag(int readFlag) {
		this.readFlag = readFlag;
	}

	public int getSendFlag() {
		return this.sendFlag;
	}

	public void setSendFlag(int sendFlag) {
		this.sendFlag = sendFlag;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationNotification getApplicationNotification() {
		return this.applicationNotification;
	}

	public void setApplicationNotification(ApplicationNotification applicationNotification) {
		this.applicationNotification = applicationNotification;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public ApplicationUser getApplicationUser3() {
		return this.applicationUser3;
	}

	public void setApplicationUser3(ApplicationUser applicationUser3) {
		this.applicationUser3 = applicationUser3;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}