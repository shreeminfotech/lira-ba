package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the qms_oos database table.
 * 
 */
@Entity
@Table(name="qms_oos")
@NamedQuery(name="QmsOos.findAll", query="SELECT q FROM QmsOos q")
public class QmsOos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="batch_number")
	private String batchNumber;

	@Column(name="batch_section")
	private String batchSection;

	@Column(name="completed_step")
	private int completedStep;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="current_step")
	private int currentStep;

	@Lob
	@Column(name="investigation_results")
	private String investigationResults;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="oos_number")
	private String oosNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to QmsOosSop
	@OneToMany(mappedBy="qmsOo")
	private List<QmsOosSop> qmsOosSops;

	public QmsOos() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBatchNumber() {
		return this.batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getBatchSection() {
		return this.batchSection;
	}

	public void setBatchSection(String batchSection) {
		this.batchSection = batchSection;
	}

	public int getCompletedStep() {
		return this.completedStep;
	}

	public void setCompletedStep(int completedStep) {
		this.completedStep = completedStep;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCurrentStep() {
		return this.currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

	public String getInvestigationResults() {
		return this.investigationResults;
	}

	public void setInvestigationResults(String investigationResults) {
		this.investigationResults = investigationResults;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getOosNumber() {
		return this.oosNumber;
	}

	public void setOosNumber(String oosNumber) {
		this.oosNumber = oosNumber;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public List<QmsOosSop> getQmsOosSops() {
		return this.qmsOosSops;
	}

	public void setQmsOosSops(List<QmsOosSop> qmsOosSops) {
		this.qmsOosSops = qmsOosSops;
	}

	public QmsOosSop addQmsOosSop(QmsOosSop qmsOosSop) {
		getQmsOosSops().add(qmsOosSop);
		qmsOosSop.setQmsOo(this);

		return qmsOosSop;
	}

	public QmsOosSop removeQmsOosSop(QmsOosSop qmsOosSop) {
		getQmsOosSops().remove(qmsOosSop);
		qmsOosSop.setQmsOo(null);

		return qmsOosSop;
	}

}