package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the application_security_roles_map database table.
 * 
 */
@Entity
@Table(name="application_security_roles_map")
@NamedQuery(name="ApplicationSecurityRolesMap.findAll", query="SELECT a FROM ApplicationSecurityRolesMap a")
public class ApplicationSecurityRolesMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="initiator_flag")
	private Boolean initiatorFlag;

	@Column(name="approver_flag")
	private Boolean approverFlag;

	@Column(name="archive_flag")
	private Boolean archiveFlag;

	@Column(name="author_flag")
	private Boolean authorFlag;

	@Column(name="create_flag")
	private Boolean createFlag;

	@Column(name="delete_flag")
	private Boolean deleteFlag;

	@Column(name="edit_flag")
	private Boolean editFlag;

	@Column(name="print_flag")
	private Boolean printFlag;

	@Column(name="reviewer_flag")
	private Boolean reviewerFlag;

	@Column(name="run_flag")
	private Boolean runFlag;

	@Column(name="share_flag")
	private Boolean shareFlag;

	@Column(name="upload_flag")
	private Boolean uploadFlag;

	@Column(name="view_flag")
	private Boolean viewFlag;

	//bi-directional many-to-one association to ApplicationModule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="module_id")
	private ApplicationModule applicationModule;

	//bi-directional many-to-one association to ApplicationSecurityRole
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="security_id")
	private ApplicationSecurityRole applicationSecurityRole;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	public ApplicationSecurityRolesMap() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getApproverFlag() {
		return this.approverFlag;
	}

	public void setApproverFlag(Boolean approverFlag) {
		this.approverFlag = approverFlag;
	}

	public Boolean getArchiveFlag() {
		return this.archiveFlag;
	}

	public void setArchiveFlag(Boolean archiveFlag) {
		this.archiveFlag = archiveFlag;
	}
	
	public Boolean getInitiatorFlag() {
		return this.initiatorFlag;
	}

	public void setInitiatorFlag(Boolean initiatorFlag) {
		this.initiatorFlag = initiatorFlag;
	}

	
	public Boolean getAuthorFlag() {
		return this.authorFlag;
	}

	public void setAuthorFlag(Boolean authorFlag) {
		this.authorFlag = authorFlag;
	}

	public Boolean getCreateFlag() {
		return this.createFlag;
	}

	public void setCreateFlag(Boolean createFlag) {
		this.createFlag = createFlag;
	}

	public Boolean getDeleteFlag() {
		return this.deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Boolean getEditFlag() {
		return this.editFlag;
	}

	public void setEditFlag(Boolean editFlag) {
		this.editFlag = editFlag;
	}

	public Boolean getPrintFlag() {
		return this.printFlag;
	}

	public void setPrintFlag(Boolean printFlag) {
		this.printFlag = printFlag;
	}

	public Boolean getReviewerFlag() {
		return this.reviewerFlag;
	}

	public void setReviewerFlag(Boolean reviewerFlag) {
		this.reviewerFlag = reviewerFlag;
	}

	public Boolean getRunFlag() {
		return this.runFlag;
	}

	public void setRunFlag(Boolean runFlag) {
		this.runFlag = runFlag;
	}

	public Boolean getShareFlag() {
		return this.shareFlag;
	}

	public void setShareFlag(Boolean shareFlag) {
		this.shareFlag = shareFlag;
	}

	public Boolean getUploadFlag() {
		return this.uploadFlag;
	}

	public void setUploadFlag(Boolean uploadFlag) {
		this.uploadFlag = uploadFlag;
	}

	public Boolean getViewFlag() {
		return this.viewFlag;
	}

	public void setViewFlag(Boolean viewFlag) {
		this.viewFlag = viewFlag;
	}

	public ApplicationModule getApplicationModule() {
		return this.applicationModule;
	}

	public void setApplicationModule(ApplicationModule applicationModule) {
		this.applicationModule = applicationModule;
	}

	public ApplicationSecurityRole getApplicationSecurityRole() {
		return this.applicationSecurityRole;
	}

	public void setApplicationSecurityRole(ApplicationSecurityRole applicationSecurityRole) {
		this.applicationSecurityRole = applicationSecurityRole;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}