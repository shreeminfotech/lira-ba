package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the application_main_modules database table.
 * 
 */
@Entity
@Table(name="application_main_modules")
@NamedQuery(name="ApplicationMainModule.findAll", query="SELECT a FROM ApplicationMainModule a")
public class ApplicationMainModule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="enum_key")
	private String enumKey;

	@Column(name="is_active")
	private Boolean isActive;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to ApplicationModule
	@OneToMany(mappedBy="applicationMainModule")
	private List<ApplicationModule> applicationModules;

	public ApplicationMainModule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEnumKey() {
		return this.enumKey;
	}

	public void setEnumKey(String enumKey) {
		this.enumKey = enumKey;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public List<ApplicationModule> getApplicationModules() {
		return this.applicationModules;
	}

	public void setApplicationModules(List<ApplicationModule> applicationModules) {
		this.applicationModules = applicationModules;
	}

	public ApplicationModule addApplicationModule(ApplicationModule applicationModule) {
		getApplicationModules().add(applicationModule);
		applicationModule.setApplicationMainModule(this);

		return applicationModule;
	}

	public ApplicationModule removeApplicationModule(ApplicationModule applicationModule) {
		getApplicationModules().remove(applicationModule);
		applicationModule.setApplicationMainModule(null);

		return applicationModule;
	}

}