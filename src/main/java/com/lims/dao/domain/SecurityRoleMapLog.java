package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the security_role_map_log database table.
 * 
 */
@Entity
@Table(name="security_role_map_log")
@NamedQuery(name="SecurityRoleMapLog.findAll", query="SELECT s FROM SecurityRoleMapLog s")
public class SecurityRoleMapLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String field;

	@Lob
	@Column(name="new_value")
	private String newValue;

	@Lob
	@Column(name="old_value")
	private String oldValue;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	public SecurityRoleMapLog() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getField() {
		return this.field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getNewValue() {
		return this.newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getOldValue() {
		return this.oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}