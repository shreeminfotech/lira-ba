package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the labs database table.
 * 
 */
@Entity
@Table(name="labs")
@NamedQuery(name="Lab.findAll", query="SELECT l FROM Lab l")
public class Lab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationSecurityRole
	@OneToMany(mappedBy="lab")
	private List<ApplicationSecurityRole> applicationSecurityRoles;

	//bi-directional many-to-one association to BatchDetail
	@OneToMany(mappedBy="lab")
	private List<BatchDetail> batchDetails;

	//bi-directional many-to-one association to BatchTeam
	@OneToMany(mappedBy="lab")
	private List<BatchTeam> batchTeams;

	//bi-directional many-to-one association to BatchType
	@OneToMany(mappedBy="lab")
	private List<BatchType> batchTypes;

	//bi-directional many-to-one association to Address
	@ManyToOne(fetch=FetchType.LAZY)
	private Address address;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to Department
	@ManyToOne(fetch=FetchType.LAZY)
	private Department department;

	//bi-directional many-to-one association to Sop
	@OneToMany(mappedBy="lab")
	private List<Sop> sops;

	//bi-directional many-to-one association to SopType
	@OneToMany(mappedBy="lab")
	private List<SopType> sopTypes;

	public Lab() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<ApplicationSecurityRole> getApplicationSecurityRoles() {
		return this.applicationSecurityRoles;
	}

	public void setApplicationSecurityRoles(List<ApplicationSecurityRole> applicationSecurityRoles) {
		this.applicationSecurityRoles = applicationSecurityRoles;
	}

	public ApplicationSecurityRole addApplicationSecurityRole(ApplicationSecurityRole applicationSecurityRole) {
		getApplicationSecurityRoles().add(applicationSecurityRole);
		applicationSecurityRole.setLab(this);

		return applicationSecurityRole;
	}

	public ApplicationSecurityRole removeApplicationSecurityRole(ApplicationSecurityRole applicationSecurityRole) {
		getApplicationSecurityRoles().remove(applicationSecurityRole);
		applicationSecurityRole.setLab(null);

		return applicationSecurityRole;
	}

	public List<BatchDetail> getBatchDetails() {
		return this.batchDetails;
	}

	public void setBatchDetails(List<BatchDetail> batchDetails) {
		this.batchDetails = batchDetails;
	}

	public BatchDetail addBatchDetail(BatchDetail batchDetail) {
		getBatchDetails().add(batchDetail);
		batchDetail.setLab(this);

		return batchDetail;
	}

	public BatchDetail removeBatchDetail(BatchDetail batchDetail) {
		getBatchDetails().remove(batchDetail);
		batchDetail.setLab(null);

		return batchDetail;
	}

	public List<BatchTeam> getBatchTeams() {
		return this.batchTeams;
	}

	public void setBatchTeams(List<BatchTeam> batchTeams) {
		this.batchTeams = batchTeams;
	}

	public BatchTeam addBatchTeam(BatchTeam batchTeam) {
		getBatchTeams().add(batchTeam);
		batchTeam.setLab(this);

		return batchTeam;
	}

	public BatchTeam removeBatchTeam(BatchTeam batchTeam) {
		getBatchTeams().remove(batchTeam);
		batchTeam.setLab(null);

		return batchTeam;
	}

	public List<BatchType> getBatchTypes() {
		return this.batchTypes;
	}

	public void setBatchTypes(List<BatchType> batchTypes) {
		this.batchTypes = batchTypes;
	}

	public BatchType addBatchType(BatchType batchType) {
		getBatchTypes().add(batchType);
		batchType.setLab(this);

		return batchType;
	}

	public BatchType removeBatchType(BatchType batchType) {
		getBatchTypes().remove(batchType);
		batchType.setLab(null);

		return batchType;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Sop> getSops() {
		return this.sops;
	}

	public void setSops(List<Sop> sops) {
		this.sops = sops;
	}

	public Sop addSop(Sop sop) {
		getSops().add(sop);
		sop.setLab(this);

		return sop;
	}

	public Sop removeSop(Sop sop) {
		getSops().remove(sop);
		sop.setLab(null);

		return sop;
	}

	public List<SopType> getSopTypes() {
		return this.sopTypes;
	}

	public void setSopTypes(List<SopType> sopTypes) {
		this.sopTypes = sopTypes;
	}

	public SopType addSopType(SopType sopType) {
		getSopTypes().add(sopType);
		sopType.setLab(this);

		return sopType;
	}

	public SopType removeSopType(SopType sopType) {
		getSopTypes().remove(sopType);
		sopType.setLab(null);

		return sopType;
	}

}