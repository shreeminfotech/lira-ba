package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the batch_record database table.
 * 
 */
@Entity
@Table(name="batch_record")
@NamedQuery(name="BatchRecord.findAll", query="SELECT b FROM BatchRecord b")
public class BatchRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private byte[] content;

	@Column(name="content_type")
	private String contentType;

	@Column(name="document_name")
	private String documentName;

	private int filesize;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="uploaded_date")
	private Date uploadedDate;

	//bi-directional many-to-one association to ApplicationStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="status_id")
	private ApplicationStatus applicationStatus;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="uploaded_by")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to BatchDetail
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="batch_detail_id")
	private BatchDetail batchDetail;

	//bi-directional many-to-one association to BatchSop
	@OneToMany(mappedBy="batchRecord")
	private List<BatchSop> batchSops;

	public BatchRecord() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getContent() {
		return this.content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public int getFilesize() {
		return this.filesize;
	}

	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}

	public Date getUploadedDate() {
		return this.uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public ApplicationStatus getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(ApplicationStatus applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public BatchDetail getBatchDetail() {
		return this.batchDetail;
	}

	public void setBatchDetail(BatchDetail batchDetail) {
		this.batchDetail = batchDetail;
	}

	public List<BatchSop> getBatchSops() {
		return this.batchSops;
	}

	public void setBatchSops(List<BatchSop> batchSops) {
		this.batchSops = batchSops;
	}

	public BatchSop addBatchSop(BatchSop batchSop) {
		getBatchSops().add(batchSop);
		batchSop.setBatchRecord(this);

		return batchSop;
	}

	public BatchSop removeBatchSop(BatchSop batchSop) {
		getBatchSops().remove(batchSop);
		batchSop.setBatchRecord(null);

		return batchSop;
	}

}