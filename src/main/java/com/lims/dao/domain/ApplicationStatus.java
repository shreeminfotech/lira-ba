package com.lims.dao.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.StatusTypes;


/**
 * The persistent class for the application_status database table.
 * 
 */
@Entity
@Table(name="application_status")
@NamedQuery(name="ApplicationStatus.findAll", query="SELECT a FROM ApplicationStatus a")
public class ApplicationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="enum_key")
	@Enumerated(EnumType.STRING)
	private StatusTypes enumKey;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationModule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="module_id")
	private ApplicationModule applicationModule;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to BatchRecord
	@OneToMany(mappedBy="applicationStatus")
	private List<BatchRecord> batchRecords;

	//bi-directional many-to-one association to Sop
	@OneToMany(mappedBy="applicationStatus")
	private List<Sop> sops;

	public ApplicationStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public StatusTypes getEnumKey() {
		return this.enumKey;
	}

	public void setEnumKey(StatusTypes enumKey) {
		this.enumKey = enumKey;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationModule getApplicationModule() {
		return this.applicationModule;
	}

	public void setApplicationModule(ApplicationModule applicationModule) {
		this.applicationModule = applicationModule;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public List<BatchRecord> getBatchRecords() {
		return this.batchRecords;
	}

	public void setBatchRecords(List<BatchRecord> batchRecords) {
		this.batchRecords = batchRecords;
	}

	public BatchRecord addBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().add(batchRecord);
		batchRecord.setApplicationStatus(this);

		return batchRecord;
	}

	public BatchRecord removeBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().remove(batchRecord);
		batchRecord.setApplicationStatus(null);

		return batchRecord;
	}

	public List<Sop> getSops() {
		return this.sops;
	}

	public void setSops(List<Sop> sops) {
		this.sops = sops;
	}

	public Sop addSop(Sop sop) {
		getSops().add(sop);
		sop.setApplicationStatus(this);

		return sop;
	}

	public Sop removeSop(Sop sop) {
		getSops().remove(sop);
		sop.setApplicationStatus(null);

		return sop;
	}

}