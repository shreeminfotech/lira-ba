package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the application_user database table.
 * 
 */
@Entity
@Table(name="application_user")
@NamedQuery(name="ApplicationUser.findAll", query="SELECT a FROM ApplicationUser a")
public class ApplicationUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	private String description;

	private String email;

	@Column(name="emp_id")
	private String empId;

	@Column(name="first_name")
	private String firstName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_temporary")
	private Boolean isTemporary;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_login_date")
	private Date lastLoginDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_logout_date")
	private Date lastLogoutDate;

	@Column(name="last_name")
	private String lastName;

	@Column(name="mobile_no")
	private String mobileNo;

	@Column(name="notification_type")
	private String notificationType;

	private String password;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	private String username;

	//bi-directional many-to-one association to ApplicationDocumentType
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationDocumentType> applicationDocumentTypes1;

	//bi-directional many-to-one association to ApplicationDocumentType
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationDocumentType> applicationDocumentTypes2;

	//bi-directional many-to-one association to ApplicationMainModule
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationMainModule> applicationMainModules1;

	//bi-directional many-to-one association to ApplicationMainModule
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationMainModule> applicationMainModules2;

	//bi-directional many-to-one association to ApplicationModuleStep
	@OneToMany(mappedBy="applicationUser")
	private List<ApplicationModuleStep> applicationModuleSteps;

	//bi-directional many-to-one association to ApplicationModule
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationModule> applicationModules1;

	//bi-directional many-to-one association to ApplicationModule
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationModule> applicationModules2;

	//bi-directional many-to-one association to ApplicationNotification
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationNotification> applicationNotifications1;

	//bi-directional many-to-one association to ApplicationNotification
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationNotification> applicationNotifications2;

	//bi-directional many-to-one association to ApplicationRole
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationRole> applicationRoles1;

	//bi-directional many-to-one association to ApplicationRole
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationRole> applicationRoles2;

	//bi-directional many-to-one association to ApplicationSecurityRole
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationSecurityRole> applicationSecurityRoles1;

	//bi-directional many-to-one association to ApplicationSecurityRole
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationSecurityRole> applicationSecurityRoles2;

	//bi-directional many-to-one association to ApplicationStatus
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationStatus> applicationStatuses1;

	//bi-directional many-to-one association to ApplicationStatus
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationStatus> applicationStatuses2;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationUser> applicationUsers1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to ApplicationUser
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationUser> applicationUsers2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to Corporate
	@ManyToOne(fetch=FetchType.LAZY)
	private Corporate corporate;

	//bi-directional many-to-one association to ApplicationUserDocument
	@OneToMany(mappedBy="applicationUser1")
	private List<ApplicationUserDocument> applicationUserDocuments1;

	//bi-directional many-to-one association to ApplicationUserDocument
	@OneToMany(mappedBy="applicationUser2")
	private List<ApplicationUserDocument> applicationUserDocuments2;

	//bi-directional many-to-one association to ApplicationUserRole
	@OneToMany(mappedBy="applicationUser")
	private List<ApplicationUserRole> applicationUserRoles;

	//bi-directional many-to-one association to AuditLog
	@OneToMany(mappedBy="applicationUser")
	private List<AuditLog> auditLogs;

	//bi-directional many-to-one association to BatchDetail
	@OneToMany(mappedBy="applicationUser1")
	private List<BatchDetail> batchDetails1;

	//bi-directional many-to-one association to BatchDetail
	@OneToMany(mappedBy="applicationUser2")
	private List<BatchDetail> batchDetails2;

	//bi-directional many-to-one association to BatchRecord
	@OneToMany(mappedBy="applicationUser")
	private List<BatchRecord> batchRecords;

	//bi-directional many-to-one association to BatchType
	@OneToMany(mappedBy="applicationUser1")
	private List<BatchType> batchTypes1;

	//bi-directional many-to-one association to BatchType
	@OneToMany(mappedBy="applicationUser2")
	private List<BatchType> batchTypes2;

	//bi-directional many-to-one association to Corporate
	@OneToMany(mappedBy="applicationUser1")
	private List<Corporate> corporates1;

	//bi-directional many-to-one association to Corporate
	@OneToMany(mappedBy="applicationUser2")
	private List<Corporate> corporates2;

	//bi-directional many-to-one association to Country
	@OneToMany(mappedBy="applicationUser1")
	private List<Country> countries1;

	//bi-directional many-to-one association to Country
	@OneToMany(mappedBy="applicationUser2")
	private List<Country> countries2;

	//bi-directional many-to-one association to Department
	@OneToMany(mappedBy="applicationUser1")
	private List<Department> departments1;

	//bi-directional many-to-one association to Department
	@OneToMany(mappedBy="applicationUser2")
	private List<Department> departments2;

	//bi-directional many-to-one association to Lab
	@OneToMany(mappedBy="applicationUser1")
	private List<Lab> labs1;

	//bi-directional many-to-one association to Lab
	@OneToMany(mappedBy="applicationUser2")
	private List<Lab> labs2;

	//bi-directional many-to-one association to QmsDeviation
	@OneToMany(mappedBy="applicationUser1")
	private List<QmsDeviation> qmsDeviations1;

	//bi-directional many-to-one association to QmsDeviation
	@OneToMany(mappedBy="applicationUser2")
	private List<QmsDeviation> qmsDeviations2;

	//bi-directional many-to-one association to QmsOo
	@OneToMany(mappedBy="applicationUser1")
	private List<QmsOos> qmsOos1;

	//bi-directional many-to-one association to QmsOo
	@OneToMany(mappedBy="applicationUser2")
	private List<QmsOos> qmsOos2;

	//bi-directional many-to-one association to Sop
	@OneToMany(mappedBy="applicationUser1")
	private List<Sop> sops1;

	//bi-directional many-to-one association to Sop
	@OneToMany(mappedBy="applicationUser2")
	private List<Sop> sops2;

	//bi-directional many-to-one association to SopType
	@OneToMany(mappedBy="applicationUser1")
	private List<SopType> sopTypes1;

	//bi-directional many-to-one association to SopType
	@OneToMany(mappedBy="applicationUser2")
	private List<SopType> sopTypes2;

	//bi-directional many-to-one association to SopTypeVisibility
	@OneToMany(mappedBy="applicationUser")
	private List<SopTypeVisibility> sopTypeVisibilities;

	//bi-directional many-to-one association to UserNotificationStatus
	@OneToMany(mappedBy="applicationUser1")
	private List<UserNotificationStatus> userNotificationStatuses1;

	//bi-directional many-to-one association to UserNotificationStatus
	@OneToMany(mappedBy="applicationUser2")
	private List<UserNotificationStatus> userNotificationStatuses2;

	//bi-directional many-to-one association to UserNotificationStatus
	@OneToMany(mappedBy="applicationUser3")
	private List<UserNotificationStatus> userNotificationStatuses3;

	//bi-directional many-to-one association to WorkFlowMetrix
	@OneToMany(mappedBy="applicationUser")
	private List<WorkFlowMetrix> workFlowMetrixs;

	@Transient
	private String fullName;
	
	
	public ApplicationUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmpId() {
		return this.empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsTemporary() {
		return this.isTemporary;
	}

	public void setIsTemporary(Boolean isTemporary) {
		this.isTemporary = isTemporary;
	}

	public Date getLastLoginDate() {
		return this.lastLoginDate;
	}

	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public Date getLastLogoutDate() {
		return this.lastLogoutDate;
	}

	public void setLastLogoutDate(Date lastLogoutDate) {
		this.lastLogoutDate = lastLogoutDate;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<ApplicationDocumentType> getApplicationDocumentTypes1() {
		return this.applicationDocumentTypes1;
	}

	public void setApplicationDocumentTypes1(List<ApplicationDocumentType> applicationDocumentTypes1) {
		this.applicationDocumentTypes1 = applicationDocumentTypes1;
	}

	public ApplicationDocumentType addApplicationDocumentTypes1(ApplicationDocumentType applicationDocumentTypes1) {
		getApplicationDocumentTypes1().add(applicationDocumentTypes1);
		applicationDocumentTypes1.setApplicationUser1(this);

		return applicationDocumentTypes1;
	}

	public ApplicationDocumentType removeApplicationDocumentTypes1(ApplicationDocumentType applicationDocumentTypes1) {
		getApplicationDocumentTypes1().remove(applicationDocumentTypes1);
		applicationDocumentTypes1.setApplicationUser1(null);

		return applicationDocumentTypes1;
	}

	public List<ApplicationDocumentType> getApplicationDocumentTypes2() {
		return this.applicationDocumentTypes2;
	}

	public void setApplicationDocumentTypes2(List<ApplicationDocumentType> applicationDocumentTypes2) {
		this.applicationDocumentTypes2 = applicationDocumentTypes2;
	}

	public ApplicationDocumentType addApplicationDocumentTypes2(ApplicationDocumentType applicationDocumentTypes2) {
		getApplicationDocumentTypes2().add(applicationDocumentTypes2);
		applicationDocumentTypes2.setApplicationUser2(this);

		return applicationDocumentTypes2;
	}

	public ApplicationDocumentType removeApplicationDocumentTypes2(ApplicationDocumentType applicationDocumentTypes2) {
		getApplicationDocumentTypes2().remove(applicationDocumentTypes2);
		applicationDocumentTypes2.setApplicationUser2(null);

		return applicationDocumentTypes2;
	}

	public List<ApplicationMainModule> getApplicationMainModules1() {
		return this.applicationMainModules1;
	}

	public void setApplicationMainModules1(List<ApplicationMainModule> applicationMainModules1) {
		this.applicationMainModules1 = applicationMainModules1;
	}

	public ApplicationMainModule addApplicationMainModules1(ApplicationMainModule applicationMainModules1) {
		getApplicationMainModules1().add(applicationMainModules1);
		applicationMainModules1.setApplicationUser1(this);

		return applicationMainModules1;
	}

	public ApplicationMainModule removeApplicationMainModules1(ApplicationMainModule applicationMainModules1) {
		getApplicationMainModules1().remove(applicationMainModules1);
		applicationMainModules1.setApplicationUser1(null);

		return applicationMainModules1;
	}

	public List<ApplicationMainModule> getApplicationMainModules2() {
		return this.applicationMainModules2;
	}

	public void setApplicationMainModules2(List<ApplicationMainModule> applicationMainModules2) {
		this.applicationMainModules2 = applicationMainModules2;
	}

	public ApplicationMainModule addApplicationMainModules2(ApplicationMainModule applicationMainModules2) {
		getApplicationMainModules2().add(applicationMainModules2);
		applicationMainModules2.setApplicationUser2(this);

		return applicationMainModules2;
	}

	public ApplicationMainModule removeApplicationMainModules2(ApplicationMainModule applicationMainModules2) {
		getApplicationMainModules2().remove(applicationMainModules2);
		applicationMainModules2.setApplicationUser2(null);

		return applicationMainModules2;
	}

	public List<ApplicationModuleStep> getApplicationModuleSteps() {
		return this.applicationModuleSteps;
	}

	public void setApplicationModuleSteps(List<ApplicationModuleStep> applicationModuleSteps) {
		this.applicationModuleSteps = applicationModuleSteps;
	}

	public ApplicationModuleStep addApplicationModuleStep(ApplicationModuleStep applicationModuleStep) {
		getApplicationModuleSteps().add(applicationModuleStep);
		applicationModuleStep.setApplicationUser(this);

		return applicationModuleStep;
	}

	public ApplicationModuleStep removeApplicationModuleStep(ApplicationModuleStep applicationModuleStep) {
		getApplicationModuleSteps().remove(applicationModuleStep);
		applicationModuleStep.setApplicationUser(null);

		return applicationModuleStep;
	}

	public List<ApplicationModule> getApplicationModules1() {
		return this.applicationModules1;
	}

	public void setApplicationModules1(List<ApplicationModule> applicationModules1) {
		this.applicationModules1 = applicationModules1;
	}

	public ApplicationModule addApplicationModules1(ApplicationModule applicationModules1) {
		getApplicationModules1().add(applicationModules1);
		applicationModules1.setApplicationUser1(this);

		return applicationModules1;
	}

	public ApplicationModule removeApplicationModules1(ApplicationModule applicationModules1) {
		getApplicationModules1().remove(applicationModules1);
		applicationModules1.setApplicationUser1(null);

		return applicationModules1;
	}

	public List<ApplicationModule> getApplicationModules2() {
		return this.applicationModules2;
	}

	public void setApplicationModules2(List<ApplicationModule> applicationModules2) {
		this.applicationModules2 = applicationModules2;
	}

	public ApplicationModule addApplicationModules2(ApplicationModule applicationModules2) {
		getApplicationModules2().add(applicationModules2);
		applicationModules2.setApplicationUser2(this);

		return applicationModules2;
	}

	public ApplicationModule removeApplicationModules2(ApplicationModule applicationModules2) {
		getApplicationModules2().remove(applicationModules2);
		applicationModules2.setApplicationUser2(null);

		return applicationModules2;
	}

	public List<ApplicationNotification> getApplicationNotifications1() {
		return this.applicationNotifications1;
	}

	public void setApplicationNotifications1(List<ApplicationNotification> applicationNotifications1) {
		this.applicationNotifications1 = applicationNotifications1;
	}

	public ApplicationNotification addApplicationNotifications1(ApplicationNotification applicationNotifications1) {
		getApplicationNotifications1().add(applicationNotifications1);
		applicationNotifications1.setApplicationUser1(this);

		return applicationNotifications1;
	}

	public ApplicationNotification removeApplicationNotifications1(ApplicationNotification applicationNotifications1) {
		getApplicationNotifications1().remove(applicationNotifications1);
		applicationNotifications1.setApplicationUser1(null);

		return applicationNotifications1;
	}

	public List<ApplicationNotification> getApplicationNotifications2() {
		return this.applicationNotifications2;
	}

	public void setApplicationNotifications2(List<ApplicationNotification> applicationNotifications2) {
		this.applicationNotifications2 = applicationNotifications2;
	}

	public ApplicationNotification addApplicationNotifications2(ApplicationNotification applicationNotifications2) {
		getApplicationNotifications2().add(applicationNotifications2);
		applicationNotifications2.setApplicationUser2(this);

		return applicationNotifications2;
	}

	public ApplicationNotification removeApplicationNotifications2(ApplicationNotification applicationNotifications2) {
		getApplicationNotifications2().remove(applicationNotifications2);
		applicationNotifications2.setApplicationUser2(null);

		return applicationNotifications2;
	}

	public List<ApplicationRole> getApplicationRoles1() {
		return this.applicationRoles1;
	}

	public void setApplicationRoles1(List<ApplicationRole> applicationRoles1) {
		this.applicationRoles1 = applicationRoles1;
	}

	public ApplicationRole addApplicationRoles1(ApplicationRole applicationRoles1) {
		getApplicationRoles1().add(applicationRoles1);
		applicationRoles1.setApplicationUser1(this);

		return applicationRoles1;
	}

	public ApplicationRole removeApplicationRoles1(ApplicationRole applicationRoles1) {
		getApplicationRoles1().remove(applicationRoles1);
		applicationRoles1.setApplicationUser1(null);

		return applicationRoles1;
	}

	public List<ApplicationRole> getApplicationRoles2() {
		return this.applicationRoles2;
	}

	public void setApplicationRoles2(List<ApplicationRole> applicationRoles2) {
		this.applicationRoles2 = applicationRoles2;
	}

	public ApplicationRole addApplicationRoles2(ApplicationRole applicationRoles2) {
		getApplicationRoles2().add(applicationRoles2);
		applicationRoles2.setApplicationUser2(this);

		return applicationRoles2;
	}

	public ApplicationRole removeApplicationRoles2(ApplicationRole applicationRoles2) {
		getApplicationRoles2().remove(applicationRoles2);
		applicationRoles2.setApplicationUser2(null);

		return applicationRoles2;
	}

	public List<ApplicationSecurityRole> getApplicationSecurityRoles1() {
		return this.applicationSecurityRoles1;
	}

	public void setApplicationSecurityRoles1(List<ApplicationSecurityRole> applicationSecurityRoles1) {
		this.applicationSecurityRoles1 = applicationSecurityRoles1;
	}

	public ApplicationSecurityRole addApplicationSecurityRoles1(ApplicationSecurityRole applicationSecurityRoles1) {
		getApplicationSecurityRoles1().add(applicationSecurityRoles1);
		applicationSecurityRoles1.setApplicationUser1(this);

		return applicationSecurityRoles1;
	}

	public ApplicationSecurityRole removeApplicationSecurityRoles1(ApplicationSecurityRole applicationSecurityRoles1) {
		getApplicationSecurityRoles1().remove(applicationSecurityRoles1);
		applicationSecurityRoles1.setApplicationUser1(null);

		return applicationSecurityRoles1;
	}

	public List<ApplicationSecurityRole> getApplicationSecurityRoles2() {
		return this.applicationSecurityRoles2;
	}

	public void setApplicationSecurityRoles2(List<ApplicationSecurityRole> applicationSecurityRoles2) {
		this.applicationSecurityRoles2 = applicationSecurityRoles2;
	}

	public ApplicationSecurityRole addApplicationSecurityRoles2(ApplicationSecurityRole applicationSecurityRoles2) {
		getApplicationSecurityRoles2().add(applicationSecurityRoles2);
		applicationSecurityRoles2.setApplicationUser2(this);

		return applicationSecurityRoles2;
	}

	public ApplicationSecurityRole removeApplicationSecurityRoles2(ApplicationSecurityRole applicationSecurityRoles2) {
		getApplicationSecurityRoles2().remove(applicationSecurityRoles2);
		applicationSecurityRoles2.setApplicationUser2(null);

		return applicationSecurityRoles2;
	}

	public List<ApplicationStatus> getApplicationStatuses1() {
		return this.applicationStatuses1;
	}

	public void setApplicationStatuses1(List<ApplicationStatus> applicationStatuses1) {
		this.applicationStatuses1 = applicationStatuses1;
	}

	public ApplicationStatus addApplicationStatuses1(ApplicationStatus applicationStatuses1) {
		getApplicationStatuses1().add(applicationStatuses1);
		applicationStatuses1.setApplicationUser1(this);

		return applicationStatuses1;
	}

	public ApplicationStatus removeApplicationStatuses1(ApplicationStatus applicationStatuses1) {
		getApplicationStatuses1().remove(applicationStatuses1);
		applicationStatuses1.setApplicationUser1(null);

		return applicationStatuses1;
	}

	public List<ApplicationStatus> getApplicationStatuses2() {
		return this.applicationStatuses2;
	}

	public void setApplicationStatuses2(List<ApplicationStatus> applicationStatuses2) {
		this.applicationStatuses2 = applicationStatuses2;
	}

	public ApplicationStatus addApplicationStatuses2(ApplicationStatus applicationStatuses2) {
		getApplicationStatuses2().add(applicationStatuses2);
		applicationStatuses2.setApplicationUser2(this);

		return applicationStatuses2;
	}

	public ApplicationStatus removeApplicationStatuses2(ApplicationStatus applicationStatuses2) {
		getApplicationStatuses2().remove(applicationStatuses2);
		applicationStatuses2.setApplicationUser2(null);

		return applicationStatuses2;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public List<ApplicationUser> getApplicationUsers1() {
		return this.applicationUsers1;
	}

	public void setApplicationUsers1(List<ApplicationUser> applicationUsers1) {
		this.applicationUsers1 = applicationUsers1;
	}

	public ApplicationUser addApplicationUsers1(ApplicationUser applicationUsers1) {
		getApplicationUsers1().add(applicationUsers1);
		applicationUsers1.setApplicationUser1(this);

		return applicationUsers1;
	}

	public ApplicationUser removeApplicationUsers1(ApplicationUser applicationUsers1) {
		getApplicationUsers1().remove(applicationUsers1);
		applicationUsers1.setApplicationUser1(null);

		return applicationUsers1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public List<ApplicationUser> getApplicationUsers2() {
		return this.applicationUsers2;
	}

	public void setApplicationUsers2(List<ApplicationUser> applicationUsers2) {
		this.applicationUsers2 = applicationUsers2;
	}

	public ApplicationUser addApplicationUsers2(ApplicationUser applicationUsers2) {
		getApplicationUsers2().add(applicationUsers2);
		applicationUsers2.setApplicationUser2(this);

		return applicationUsers2;
	}

	public ApplicationUser removeApplicationUsers2(ApplicationUser applicationUsers2) {
		getApplicationUsers2().remove(applicationUsers2);
		applicationUsers2.setApplicationUser2(null);

		return applicationUsers2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Corporate getCorporate() {
		return this.corporate;
	}

	public void setCorporate(Corporate corporate) {
		this.corporate = corporate;
	}

	public List<ApplicationUserDocument> getApplicationUserDocuments1() {
		return this.applicationUserDocuments1;
	}

	public void setApplicationUserDocuments1(List<ApplicationUserDocument> applicationUserDocuments1) {
		this.applicationUserDocuments1 = applicationUserDocuments1;
	}

	public ApplicationUserDocument addApplicationUserDocuments1(ApplicationUserDocument applicationUserDocuments1) {
		getApplicationUserDocuments1().add(applicationUserDocuments1);
		applicationUserDocuments1.setApplicationUser1(this);

		return applicationUserDocuments1;
	}

	public ApplicationUserDocument removeApplicationUserDocuments1(ApplicationUserDocument applicationUserDocuments1) {
		getApplicationUserDocuments1().remove(applicationUserDocuments1);
		applicationUserDocuments1.setApplicationUser1(null);

		return applicationUserDocuments1;
	}

	public List<ApplicationUserDocument> getApplicationUserDocuments2() {
		return this.applicationUserDocuments2;
	}

	public void setApplicationUserDocuments2(List<ApplicationUserDocument> applicationUserDocuments2) {
		this.applicationUserDocuments2 = applicationUserDocuments2;
	}

	public ApplicationUserDocument addApplicationUserDocuments2(ApplicationUserDocument applicationUserDocuments2) {
		getApplicationUserDocuments2().add(applicationUserDocuments2);
		applicationUserDocuments2.setApplicationUser2(this);

		return applicationUserDocuments2;
	}

	public ApplicationUserDocument removeApplicationUserDocuments2(ApplicationUserDocument applicationUserDocuments2) {
		getApplicationUserDocuments2().remove(applicationUserDocuments2);
		applicationUserDocuments2.setApplicationUser2(null);

		return applicationUserDocuments2;
	}

	public List<ApplicationUserRole> getApplicationUserRoles() {
		return this.applicationUserRoles;
	}

	public void setApplicationUserRoles(List<ApplicationUserRole> applicationUserRoles) {
		this.applicationUserRoles = applicationUserRoles;
	}

	public ApplicationUserRole addApplicationUserRole(ApplicationUserRole applicationUserRole) {
		getApplicationUserRoles().add(applicationUserRole);
		applicationUserRole.setApplicationUser(this);

		return applicationUserRole;
	}

	public ApplicationUserRole removeApplicationUserRole(ApplicationUserRole applicationUserRole) {
		getApplicationUserRoles().remove(applicationUserRole);
		applicationUserRole.setApplicationUser(null);

		return applicationUserRole;
	}

	public List<AuditLog> getAuditLogs() {
		return this.auditLogs;
	}

	public void setAuditLogs(List<AuditLog> auditLogs) {
		this.auditLogs = auditLogs;
	}

	public AuditLog addAuditLog(AuditLog auditLog) {
		getAuditLogs().add(auditLog);
		auditLog.setApplicationUser(this);

		return auditLog;
	}

	public AuditLog removeAuditLog(AuditLog auditLog) {
		getAuditLogs().remove(auditLog);
		auditLog.setApplicationUser(null);

		return auditLog;
	}

	public List<BatchDetail> getBatchDetails1() {
		return this.batchDetails1;
	}

	public void setBatchDetails1(List<BatchDetail> batchDetails1) {
		this.batchDetails1 = batchDetails1;
	}

	public BatchDetail addBatchDetails1(BatchDetail batchDetails1) {
		getBatchDetails1().add(batchDetails1);
		batchDetails1.setApplicationUser1(this);

		return batchDetails1;
	}

	public BatchDetail removeBatchDetails1(BatchDetail batchDetails1) {
		getBatchDetails1().remove(batchDetails1);
		batchDetails1.setApplicationUser1(null);

		return batchDetails1;
	}

	public List<BatchDetail> getBatchDetails2() {
		return this.batchDetails2;
	}

	public void setBatchDetails2(List<BatchDetail> batchDetails2) {
		this.batchDetails2 = batchDetails2;
	}

	public BatchDetail addBatchDetails2(BatchDetail batchDetails2) {
		getBatchDetails2().add(batchDetails2);
		batchDetails2.setApplicationUser2(this);

		return batchDetails2;
	}

	public BatchDetail removeBatchDetails2(BatchDetail batchDetails2) {
		getBatchDetails2().remove(batchDetails2);
		batchDetails2.setApplicationUser2(null);

		return batchDetails2;
	}

	public List<BatchRecord> getBatchRecords() {
		return this.batchRecords;
	}

	public void setBatchRecords(List<BatchRecord> batchRecords) {
		this.batchRecords = batchRecords;
	}

	public BatchRecord addBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().add(batchRecord);
		batchRecord.setApplicationUser(this);

		return batchRecord;
	}

	public BatchRecord removeBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().remove(batchRecord);
		batchRecord.setApplicationUser(null);

		return batchRecord;
	}

	public List<BatchType> getBatchTypes1() {
		return this.batchTypes1;
	}

	public void setBatchTypes1(List<BatchType> batchTypes1) {
		this.batchTypes1 = batchTypes1;
	}

	public BatchType addBatchTypes1(BatchType batchTypes1) {
		getBatchTypes1().add(batchTypes1);
		batchTypes1.setApplicationUser1(this);

		return batchTypes1;
	}

	public BatchType removeBatchTypes1(BatchType batchTypes1) {
		getBatchTypes1().remove(batchTypes1);
		batchTypes1.setApplicationUser1(null);

		return batchTypes1;
	}

	public List<BatchType> getBatchTypes2() {
		return this.batchTypes2;
	}

	public void setBatchTypes2(List<BatchType> batchTypes2) {
		this.batchTypes2 = batchTypes2;
	}

	public BatchType addBatchTypes2(BatchType batchTypes2) {
		getBatchTypes2().add(batchTypes2);
		batchTypes2.setApplicationUser2(this);

		return batchTypes2;
	}

	public BatchType removeBatchTypes2(BatchType batchTypes2) {
		getBatchTypes2().remove(batchTypes2);
		batchTypes2.setApplicationUser2(null);

		return batchTypes2;
	}

	public List<Corporate> getCorporates1() {
		return this.corporates1;
	}

	public void setCorporates1(List<Corporate> corporates1) {
		this.corporates1 = corporates1;
	}

	public Corporate addCorporates1(Corporate corporates1) {
		getCorporates1().add(corporates1);
		corporates1.setApplicationUser1(this);

		return corporates1;
	}

	public Corporate removeCorporates1(Corporate corporates1) {
		getCorporates1().remove(corporates1);
		corporates1.setApplicationUser1(null);

		return corporates1;
	}

	public List<Corporate> getCorporates2() {
		return this.corporates2;
	}

	public void setCorporates2(List<Corporate> corporates2) {
		this.corporates2 = corporates2;
	}

	public Corporate addCorporates2(Corporate corporates2) {
		getCorporates2().add(corporates2);
		corporates2.setApplicationUser2(this);

		return corporates2;
	}

	public Corporate removeCorporates2(Corporate corporates2) {
		getCorporates2().remove(corporates2);
		corporates2.setApplicationUser2(null);

		return corporates2;
	}

	public List<Country> getCountries1() {
		return this.countries1;
	}

	public void setCountries1(List<Country> countries1) {
		this.countries1 = countries1;
	}

	public Country addCountries1(Country countries1) {
		getCountries1().add(countries1);
		countries1.setApplicationUser1(this);

		return countries1;
	}

	public Country removeCountries1(Country countries1) {
		getCountries1().remove(countries1);
		countries1.setApplicationUser1(null);

		return countries1;
	}

	public List<Country> getCountries2() {
		return this.countries2;
	}

	public void setCountries2(List<Country> countries2) {
		this.countries2 = countries2;
	}

	public Country addCountries2(Country countries2) {
		getCountries2().add(countries2);
		countries2.setApplicationUser2(this);

		return countries2;
	}

	public Country removeCountries2(Country countries2) {
		getCountries2().remove(countries2);
		countries2.setApplicationUser2(null);

		return countries2;
	}

	public List<Department> getDepartments1() {
		return this.departments1;
	}

	public void setDepartments1(List<Department> departments1) {
		this.departments1 = departments1;
	}

	public Department addDepartments1(Department departments1) {
		getDepartments1().add(departments1);
		departments1.setApplicationUser1(this);

		return departments1;
	}

	public Department removeDepartments1(Department departments1) {
		getDepartments1().remove(departments1);
		departments1.setApplicationUser1(null);

		return departments1;
	}

	public List<Department> getDepartments2() {
		return this.departments2;
	}

	public void setDepartments2(List<Department> departments2) {
		this.departments2 = departments2;
	}

	public Department addDepartments2(Department departments2) {
		getDepartments2().add(departments2);
		departments2.setApplicationUser2(this);

		return departments2;
	}

	public Department removeDepartments2(Department departments2) {
		getDepartments2().remove(departments2);
		departments2.setApplicationUser2(null);

		return departments2;
	}

	public List<Lab> getLabs1() {
		return this.labs1;
	}

	public void setLabs1(List<Lab> labs1) {
		this.labs1 = labs1;
	}

	public Lab addLabs1(Lab labs1) {
		getLabs1().add(labs1);
		labs1.setApplicationUser1(this);

		return labs1;
	}

	public Lab removeLabs1(Lab labs1) {
		getLabs1().remove(labs1);
		labs1.setApplicationUser1(null);

		return labs1;
	}

	public List<Lab> getLabs2() {
		return this.labs2;
	}

	public void setLabs2(List<Lab> labs2) {
		this.labs2 = labs2;
	}

	public Lab addLabs2(Lab labs2) {
		getLabs2().add(labs2);
		labs2.setApplicationUser2(this);

		return labs2;
	}

	public Lab removeLabs2(Lab labs2) {
		getLabs2().remove(labs2);
		labs2.setApplicationUser2(null);

		return labs2;
	}

	public List<QmsDeviation> getQmsDeviations1() {
		return this.qmsDeviations1;
	}

	public void setQmsDeviations1(List<QmsDeviation> qmsDeviations1) {
		this.qmsDeviations1 = qmsDeviations1;
	}

	public QmsDeviation addQmsDeviations1(QmsDeviation qmsDeviations1) {
		getQmsDeviations1().add(qmsDeviations1);
		qmsDeviations1.setApplicationUser1(this);

		return qmsDeviations1;
	}

	public QmsDeviation removeQmsDeviations1(QmsDeviation qmsDeviations1) {
		getQmsDeviations1().remove(qmsDeviations1);
		qmsDeviations1.setApplicationUser1(null);

		return qmsDeviations1;
	}

	public List<QmsDeviation> getQmsDeviations2() {
		return this.qmsDeviations2;
	}

	public void setQmsDeviations2(List<QmsDeviation> qmsDeviations2) {
		this.qmsDeviations2 = qmsDeviations2;
	}

	public QmsDeviation addQmsDeviations2(QmsDeviation qmsDeviations2) {
		getQmsDeviations2().add(qmsDeviations2);
		qmsDeviations2.setApplicationUser2(this);

		return qmsDeviations2;
	}

	public QmsDeviation removeQmsDeviations2(QmsDeviation qmsDeviations2) {
		getQmsDeviations2().remove(qmsDeviations2);
		qmsDeviations2.setApplicationUser2(null);

		return qmsDeviations2;
	}

	public List<QmsOos> getQmsOos1() {
		return this.qmsOos1;
	}

	public void setQmsOos1(List<QmsOos> qmsOos1) {
		this.qmsOos1 = qmsOos1;
	}

	public QmsOos addQmsOos1(QmsOos qmsOos1) {
		getQmsOos1().add(qmsOos1);
		qmsOos1.setApplicationUser1(this);

		return qmsOos1;
	}

	public QmsOos removeQmsOos1(QmsOos qmsOos1) {
		getQmsOos1().remove(qmsOos1);
		qmsOos1.setApplicationUser1(null);

		return qmsOos1;
	}

	public List<QmsOos> getQmsOos2() {
		return this.qmsOos2;
	}

	public void setQmsOos2(List<QmsOos> qmsOos2) {
		this.qmsOos2 = qmsOos2;
	}

	public QmsOos addQmsOos2(QmsOos qmsOos2) {
		getQmsOos2().add(qmsOos2);
		qmsOos2.setApplicationUser2(this);

		return qmsOos2;
	}

	public QmsOos removeQmsOos2(QmsOos qmsOos2) {
		getQmsOos2().remove(qmsOos2);
		qmsOos2.setApplicationUser2(null);

		return qmsOos2;
	}

	public List<Sop> getSops1() {
		return this.sops1;
	}

	public void setSops1(List<Sop> sops1) {
		this.sops1 = sops1;
	}

	public Sop addSops1(Sop sops1) {
		getSops1().add(sops1);
		sops1.setApplicationUser1(this);

		return sops1;
	}

	public Sop removeSops1(Sop sops1) {
		getSops1().remove(sops1);
		sops1.setApplicationUser1(null);

		return sops1;
	}

	public List<Sop> getSops2() {
		return this.sops2;
	}

	public void setSops2(List<Sop> sops2) {
		this.sops2 = sops2;
	}

	public Sop addSops2(Sop sops2) {
		getSops2().add(sops2);
		sops2.setApplicationUser2(this);

		return sops2;
	}

	public Sop removeSops2(Sop sops2) {
		getSops2().remove(sops2);
		sops2.setApplicationUser2(null);

		return sops2;
	}

	public List<SopType> getSopTypes1() {
		return this.sopTypes1;
	}

	public void setSopTypes1(List<SopType> sopTypes1) {
		this.sopTypes1 = sopTypes1;
	}

	public SopType addSopTypes1(SopType sopTypes1) {
		getSopTypes1().add(sopTypes1);
		sopTypes1.setApplicationUser1(this);

		return sopTypes1;
	}

	public SopType removeSopTypes1(SopType sopTypes1) {
		getSopTypes1().remove(sopTypes1);
		sopTypes1.setApplicationUser1(null);

		return sopTypes1;
	}

	public List<SopType> getSopTypes2() {
		return this.sopTypes2;
	}

	public void setSopTypes2(List<SopType> sopTypes2) {
		this.sopTypes2 = sopTypes2;
	}

	public SopType addSopTypes2(SopType sopTypes2) {
		getSopTypes2().add(sopTypes2);
		sopTypes2.setApplicationUser2(this);

		return sopTypes2;
	}

	public SopType removeSopTypes2(SopType sopTypes2) {
		getSopTypes2().remove(sopTypes2);
		sopTypes2.setApplicationUser2(null);

		return sopTypes2;
	}

	public List<SopTypeVisibility> getSopTypeVisibilities() {
		return this.sopTypeVisibilities;
	}

	public void setSopTypeVisibilities(List<SopTypeVisibility> sopTypeVisibilities) {
		this.sopTypeVisibilities = sopTypeVisibilities;
	}

	public SopTypeVisibility addSopTypeVisibility(SopTypeVisibility sopTypeVisibility) {
		getSopTypeVisibilities().add(sopTypeVisibility);
		sopTypeVisibility.setApplicationUser(this);

		return sopTypeVisibility;
	}

	public SopTypeVisibility removeSopTypeVisibility(SopTypeVisibility sopTypeVisibility) {
		getSopTypeVisibilities().remove(sopTypeVisibility);
		sopTypeVisibility.setApplicationUser(null);

		return sopTypeVisibility;
	}

	public List<UserNotificationStatus> getUserNotificationStatuses1() {
		return this.userNotificationStatuses1;
	}

	public void setUserNotificationStatuses1(List<UserNotificationStatus> userNotificationStatuses1) {
		this.userNotificationStatuses1 = userNotificationStatuses1;
	}

	public UserNotificationStatus addUserNotificationStatuses1(UserNotificationStatus userNotificationStatuses1) {
		getUserNotificationStatuses1().add(userNotificationStatuses1);
		userNotificationStatuses1.setApplicationUser1(this);

		return userNotificationStatuses1;
	}

	public UserNotificationStatus removeUserNotificationStatuses1(UserNotificationStatus userNotificationStatuses1) {
		getUserNotificationStatuses1().remove(userNotificationStatuses1);
		userNotificationStatuses1.setApplicationUser1(null);

		return userNotificationStatuses1;
	}

	public List<UserNotificationStatus> getUserNotificationStatuses2() {
		return this.userNotificationStatuses2;
	}

	public void setUserNotificationStatuses2(List<UserNotificationStatus> userNotificationStatuses2) {
		this.userNotificationStatuses2 = userNotificationStatuses2;
	}

	public UserNotificationStatus addUserNotificationStatuses2(UserNotificationStatus userNotificationStatuses2) {
		getUserNotificationStatuses2().add(userNotificationStatuses2);
		userNotificationStatuses2.setApplicationUser2(this);

		return userNotificationStatuses2;
	}

	public UserNotificationStatus removeUserNotificationStatuses2(UserNotificationStatus userNotificationStatuses2) {
		getUserNotificationStatuses2().remove(userNotificationStatuses2);
		userNotificationStatuses2.setApplicationUser2(null);

		return userNotificationStatuses2;
	}

	public List<UserNotificationStatus> getUserNotificationStatuses3() {
		return this.userNotificationStatuses3;
	}

	public void setUserNotificationStatuses3(List<UserNotificationStatus> userNotificationStatuses3) {
		this.userNotificationStatuses3 = userNotificationStatuses3;
	}

	public UserNotificationStatus addUserNotificationStatuses3(UserNotificationStatus userNotificationStatuses3) {
		getUserNotificationStatuses3().add(userNotificationStatuses3);
		userNotificationStatuses3.setApplicationUser3(this);

		return userNotificationStatuses3;
	}

	public UserNotificationStatus removeUserNotificationStatuses3(UserNotificationStatus userNotificationStatuses3) {
		getUserNotificationStatuses3().remove(userNotificationStatuses3);
		userNotificationStatuses3.setApplicationUser3(null);

		return userNotificationStatuses3;
	}

	public List<WorkFlowMetrix> getWorkFlowMetrixs() {
		return this.workFlowMetrixs;
	}

	public void setWorkFlowMetrixs(List<WorkFlowMetrix> workFlowMetrixs) {
		this.workFlowMetrixs = workFlowMetrixs;
	}

	public WorkFlowMetrix addWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		getWorkFlowMetrixs().add(workFlowMetrix);
		workFlowMetrix.setApplicationUser(this);

		return workFlowMetrix;
	}

	public WorkFlowMetrix removeWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		getWorkFlowMetrixs().remove(workFlowMetrix);
		workFlowMetrix.setApplicationUser(null);

		return workFlowMetrix;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}