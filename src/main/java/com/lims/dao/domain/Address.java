package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the address database table.
 * 
 */
@Entity
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address a")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="address_line1")
	private String addressLine1;

	@Column(name="address_line2")
	private String addressLine2;

	private String city;

	private String pin;

	private String state;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to Country
	@ManyToOne(fetch=FetchType.LAZY)
	private Country country;

	//bi-directional many-to-one association to Corporate
	@OneToMany(mappedBy="address1")
	private List<Corporate> corporates1;

	//bi-directional many-to-one association to Corporate
	@OneToMany(mappedBy="address2")
	private List<Corporate> corporates2;

	//bi-directional many-to-one association to Department
	@OneToMany(mappedBy="address")
	private List<Department> departments;

	//bi-directional many-to-one association to Lab
	@OneToMany(mappedBy="address")
	private List<Lab> labs;

	public Address() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPin() {
		return this.pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public List<Corporate> getCorporates1() {
		return this.corporates1;
	}

	public void setCorporates1(List<Corporate> corporates1) {
		this.corporates1 = corporates1;
	}

	public Corporate addCorporates1(Corporate corporates1) {
		getCorporates1().add(corporates1);
		corporates1.setAddress1(this);

		return corporates1;
	}

	public Corporate removeCorporates1(Corporate corporates1) {
		getCorporates1().remove(corporates1);
		corporates1.setAddress1(null);

		return corporates1;
	}

	public List<Corporate> getCorporates2() {
		return this.corporates2;
	}

	public void setCorporates2(List<Corporate> corporates2) {
		this.corporates2 = corporates2;
	}

	public Corporate addCorporates2(Corporate corporates2) {
		getCorporates2().add(corporates2);
		corporates2.setAddress2(this);

		return corporates2;
	}

	public Corporate removeCorporates2(Corporate corporates2) {
		getCorporates2().remove(corporates2);
		corporates2.setAddress2(null);

		return corporates2;
	}

	public List<Department> getDepartments() {
		return this.departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public Department addDepartment(Department department) {
		getDepartments().add(department);
		department.setAddress(this);

		return department;
	}

	public Department removeDepartment(Department department) {
		getDepartments().remove(department);
		department.setAddress(null);

		return department;
	}

	public List<Lab> getLabs() {
		return this.labs;
	}

	public void setLabs(List<Lab> labs) {
		this.labs = labs;
	}

	public Lab addLab(Lab lab) {
		getLabs().add(lab);
		lab.setAddress(this);

		return lab;
	}

	public Lab removeLab(Lab lab) {
		getLabs().remove(lab);
		lab.setAddress(null);

		return lab;
	}

}