package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the qms_oos_sops database table.
 * 
 */
@Entity
@Table(name="qms_oos_sops")
@NamedQuery(name="QmsOosSop.findAll", query="SELECT q FROM QmsOosSop q")
public class QmsOosSop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to QmsOo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="oos_id")
	private QmsOos qmsOo;

	//bi-directional many-to-one association to Sop
	@ManyToOne(fetch=FetchType.LAZY)
	private Sop sop;

	public QmsOosSop() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public QmsOos getQmsOo() {
		return this.qmsOo;
	}

	public void setQmsOo(QmsOos qmsOo) {
		this.qmsOo = qmsOo;
	}

	public Sop getSop() {
		return this.sop;
	}

	public void setSop(Sop sop) {
		this.sop = sop;
	}

}