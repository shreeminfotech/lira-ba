package com.lims.dao.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.EntityTypes;


/**
 * The persistent class for the application_modules database table.
 * 
 */
@Entity
@Table(name="application_modules")
@NamedQuery(name="ApplicationModule.findAll", query="SELECT a FROM ApplicationModule a")
public class ApplicationModule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="enum_key")
	@Enumerated(EnumType.STRING)
	private EntityTypes enumKey;

	@Column(name="is_active")
	private Boolean isActive;

	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationModuleStep
	@OneToMany(mappedBy="applicationModule")
	private List<ApplicationModuleStep> applicationModuleSteps;

	//bi-directional many-to-one association to ApplicationMainModule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="main_module_id")
	private ApplicationMainModule applicationMainModule;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to ApplicationSecurityRolesMap
	@OneToMany(mappedBy="applicationModule")
	private List<ApplicationSecurityRolesMap> applicationSecurityRolesMaps;

	//bi-directional many-to-one association to ApplicationStatus
	@OneToMany(mappedBy="applicationModule")
	private List<ApplicationStatus> applicationStatuses;

	//bi-directional many-to-one association to AuditLog
	@OneToMany(mappedBy="applicationModule")
	private List<AuditLog> auditLogs;

	public ApplicationModule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public EntityTypes getEnumKey() {
		return this.enumKey;
	}

	public void setEnumKey(EntityTypes enumKey) {
		this.enumKey = enumKey;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<ApplicationModuleStep> getApplicationModuleSteps() {
		return this.applicationModuleSteps;
	}

	public void setApplicationModuleSteps(List<ApplicationModuleStep> applicationModuleSteps) {
		this.applicationModuleSteps = applicationModuleSteps;
	}

	public ApplicationModuleStep addApplicationModuleStep(ApplicationModuleStep applicationModuleStep) {
		getApplicationModuleSteps().add(applicationModuleStep);
		applicationModuleStep.setApplicationModule(this);

		return applicationModuleStep;
	}

	public ApplicationModuleStep removeApplicationModuleStep(ApplicationModuleStep applicationModuleStep) {
		getApplicationModuleSteps().remove(applicationModuleStep);
		applicationModuleStep.setApplicationModule(null);

		return applicationModuleStep;
	}

	public ApplicationMainModule getApplicationMainModule() {
		return this.applicationMainModule;
	}

	public void setApplicationMainModule(ApplicationMainModule applicationMainModule) {
		this.applicationMainModule = applicationMainModule;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public List<ApplicationSecurityRolesMap> getApplicationSecurityRolesMaps() {
		return this.applicationSecurityRolesMaps;
	}

	public void setApplicationSecurityRolesMaps(List<ApplicationSecurityRolesMap> applicationSecurityRolesMaps) {
		this.applicationSecurityRolesMaps = applicationSecurityRolesMaps;
	}

	public ApplicationSecurityRolesMap addApplicationSecurityRolesMap(ApplicationSecurityRolesMap applicationSecurityRolesMap) {
		getApplicationSecurityRolesMaps().add(applicationSecurityRolesMap);
		applicationSecurityRolesMap.setApplicationModule(this);

		return applicationSecurityRolesMap;
	}

	public ApplicationSecurityRolesMap removeApplicationSecurityRolesMap(ApplicationSecurityRolesMap applicationSecurityRolesMap) {
		getApplicationSecurityRolesMaps().remove(applicationSecurityRolesMap);
		applicationSecurityRolesMap.setApplicationModule(null);

		return applicationSecurityRolesMap;
	}

	public List<ApplicationStatus> getApplicationStatuses() {
		return this.applicationStatuses;
	}

	public void setApplicationStatuses(List<ApplicationStatus> applicationStatuses) {
		this.applicationStatuses = applicationStatuses;
	}

	public ApplicationStatus addApplicationStatus(ApplicationStatus applicationStatus) {
		getApplicationStatuses().add(applicationStatus);
		applicationStatus.setApplicationModule(this);

		return applicationStatus;
	}

	public ApplicationStatus removeApplicationStatus(ApplicationStatus applicationStatus) {
		getApplicationStatuses().remove(applicationStatus);
		applicationStatus.setApplicationModule(null);

		return applicationStatus;
	}

	public List<AuditLog> getAuditLogs() {
		return this.auditLogs;
	}

	public void setAuditLogs(List<AuditLog> auditLogs) {
		this.auditLogs = auditLogs;
	}

	public AuditLog addAuditLog(AuditLog auditLog) {
		getAuditLogs().add(auditLog);
		auditLog.setApplicationModule(this);

		return auditLog;
	}

	public AuditLog removeAuditLog(AuditLog auditLog) {
		getAuditLogs().remove(auditLog);
		auditLog.setApplicationModule(null);

		return auditLog;
	}

}