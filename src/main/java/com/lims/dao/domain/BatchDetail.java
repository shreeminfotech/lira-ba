package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the batch_details database table.
 * 
 */
@Entity
@Table(name="batch_details")
@NamedQuery(name="BatchDetail.findAll", query="SELECT b FROM BatchDetail b")
public class BatchDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	@Column(name="batch_desciption")
	private String batchDesciption;

	@Column(name="batch_number")
	private String batchNumber;

	@Column(name="batch_title")
	private String batchTitle;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to BatchType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="batch_type")
	private BatchType batchTypeBean;

	//bi-directional many-to-one association to Lab
	@ManyToOne(fetch=FetchType.LAZY)
	private Lab lab;

	//bi-directional many-to-one association to BatchRecord
	@OneToMany(mappedBy="batchDetail")
	private List<BatchRecord> batchRecords;

	//bi-directional many-to-one association to BatchTeam
	@OneToMany(mappedBy="batchDetail")
	private List<BatchTeam> batchTeams;

	public BatchDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBatchDesciption() {
		return this.batchDesciption;
	}

	public void setBatchDesciption(String batchDesciption) {
		this.batchDesciption = batchDesciption;
	}

	public String getBatchNumber() {
		return this.batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getBatchTitle() {
		return this.batchTitle;
	}

	public void setBatchTitle(String batchTitle) {
		this.batchTitle = batchTitle;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public BatchType getBatchTypeBean() {
		return this.batchTypeBean;
	}

	public void setBatchTypeBean(BatchType batchTypeBean) {
		this.batchTypeBean = batchTypeBean;
	}

	public Lab getLab() {
		return this.lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public List<BatchRecord> getBatchRecords() {
		return this.batchRecords;
	}

	public void setBatchRecords(List<BatchRecord> batchRecords) {
		this.batchRecords = batchRecords;
	}

	public BatchRecord addBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().add(batchRecord);
		batchRecord.setBatchDetail(this);

		return batchRecord;
	}

	public BatchRecord removeBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().remove(batchRecord);
		batchRecord.setBatchDetail(null);

		return batchRecord;
	}

	public List<BatchTeam> getBatchTeams() {
		return this.batchTeams;
	}

	public void setBatchTeams(List<BatchTeam> batchTeams) {
		this.batchTeams = batchTeams;
	}

	public BatchTeam addBatchTeam(BatchTeam batchTeam) {
		getBatchTeams().add(batchTeam);
		batchTeam.setBatchDetail(this);

		return batchTeam;
	}

	public BatchTeam removeBatchTeam(BatchTeam batchTeam) {
		getBatchTeams().remove(batchTeam);
		batchTeam.setBatchDetail(null);

		return batchTeam;
	}

}