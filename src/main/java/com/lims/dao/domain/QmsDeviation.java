package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the qms_deviation database table.
 * 
 */
@Entity
@Table(name="qms_deviation")
@NamedQuery(name="QmsDeviation.findAll", query="SELECT q FROM QmsDeviation q")
public class QmsDeviation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="batch_number")
	private String batchNumber;

	@Column(name="completed_step")
	private int completedStep;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="current_step")
	private int currentStep;

	@Column(name="deviation_number")
	private String deviationNumber;

	@Column(name="is_active")
	private Boolean isActive;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to QmsDeviationSop
	@OneToMany(mappedBy="qmsDeviation")
	private List<QmsDeviationSop> qmsDeviationSops;

	public QmsDeviation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBatchNumber() {
		return this.batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public int getCompletedStep() {
		return this.completedStep;
	}

	public void setCompletedStep(int completedStep) {
		this.completedStep = completedStep;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCurrentStep() {
		return this.currentStep;
	}

	public void setCurrentStep(int currentStep) {
		this.currentStep = currentStep;
	}

	public String getDeviationNumber() {
		return this.deviationNumber;
	}

	public void setDeviationNumber(String deviationNumber) {
		this.deviationNumber = deviationNumber;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public List<QmsDeviationSop> getQmsDeviationSops() {
		return this.qmsDeviationSops;
	}

	public void setQmsDeviationSops(List<QmsDeviationSop> qmsDeviationSops) {
		this.qmsDeviationSops = qmsDeviationSops;
	}

	public QmsDeviationSop addQmsDeviationSop(QmsDeviationSop qmsDeviationSop) {
		getQmsDeviationSops().add(qmsDeviationSop);
		qmsDeviationSop.setQmsDeviation(this);

		return qmsDeviationSop;
	}

	public QmsDeviationSop removeQmsDeviationSop(QmsDeviationSop qmsDeviationSop) {
		getQmsDeviationSops().remove(qmsDeviationSop);
		qmsDeviationSop.setQmsDeviation(null);

		return qmsDeviationSop;
	}

}