package com.lims.dao.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.EntityTypes;


/**
 * The persistent class for the application_scheduler database table.
 * 
 */
@Entity
@Table(name="application_scheduler")
@NamedQuery(name="ApplicationScheduler.findAll", query="SELECT a FROM ApplicationScheduler a")
public class ApplicationScheduler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_date")
	private Date endDate;

	@Column(name="end_time")
	private String endTime;

	@Column(name="entity_id")
	private int entityId;

	@Column(name="entity_type")
	@Enumerated(EnumType.STRING)
	private EntityTypes entityType;

	@Column(name="frequency_type")
	private String frequencyType;

	@Column(name="repeat_count")
	private int repeatCount;

	@Column(name="repeat_on")
	private String repeatOn;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_date")
	private Date startDate;

	@Column(name="start_time")
	private String startTime;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	public ApplicationScheduler() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return this.endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public EntityTypes getEntityType() {
		return this.entityType;
	}

	public void setEntityType(EntityTypes entityType) {
		this.entityType = entityType;
	}

	public String getFrequencyType() {
		return this.frequencyType;
	}

	public void setFrequencyType(String frequencyType) {
		this.frequencyType = frequencyType;
	}

	public int getRepeatCount() {
		return this.repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	public String getRepeatOn() {
		return this.repeatOn;
	}

	public void setRepeatOn(String repeatOn) {
		this.repeatOn = repeatOn;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return this.startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}