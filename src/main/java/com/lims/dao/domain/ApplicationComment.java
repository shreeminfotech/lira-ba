package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the application_comments database table.
 * 
 */
@Entity
@Table(name="application_comments")
@NamedQuery(name="ApplicationComment.findAll", query="SELECT a FROM ApplicationComment a")
public class ApplicationComment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String comments;
	
	@Column(name = "freeze")
	private Boolean freeze;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to WorkFlowMetrix
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="wf_metrix_id")
	private WorkFlowMetrix workFlowMetrix;

	public ApplicationComment() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public WorkFlowMetrix getWorkFlowMetrix() {
		return this.workFlowMetrix;
	}

	public void setWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		this.workFlowMetrix = workFlowMetrix;
	}

	public Boolean getFreeze() {
		return freeze;
	}

	public void setFreeze(Boolean freeze) {
		this.freeze = freeze;
	}
	
	

}