package com.lims.dao.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.EntityTypes;


/**
 * The persistent class for the application_notification database table.
 * 
 */
@Entity
@Table(name="application_notification")
@NamedQuery(name="ApplicationNotification.findAll", query="SELECT a FROM ApplicationNotification a")
public class ApplicationNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="entity_id")
	private int entityId;

	@Column(name="entity_type")
	@Enumerated(EnumType.STRING)
	private EntityTypes entityType;

	@Column(name="notification_message")
	private String notificationMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to UserNotificationStatus
	@OneToMany(mappedBy="applicationNotification")
	private List<UserNotificationStatus> userNotificationStatuses;

	public ApplicationNotification() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public EntityTypes getEntityType() {
		return this.entityType;
	}

	public void setEntityType(EntityTypes entityType) {
		this.entityType = entityType;
	}

	public String getNotificationMessage() {
		return this.notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public List<UserNotificationStatus> getUserNotificationStatuses() {
		return this.userNotificationStatuses;
	}

	public void setUserNotificationStatuses(List<UserNotificationStatus> userNotificationStatuses) {
		this.userNotificationStatuses = userNotificationStatuses;
	}

	public UserNotificationStatus addUserNotificationStatus(UserNotificationStatus userNotificationStatus) {
		getUserNotificationStatuses().add(userNotificationStatus);
		userNotificationStatus.setApplicationNotification(this);

		return userNotificationStatus;
	}

	public UserNotificationStatus removeUserNotificationStatus(UserNotificationStatus userNotificationStatus) {
		getUserNotificationStatuses().remove(userNotificationStatus);
		userNotificationStatus.setApplicationNotification(null);

		return userNotificationStatus;
	}

}