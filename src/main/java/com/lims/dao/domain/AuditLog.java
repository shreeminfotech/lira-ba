package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the audit_log database table.
 * 
 */
@Entity
@Table(name="audit_log")
@NamedQuery(name="AuditLog.findAll", query="SELECT a FROM AuditLog a")
public class AuditLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="action_type")
	private String actionType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="entity_id")
	private int entityId;

	//bi-directional many-to-one association to Address
	@OneToMany(mappedBy="auditLog")
	private List<Address> addresses;

	//bi-directional many-to-one association to AddressLog
	@OneToMany(mappedBy="auditLog")
	private List<AddressLog> addressLogs;

	//bi-directional many-to-one association to ApplicationComment
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationComment> applicationComments;

	//bi-directional many-to-one association to ApplicationDocumentCommentsLog
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationDocumentCommentsLog> applicationDocumentCommentsLogs;

	//bi-directional many-to-one association to ApplicationNotification
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationNotification> applicationNotifications;

	//bi-directional many-to-one association to ApplicationNotificationLog
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationNotificationLog> applicationNotificationLogs;

	//bi-directional many-to-one association to ApplicationScheduler
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationScheduler> applicationSchedulers;

	//bi-directional many-to-one association to ApplicationSchedulerLog
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationSchedulerLog> applicationSchedulerLogs;

	//bi-directional many-to-one association to ApplicationSecurityRole
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationSecurityRole> applicationSecurityRoles;

	//bi-directional many-to-one association to ApplicationSecurityRolesMap
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationSecurityRolesMap> applicationSecurityRolesMaps;

	//bi-directional many-to-one association to ApplicationUser
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationUser> applicationUsers;

	//bi-directional many-to-one association to ApplicationUserLog
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationUserLog> applicationUserLogs;

	//bi-directional many-to-one association to ApplicationUserRole
	@OneToMany(mappedBy="auditLog")
	private List<ApplicationUserRole> applicationUserRoles;

	//bi-directional many-to-one association to ApplicationModule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="module_id")
	private ApplicationModule applicationModule;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to BatchDetail
	@OneToMany(mappedBy="auditLog")
	private List<BatchDetail> batchDetails;

	//bi-directional many-to-one association to BatchDetailsLog
	@OneToMany(mappedBy="auditLog")
	private List<BatchDetailsLog> batchDetailsLogs;

	//bi-directional many-to-one association to BatchRecord
	@OneToMany(mappedBy="auditLog")
	private List<BatchRecord> batchRecords;

	//bi-directional many-to-one association to BatchRecordLog
	@OneToMany(mappedBy="auditLog")
	private List<BatchRecordLog> batchRecordLogs;

	//bi-directional many-to-one association to BatchSop
	@OneToMany(mappedBy="auditLog")
	private List<BatchSop> batchSops;

	//bi-directional many-to-one association to BatchTeam
	@OneToMany(mappedBy="auditLog")
	private List<BatchTeam> batchTeams;

	//bi-directional many-to-one association to BatchTeamLog
	@OneToMany(mappedBy="auditLog")
	private List<BatchTeamLog> batchTeamLogs;

	//bi-directional many-to-one association to BatchType
	@OneToMany(mappedBy="auditLog")
	private List<BatchType> batchTypes;

	//bi-directional many-to-one association to BatchTypeLog
	@OneToMany(mappedBy="auditLog")
	private List<BatchTypeLog> batchTypeLogs;

	//bi-directional many-to-one association to Corporate
	@OneToMany(mappedBy="auditLog")
	private List<Corporate> corporates;

	//bi-directional many-to-one association to CorporateLog
	@OneToMany(mappedBy="auditLog")
	private List<CorporateLog> corporateLogs;

	//bi-directional many-to-one association to Department
	@OneToMany(mappedBy="auditLog")
	private List<Department> departments;

	//bi-directional many-to-one association to DepartmentsLog
	@OneToMany(mappedBy="auditLog")
	private List<DepartmentsLog> departmentsLogs;

	//bi-directional many-to-one association to Lab
	@OneToMany(mappedBy="auditLog")
	private List<Lab> labs;

	//bi-directional many-to-one association to LabsLog
	@OneToMany(mappedBy="auditLog")
	private List<LabsLog> labsLogs;

	//bi-directional many-to-one association to QmsDeviation
	@OneToMany(mappedBy="auditLog")
	private List<QmsDeviation> qmsDeviations;

	//bi-directional many-to-one association to QmsDeviationLog
	@OneToMany(mappedBy="auditLog")
	private List<QmsDeviationLog> qmsDeviationLogs;

	//bi-directional many-to-one association to QmsOo
	@OneToMany(mappedBy="auditLog")
	private List<QmsOos> qmsOos;

	//bi-directional many-to-one association to QmsOosLog
	@OneToMany(mappedBy="auditLog")
	private List<QmsOosLog> qmsOosLogs;

	//bi-directional many-to-one association to SecurityRoleLog
	@OneToMany(mappedBy="auditLog")
	private List<SecurityRoleLog> securityRoleLogs;

	//bi-directional many-to-one association to SecurityRoleMapLog
	@OneToMany(mappedBy="auditLog")
	private List<SecurityRoleMapLog> securityRoleMapLogs;

	//bi-directional many-to-one association to Sop
	@OneToMany(mappedBy="auditLog")
	private List<Sop> sops;

	//bi-directional many-to-one association to SopLog
	@OneToMany(mappedBy="auditLog")
	private List<SopLog> sopLogs;

	//bi-directional many-to-one association to SopType
	@OneToMany(mappedBy="auditLog")
	private List<SopType> sopTypes;

	//bi-directional many-to-one association to SopTypeLog
	@OneToMany(mappedBy="auditLog")
	private List<SopTypeLog> sopTypeLogs;

	//bi-directional many-to-one association to SopTypeVisibility
	@OneToMany(mappedBy="auditLog")
	private List<SopTypeVisibility> sopTypeVisibilities;

	//bi-directional many-to-one association to UserNotificationStatus
	@OneToMany(mappedBy="auditLog")
	private List<UserNotificationStatus> userNotificationStatuses;

	//bi-directional many-to-one association to UserNotificationStatusLog
	@OneToMany(mappedBy="auditLog")
	private List<UserNotificationStatusLog> userNotificationStatusLogs;

	//bi-directional many-to-one association to WorkFlowMetrix
	@OneToMany(mappedBy="auditLog")
	private List<WorkFlowMetrix> workFlowMetrixs;

	//bi-directional many-to-one association to WorkFlowMetrixLog
	@OneToMany(mappedBy="auditLog")
	private List<WorkFlowMetrixLog> workFlowMetrixLogs;

	public AuditLog() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActionType() {
		return this.actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public List<Address> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public Address addAddress(Address address) {
		getAddresses().add(address);
		address.setAuditLog(this);

		return address;
	}

	public Address removeAddress(Address address) {
		getAddresses().remove(address);
		address.setAuditLog(null);

		return address;
	}

	public List<AddressLog> getAddressLogs() {
		return this.addressLogs;
	}

	public void setAddressLogs(List<AddressLog> addressLogs) {
		this.addressLogs = addressLogs;
	}

	public AddressLog addAddressLog(AddressLog addressLog) {
		getAddressLogs().add(addressLog);
		addressLog.setAuditLog(this);

		return addressLog;
	}

	public AddressLog removeAddressLog(AddressLog addressLog) {
		getAddressLogs().remove(addressLog);
		addressLog.setAuditLog(null);

		return addressLog;
	}

	public List<ApplicationComment> getApplicationComments() {
		return this.applicationComments;
	}

	public void setApplicationComments(List<ApplicationComment> applicationComments) {
		this.applicationComments = applicationComments;
	}

	public ApplicationComment addApplicationComment(ApplicationComment applicationComment) {
		getApplicationComments().add(applicationComment);
		applicationComment.setAuditLog(this);

		return applicationComment;
	}

	public ApplicationComment removeApplicationComment(ApplicationComment applicationComment) {
		getApplicationComments().remove(applicationComment);
		applicationComment.setAuditLog(null);

		return applicationComment;
	}

	public List<ApplicationDocumentCommentsLog> getApplicationDocumentCommentsLogs() {
		return this.applicationDocumentCommentsLogs;
	}

	public void setApplicationDocumentCommentsLogs(List<ApplicationDocumentCommentsLog> applicationDocumentCommentsLogs) {
		this.applicationDocumentCommentsLogs = applicationDocumentCommentsLogs;
	}

	public ApplicationDocumentCommentsLog addApplicationDocumentCommentsLog(ApplicationDocumentCommentsLog applicationDocumentCommentsLog) {
		getApplicationDocumentCommentsLogs().add(applicationDocumentCommentsLog);
		applicationDocumentCommentsLog.setAuditLog(this);

		return applicationDocumentCommentsLog;
	}

	public ApplicationDocumentCommentsLog removeApplicationDocumentCommentsLog(ApplicationDocumentCommentsLog applicationDocumentCommentsLog) {
		getApplicationDocumentCommentsLogs().remove(applicationDocumentCommentsLog);
		applicationDocumentCommentsLog.setAuditLog(null);

		return applicationDocumentCommentsLog;
	}

	public List<ApplicationNotification> getApplicationNotifications() {
		return this.applicationNotifications;
	}

	public void setApplicationNotifications(List<ApplicationNotification> applicationNotifications) {
		this.applicationNotifications = applicationNotifications;
	}

	public ApplicationNotification addApplicationNotification(ApplicationNotification applicationNotification) {
		getApplicationNotifications().add(applicationNotification);
		applicationNotification.setAuditLog(this);

		return applicationNotification;
	}

	public ApplicationNotification removeApplicationNotification(ApplicationNotification applicationNotification) {
		getApplicationNotifications().remove(applicationNotification);
		applicationNotification.setAuditLog(null);

		return applicationNotification;
	}

	public List<ApplicationNotificationLog> getApplicationNotificationLogs() {
		return this.applicationNotificationLogs;
	}

	public void setApplicationNotificationLogs(List<ApplicationNotificationLog> applicationNotificationLogs) {
		this.applicationNotificationLogs = applicationNotificationLogs;
	}

	public ApplicationNotificationLog addApplicationNotificationLog(ApplicationNotificationLog applicationNotificationLog) {
		getApplicationNotificationLogs().add(applicationNotificationLog);
		applicationNotificationLog.setAuditLog(this);

		return applicationNotificationLog;
	}

	public ApplicationNotificationLog removeApplicationNotificationLog(ApplicationNotificationLog applicationNotificationLog) {
		getApplicationNotificationLogs().remove(applicationNotificationLog);
		applicationNotificationLog.setAuditLog(null);

		return applicationNotificationLog;
	}

	public List<ApplicationScheduler> getApplicationSchedulers() {
		return this.applicationSchedulers;
	}

	public void setApplicationSchedulers(List<ApplicationScheduler> applicationSchedulers) {
		this.applicationSchedulers = applicationSchedulers;
	}

	public ApplicationScheduler addApplicationScheduler(ApplicationScheduler applicationScheduler) {
		getApplicationSchedulers().add(applicationScheduler);
		applicationScheduler.setAuditLog(this);

		return applicationScheduler;
	}

	public ApplicationScheduler removeApplicationScheduler(ApplicationScheduler applicationScheduler) {
		getApplicationSchedulers().remove(applicationScheduler);
		applicationScheduler.setAuditLog(null);

		return applicationScheduler;
	}

	public List<ApplicationSchedulerLog> getApplicationSchedulerLogs() {
		return this.applicationSchedulerLogs;
	}

	public void setApplicationSchedulerLogs(List<ApplicationSchedulerLog> applicationSchedulerLogs) {
		this.applicationSchedulerLogs = applicationSchedulerLogs;
	}

	public ApplicationSchedulerLog addApplicationSchedulerLog(ApplicationSchedulerLog applicationSchedulerLog) {
		getApplicationSchedulerLogs().add(applicationSchedulerLog);
		applicationSchedulerLog.setAuditLog(this);

		return applicationSchedulerLog;
	}

	public ApplicationSchedulerLog removeApplicationSchedulerLog(ApplicationSchedulerLog applicationSchedulerLog) {
		getApplicationSchedulerLogs().remove(applicationSchedulerLog);
		applicationSchedulerLog.setAuditLog(null);

		return applicationSchedulerLog;
	}

	public List<ApplicationSecurityRole> getApplicationSecurityRoles() {
		return this.applicationSecurityRoles;
	}

	public void setApplicationSecurityRoles(List<ApplicationSecurityRole> applicationSecurityRoles) {
		this.applicationSecurityRoles = applicationSecurityRoles;
	}

	public ApplicationSecurityRole addApplicationSecurityRole(ApplicationSecurityRole applicationSecurityRole) {
		getApplicationSecurityRoles().add(applicationSecurityRole);
		applicationSecurityRole.setAuditLog(this);

		return applicationSecurityRole;
	}

	public ApplicationSecurityRole removeApplicationSecurityRole(ApplicationSecurityRole applicationSecurityRole) {
		getApplicationSecurityRoles().remove(applicationSecurityRole);
		applicationSecurityRole.setAuditLog(null);

		return applicationSecurityRole;
	}

	public List<ApplicationSecurityRolesMap> getApplicationSecurityRolesMaps() {
		return this.applicationSecurityRolesMaps;
	}

	public void setApplicationSecurityRolesMaps(List<ApplicationSecurityRolesMap> applicationSecurityRolesMaps) {
		this.applicationSecurityRolesMaps = applicationSecurityRolesMaps;
	}

	public ApplicationSecurityRolesMap addApplicationSecurityRolesMap(ApplicationSecurityRolesMap applicationSecurityRolesMap) {
		getApplicationSecurityRolesMaps().add(applicationSecurityRolesMap);
		applicationSecurityRolesMap.setAuditLog(this);

		return applicationSecurityRolesMap;
	}

	public ApplicationSecurityRolesMap removeApplicationSecurityRolesMap(ApplicationSecurityRolesMap applicationSecurityRolesMap) {
		getApplicationSecurityRolesMaps().remove(applicationSecurityRolesMap);
		applicationSecurityRolesMap.setAuditLog(null);

		return applicationSecurityRolesMap;
	}

	public List<ApplicationUser> getApplicationUsers() {
		return this.applicationUsers;
	}

	public void setApplicationUsers(List<ApplicationUser> applicationUsers) {
		this.applicationUsers = applicationUsers;
	}

	public ApplicationUser addApplicationUser(ApplicationUser applicationUser) {
		getApplicationUsers().add(applicationUser);
		applicationUser.setAuditLog(this);

		return applicationUser;
	}

	public ApplicationUser removeApplicationUser(ApplicationUser applicationUser) {
		getApplicationUsers().remove(applicationUser);
		applicationUser.setAuditLog(null);

		return applicationUser;
	}

	public List<ApplicationUserLog> getApplicationUserLogs() {
		return this.applicationUserLogs;
	}

	public void setApplicationUserLogs(List<ApplicationUserLog> applicationUserLogs) {
		this.applicationUserLogs = applicationUserLogs;
	}

	public ApplicationUserLog addApplicationUserLog(ApplicationUserLog applicationUserLog) {
		getApplicationUserLogs().add(applicationUserLog);
		applicationUserLog.setAuditLog(this);

		return applicationUserLog;
	}

	public ApplicationUserLog removeApplicationUserLog(ApplicationUserLog applicationUserLog) {
		getApplicationUserLogs().remove(applicationUserLog);
		applicationUserLog.setAuditLog(null);

		return applicationUserLog;
	}

	public List<ApplicationUserRole> getApplicationUserRoles() {
		return this.applicationUserRoles;
	}

	public void setApplicationUserRoles(List<ApplicationUserRole> applicationUserRoles) {
		this.applicationUserRoles = applicationUserRoles;
	}

	public ApplicationUserRole addApplicationUserRole(ApplicationUserRole applicationUserRole) {
		getApplicationUserRoles().add(applicationUserRole);
		applicationUserRole.setAuditLog(this);

		return applicationUserRole;
	}

	public ApplicationUserRole removeApplicationUserRole(ApplicationUserRole applicationUserRole) {
		getApplicationUserRoles().remove(applicationUserRole);
		applicationUserRole.setAuditLog(null);

		return applicationUserRole;
	}

	public ApplicationModule getApplicationModule() {
		return this.applicationModule;
	}

	public void setApplicationModule(ApplicationModule applicationModule) {
		this.applicationModule = applicationModule;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public List<BatchDetail> getBatchDetails() {
		return this.batchDetails;
	}

	public void setBatchDetails(List<BatchDetail> batchDetails) {
		this.batchDetails = batchDetails;
	}

	public BatchDetail addBatchDetail(BatchDetail batchDetail) {
		getBatchDetails().add(batchDetail);
		batchDetail.setAuditLog(this);

		return batchDetail;
	}

	public BatchDetail removeBatchDetail(BatchDetail batchDetail) {
		getBatchDetails().remove(batchDetail);
		batchDetail.setAuditLog(null);

		return batchDetail;
	}

	public List<BatchDetailsLog> getBatchDetailsLogs() {
		return this.batchDetailsLogs;
	}

	public void setBatchDetailsLogs(List<BatchDetailsLog> batchDetailsLogs) {
		this.batchDetailsLogs = batchDetailsLogs;
	}

	public BatchDetailsLog addBatchDetailsLog(BatchDetailsLog batchDetailsLog) {
		getBatchDetailsLogs().add(batchDetailsLog);
		batchDetailsLog.setAuditLog(this);

		return batchDetailsLog;
	}

	public BatchDetailsLog removeBatchDetailsLog(BatchDetailsLog batchDetailsLog) {
		getBatchDetailsLogs().remove(batchDetailsLog);
		batchDetailsLog.setAuditLog(null);

		return batchDetailsLog;
	}

	public List<BatchRecord> getBatchRecords() {
		return this.batchRecords;
	}

	public void setBatchRecords(List<BatchRecord> batchRecords) {
		this.batchRecords = batchRecords;
	}

	public BatchRecord addBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().add(batchRecord);
		batchRecord.setAuditLog(this);

		return batchRecord;
	}

	public BatchRecord removeBatchRecord(BatchRecord batchRecord) {
		getBatchRecords().remove(batchRecord);
		batchRecord.setAuditLog(null);

		return batchRecord;
	}

	public List<BatchRecordLog> getBatchRecordLogs() {
		return this.batchRecordLogs;
	}

	public void setBatchRecordLogs(List<BatchRecordLog> batchRecordLogs) {
		this.batchRecordLogs = batchRecordLogs;
	}

	public BatchRecordLog addBatchRecordLog(BatchRecordLog batchRecordLog) {
		getBatchRecordLogs().add(batchRecordLog);
		batchRecordLog.setAuditLog(this);

		return batchRecordLog;
	}

	public BatchRecordLog removeBatchRecordLog(BatchRecordLog batchRecordLog) {
		getBatchRecordLogs().remove(batchRecordLog);
		batchRecordLog.setAuditLog(null);

		return batchRecordLog;
	}

	public List<BatchSop> getBatchSops() {
		return this.batchSops;
	}

	public void setBatchSops(List<BatchSop> batchSops) {
		this.batchSops = batchSops;
	}

	public BatchSop addBatchSop(BatchSop batchSop) {
		getBatchSops().add(batchSop);
		batchSop.setAuditLog(this);

		return batchSop;
	}

	public BatchSop removeBatchSop(BatchSop batchSop) {
		getBatchSops().remove(batchSop);
		batchSop.setAuditLog(null);

		return batchSop;
	}

	public List<BatchTeam> getBatchTeams() {
		return this.batchTeams;
	}

	public void setBatchTeams(List<BatchTeam> batchTeams) {
		this.batchTeams = batchTeams;
	}

	public BatchTeam addBatchTeam(BatchTeam batchTeam) {
		getBatchTeams().add(batchTeam);
		batchTeam.setAuditLog(this);

		return batchTeam;
	}

	public BatchTeam removeBatchTeam(BatchTeam batchTeam) {
		getBatchTeams().remove(batchTeam);
		batchTeam.setAuditLog(null);

		return batchTeam;
	}

	public List<BatchTeamLog> getBatchTeamLogs() {
		return this.batchTeamLogs;
	}

	public void setBatchTeamLogs(List<BatchTeamLog> batchTeamLogs) {
		this.batchTeamLogs = batchTeamLogs;
	}

	public BatchTeamLog addBatchTeamLog(BatchTeamLog batchTeamLog) {
		getBatchTeamLogs().add(batchTeamLog);
		batchTeamLog.setAuditLog(this);

		return batchTeamLog;
	}

	public BatchTeamLog removeBatchTeamLog(BatchTeamLog batchTeamLog) {
		getBatchTeamLogs().remove(batchTeamLog);
		batchTeamLog.setAuditLog(null);

		return batchTeamLog;
	}

	public List<BatchType> getBatchTypes() {
		return this.batchTypes;
	}

	public void setBatchTypes(List<BatchType> batchTypes) {
		this.batchTypes = batchTypes;
	}

	public BatchType addBatchType(BatchType batchType) {
		getBatchTypes().add(batchType);
		batchType.setAuditLog(this);

		return batchType;
	}

	public BatchType removeBatchType(BatchType batchType) {
		getBatchTypes().remove(batchType);
		batchType.setAuditLog(null);

		return batchType;
	}

	public List<BatchTypeLog> getBatchTypeLogs() {
		return this.batchTypeLogs;
	}

	public void setBatchTypeLogs(List<BatchTypeLog> batchTypeLogs) {
		this.batchTypeLogs = batchTypeLogs;
	}

	public BatchTypeLog addBatchTypeLog(BatchTypeLog batchTypeLog) {
		getBatchTypeLogs().add(batchTypeLog);
		batchTypeLog.setAuditLog(this);

		return batchTypeLog;
	}

	public BatchTypeLog removeBatchTypeLog(BatchTypeLog batchTypeLog) {
		getBatchTypeLogs().remove(batchTypeLog);
		batchTypeLog.setAuditLog(null);

		return batchTypeLog;
	}

	public List<Corporate> getCorporates() {
		return this.corporates;
	}

	public void setCorporates(List<Corporate> corporates) {
		this.corporates = corporates;
	}

	public Corporate addCorporate(Corporate corporate) {
		getCorporates().add(corporate);
		corporate.setAuditLog(this);

		return corporate;
	}

	public Corporate removeCorporate(Corporate corporate) {
		getCorporates().remove(corporate);
		corporate.setAuditLog(null);

		return corporate;
	}

	public List<CorporateLog> getCorporateLogs() {
		return this.corporateLogs;
	}

	public void setCorporateLogs(List<CorporateLog> corporateLogs) {
		this.corporateLogs = corporateLogs;
	}

	public CorporateLog addCorporateLog(CorporateLog corporateLog) {
		getCorporateLogs().add(corporateLog);
		corporateLog.setAuditLog(this);

		return corporateLog;
	}

	public CorporateLog removeCorporateLog(CorporateLog corporateLog) {
		getCorporateLogs().remove(corporateLog);
		corporateLog.setAuditLog(null);

		return corporateLog;
	}

	public List<Department> getDepartments() {
		return this.departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public Department addDepartment(Department department) {
		getDepartments().add(department);
		department.setAuditLog(this);

		return department;
	}

	public Department removeDepartment(Department department) {
		getDepartments().remove(department);
		department.setAuditLog(null);

		return department;
	}

	public List<DepartmentsLog> getDepartmentsLogs() {
		return this.departmentsLogs;
	}

	public void setDepartmentsLogs(List<DepartmentsLog> departmentsLogs) {
		this.departmentsLogs = departmentsLogs;
	}

	public DepartmentsLog addDepartmentsLog(DepartmentsLog departmentsLog) {
		getDepartmentsLogs().add(departmentsLog);
		departmentsLog.setAuditLog(this);

		return departmentsLog;
	}

	public DepartmentsLog removeDepartmentsLog(DepartmentsLog departmentsLog) {
		getDepartmentsLogs().remove(departmentsLog);
		departmentsLog.setAuditLog(null);

		return departmentsLog;
	}

	public List<Lab> getLabs() {
		return this.labs;
	}

	public void setLabs(List<Lab> labs) {
		this.labs = labs;
	}

	public Lab addLab(Lab lab) {
		getLabs().add(lab);
		lab.setAuditLog(this);

		return lab;
	}

	public Lab removeLab(Lab lab) {
		getLabs().remove(lab);
		lab.setAuditLog(null);

		return lab;
	}

	public List<LabsLog> getLabsLogs() {
		return this.labsLogs;
	}

	public void setLabsLogs(List<LabsLog> labsLogs) {
		this.labsLogs = labsLogs;
	}

	public LabsLog addLabsLog(LabsLog labsLog) {
		getLabsLogs().add(labsLog);
		labsLog.setAuditLog(this);

		return labsLog;
	}

	public LabsLog removeLabsLog(LabsLog labsLog) {
		getLabsLogs().remove(labsLog);
		labsLog.setAuditLog(null);

		return labsLog;
	}

	public List<QmsDeviation> getQmsDeviations() {
		return this.qmsDeviations;
	}

	public void setQmsDeviations(List<QmsDeviation> qmsDeviations) {
		this.qmsDeviations = qmsDeviations;
	}

	public QmsDeviation addQmsDeviation(QmsDeviation qmsDeviation) {
		getQmsDeviations().add(qmsDeviation);
		qmsDeviation.setAuditLog(this);

		return qmsDeviation;
	}

	public QmsDeviation removeQmsDeviation(QmsDeviation qmsDeviation) {
		getQmsDeviations().remove(qmsDeviation);
		qmsDeviation.setAuditLog(null);

		return qmsDeviation;
	}

	public List<QmsDeviationLog> getQmsDeviationLogs() {
		return this.qmsDeviationLogs;
	}

	public void setQmsDeviationLogs(List<QmsDeviationLog> qmsDeviationLogs) {
		this.qmsDeviationLogs = qmsDeviationLogs;
	}

	public QmsDeviationLog addQmsDeviationLog(QmsDeviationLog qmsDeviationLog) {
		getQmsDeviationLogs().add(qmsDeviationLog);
		qmsDeviationLog.setAuditLog(this);

		return qmsDeviationLog;
	}

	public QmsDeviationLog removeQmsDeviationLog(QmsDeviationLog qmsDeviationLog) {
		getQmsDeviationLogs().remove(qmsDeviationLog);
		qmsDeviationLog.setAuditLog(null);

		return qmsDeviationLog;
	}

	public List<QmsOos> getQmsOos() {
		return this.qmsOos;
	}

	public void setQmsOos(List<QmsOos> qmsOos) {
		this.qmsOos = qmsOos;
	}

	public QmsOos addQmsOo(QmsOos qmsOo) {
		getQmsOos().add(qmsOo);
		qmsOo.setAuditLog(this);

		return qmsOo;
	}

	public QmsOos removeQmsOo(QmsOos qmsOo) {
		getQmsOos().remove(qmsOo);
		qmsOo.setAuditLog(null);

		return qmsOo;
	}

	public List<QmsOosLog> getQmsOosLogs() {
		return this.qmsOosLogs;
	}

	public void setQmsOosLogs(List<QmsOosLog> qmsOosLogs) {
		this.qmsOosLogs = qmsOosLogs;
	}

	public QmsOosLog addQmsOosLog(QmsOosLog qmsOosLog) {
		getQmsOosLogs().add(qmsOosLog);
		qmsOosLog.setAuditLog(this);

		return qmsOosLog;
	}

	public QmsOosLog removeQmsOosLog(QmsOosLog qmsOosLog) {
		getQmsOosLogs().remove(qmsOosLog);
		qmsOosLog.setAuditLog(null);

		return qmsOosLog;
	}

	public List<SecurityRoleLog> getSecurityRoleLogs() {
		return this.securityRoleLogs;
	}

	public void setSecurityRoleLogs(List<SecurityRoleLog> securityRoleLogs) {
		this.securityRoleLogs = securityRoleLogs;
	}

	public SecurityRoleLog addSecurityRoleLog(SecurityRoleLog securityRoleLog) {
		getSecurityRoleLogs().add(securityRoleLog);
		securityRoleLog.setAuditLog(this);

		return securityRoleLog;
	}

	public SecurityRoleLog removeSecurityRoleLog(SecurityRoleLog securityRoleLog) {
		getSecurityRoleLogs().remove(securityRoleLog);
		securityRoleLog.setAuditLog(null);

		return securityRoleLog;
	}

	public List<SecurityRoleMapLog> getSecurityRoleMapLogs() {
		return this.securityRoleMapLogs;
	}

	public void setSecurityRoleMapLogs(List<SecurityRoleMapLog> securityRoleMapLogs) {
		this.securityRoleMapLogs = securityRoleMapLogs;
	}

	public SecurityRoleMapLog addSecurityRoleMapLog(SecurityRoleMapLog securityRoleMapLog) {
		getSecurityRoleMapLogs().add(securityRoleMapLog);
		securityRoleMapLog.setAuditLog(this);

		return securityRoleMapLog;
	}

	public SecurityRoleMapLog removeSecurityRoleMapLog(SecurityRoleMapLog securityRoleMapLog) {
		getSecurityRoleMapLogs().remove(securityRoleMapLog);
		securityRoleMapLog.setAuditLog(null);

		return securityRoleMapLog;
	}

	public List<Sop> getSops() {
		return this.sops;
	}

	public void setSops(List<Sop> sops) {
		this.sops = sops;
	}

	public Sop addSop(Sop sop) {
		getSops().add(sop);
		sop.setAuditLog(this);

		return sop;
	}

	public Sop removeSop(Sop sop) {
		getSops().remove(sop);
		sop.setAuditLog(null);

		return sop;
	}

	public List<SopLog> getSopLogs() {
		return this.sopLogs;
	}

	public void setSopLogs(List<SopLog> sopLogs) {
		this.sopLogs = sopLogs;
	}

	public SopLog addSopLog(SopLog sopLog) {
		getSopLogs().add(sopLog);
		sopLog.setAuditLog(this);

		return sopLog;
	}

	public SopLog removeSopLog(SopLog sopLog) {
		getSopLogs().remove(sopLog);
		sopLog.setAuditLog(null);

		return sopLog;
	}

	public List<SopType> getSopTypes() {
		return this.sopTypes;
	}

	public void setSopTypes(List<SopType> sopTypes) {
		this.sopTypes = sopTypes;
	}

	public SopType addSopType(SopType sopType) {
		getSopTypes().add(sopType);
		sopType.setAuditLog(this);

		return sopType;
	}

	public SopType removeSopType(SopType sopType) {
		getSopTypes().remove(sopType);
		sopType.setAuditLog(null);

		return sopType;
	}

	public List<SopTypeLog> getSopTypeLogs() {
		return this.sopTypeLogs;
	}

	public void setSopTypeLogs(List<SopTypeLog> sopTypeLogs) {
		this.sopTypeLogs = sopTypeLogs;
	}

	public SopTypeLog addSopTypeLog(SopTypeLog sopTypeLog) {
		getSopTypeLogs().add(sopTypeLog);
		sopTypeLog.setAuditLog(this);

		return sopTypeLog;
	}

	public SopTypeLog removeSopTypeLog(SopTypeLog sopTypeLog) {
		getSopTypeLogs().remove(sopTypeLog);
		sopTypeLog.setAuditLog(null);

		return sopTypeLog;
	}

	public List<SopTypeVisibility> getSopTypeVisibilities() {
		return this.sopTypeVisibilities;
	}

	public void setSopTypeVisibilities(List<SopTypeVisibility> sopTypeVisibilities) {
		this.sopTypeVisibilities = sopTypeVisibilities;
	}

	public SopTypeVisibility addSopTypeVisibility(SopTypeVisibility sopTypeVisibility) {
		getSopTypeVisibilities().add(sopTypeVisibility);
		sopTypeVisibility.setAuditLog(this);

		return sopTypeVisibility;
	}

	public SopTypeVisibility removeSopTypeVisibility(SopTypeVisibility sopTypeVisibility) {
		getSopTypeVisibilities().remove(sopTypeVisibility);
		sopTypeVisibility.setAuditLog(null);

		return sopTypeVisibility;
	}

	public List<UserNotificationStatus> getUserNotificationStatuses() {
		return this.userNotificationStatuses;
	}

	public void setUserNotificationStatuses(List<UserNotificationStatus> userNotificationStatuses) {
		this.userNotificationStatuses = userNotificationStatuses;
	}

	public UserNotificationStatus addUserNotificationStatus(UserNotificationStatus userNotificationStatus) {
		getUserNotificationStatuses().add(userNotificationStatus);
		userNotificationStatus.setAuditLog(this);

		return userNotificationStatus;
	}

	public UserNotificationStatus removeUserNotificationStatus(UserNotificationStatus userNotificationStatus) {
		getUserNotificationStatuses().remove(userNotificationStatus);
		userNotificationStatus.setAuditLog(null);

		return userNotificationStatus;
	}

	public List<UserNotificationStatusLog> getUserNotificationStatusLogs() {
		return this.userNotificationStatusLogs;
	}

	public void setUserNotificationStatusLogs(List<UserNotificationStatusLog> userNotificationStatusLogs) {
		this.userNotificationStatusLogs = userNotificationStatusLogs;
	}

	public UserNotificationStatusLog addUserNotificationStatusLog(UserNotificationStatusLog userNotificationStatusLog) {
		getUserNotificationStatusLogs().add(userNotificationStatusLog);
		userNotificationStatusLog.setAuditLog(this);

		return userNotificationStatusLog;
	}

	public UserNotificationStatusLog removeUserNotificationStatusLog(UserNotificationStatusLog userNotificationStatusLog) {
		getUserNotificationStatusLogs().remove(userNotificationStatusLog);
		userNotificationStatusLog.setAuditLog(null);

		return userNotificationStatusLog;
	}

	public List<WorkFlowMetrix> getWorkFlowMetrixs() {
		return this.workFlowMetrixs;
	}

	public void setWorkFlowMetrixs(List<WorkFlowMetrix> workFlowMetrixs) {
		this.workFlowMetrixs = workFlowMetrixs;
	}

	public WorkFlowMetrix addWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		getWorkFlowMetrixs().add(workFlowMetrix);
		workFlowMetrix.setAuditLog(this);

		return workFlowMetrix;
	}

	public WorkFlowMetrix removeWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		getWorkFlowMetrixs().remove(workFlowMetrix);
		workFlowMetrix.setAuditLog(null);

		return workFlowMetrix;
	}

	public List<WorkFlowMetrixLog> getWorkFlowMetrixLogs() {
		return this.workFlowMetrixLogs;
	}

	public void setWorkFlowMetrixLogs(List<WorkFlowMetrixLog> workFlowMetrixLogs) {
		this.workFlowMetrixLogs = workFlowMetrixLogs;
	}

	public WorkFlowMetrixLog addWorkFlowMetrixLog(WorkFlowMetrixLog workFlowMetrixLog) {
		getWorkFlowMetrixLogs().add(workFlowMetrixLog);
		workFlowMetrixLog.setAuditLog(this);

		return workFlowMetrixLog;
	}

	public WorkFlowMetrixLog removeWorkFlowMetrixLog(WorkFlowMetrixLog workFlowMetrixLog) {
		getWorkFlowMetrixLogs().remove(workFlowMetrixLog);
		workFlowMetrixLog.setAuditLog(null);

		return workFlowMetrixLog;
	}

}