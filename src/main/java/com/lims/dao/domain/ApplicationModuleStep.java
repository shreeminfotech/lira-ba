package com.lims.dao.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.lims.enums.StepKeys;


/**
 * The persistent class for the application_module_steps database table.
 * 
 */
@Entity
@Table(name="application_module_steps")
@NamedQuery(name="ApplicationModuleStep.findAll", query="SELECT a FROM ApplicationModuleStep a")
public class ApplicationModuleStep implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private Boolean clickable;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	private Boolean enabled;

	@Column(name="step_key")
	@Enumerated(EnumType.STRING)
	private StepKeys stepKey;
	
	@Column(name="step_desc")
	private String stepDesc;

	@Column(name="step_order")
	private int stepOrder;

	//bi-directional many-to-one association to ApplicationModule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="module_id")
	private ApplicationModule applicationModule;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to WorkFlowMetrix
	@OneToMany(mappedBy="applicationModuleStep")
	private List<WorkFlowMetrix> workFlowMetrixs;

	public ApplicationModuleStep() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getClickable() {
		return this.clickable;
	}

	public void setClickable(Boolean clickable) {
		this.clickable = clickable;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public StepKeys getStepKey() {
		return this.stepKey;
	}

	public void setStepKey(StepKeys stepKey) {
		this.stepKey = stepKey;
	}

	public String getStepDesc() {
		return stepDesc;
	}

	public void setStepDesc(String stepDesc) {
		this.stepDesc = stepDesc;
	}

	public int getStepOrder() {
		return this.stepOrder;
	}

	public void setStepOrder(int stepOrder) {
		this.stepOrder = stepOrder;
	}

	public ApplicationModule getApplicationModule() {
		return this.applicationModule;
	}

	public void setApplicationModule(ApplicationModule applicationModule) {
		this.applicationModule = applicationModule;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public List<WorkFlowMetrix> getWorkFlowMetrixs() {
		return this.workFlowMetrixs;
	}

	public void setWorkFlowMetrixs(List<WorkFlowMetrix> workFlowMetrixs) {
		this.workFlowMetrixs = workFlowMetrixs;
	}

	public WorkFlowMetrix addWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		getWorkFlowMetrixs().add(workFlowMetrix);
		workFlowMetrix.setApplicationModuleStep(this);

		return workFlowMetrix;
	}

	public WorkFlowMetrix removeWorkFlowMetrix(WorkFlowMetrix workFlowMetrix) {
		getWorkFlowMetrixs().remove(workFlowMetrix);
		workFlowMetrix.setApplicationModuleStep(null);

		return workFlowMetrix;
	}

}