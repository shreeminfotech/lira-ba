package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the application_user_roles database table.
 * 
 */
@Entity
@Table(name="application_user_roles")
@NamedQuery(name="ApplicationUserRole.findAll", query="SELECT a FROM ApplicationUserRole a")
public class ApplicationUserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to ApplicationSecurityRole
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="security_role_id")
	private ApplicationSecurityRole applicationSecurityRole;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	public ApplicationUserRole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ApplicationSecurityRole getApplicationSecurityRole() {
		return this.applicationSecurityRole;
	}

	public void setApplicationSecurityRole(ApplicationSecurityRole applicationSecurityRole) {
		this.applicationSecurityRole = applicationSecurityRole;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

}