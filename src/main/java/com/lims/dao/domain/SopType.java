package com.lims.dao.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the sop_type database table.
 * 
 */
@Entity
@Table(name="sop_type")
@NamedQuery(name="SopType.findAll", query="SELECT s FROM SopType s")
public class SopType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="color_code")
	private String colorCode;

	@Lob
	private String comments;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	private String name;

	@Column(name="system_default")
	private Boolean systemDefault;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	private String visibility;

	//bi-directional many-to-one association to Sop
	@OneToMany(mappedBy="sopType")
	private List<Sop> sops;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to Lab
	@ManyToOne(fetch=FetchType.LAZY)
	private Lab lab;

	//bi-directional many-to-one association to SopTypeVisibility
	@OneToMany(mappedBy="sopType", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SopTypeVisibility> sopTypeVisibilities;

	public SopType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColorCode() {
		return this.colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getSystemDefault() {
		return this.systemDefault;
	}

	public void setSystemDefault(Boolean systemDefault) {
		this.systemDefault = systemDefault;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getVisibility() {
		return this.visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public List<Sop> getSops() {
		return this.sops;
	}

	public void setSops(List<Sop> sops) {
		this.sops = sops;
	}

	public Sop addSop(Sop sop) {
		getSops().add(sop);
		sop.setSopType(this);

		return sop;
	}

	public Sop removeSop(Sop sop) {
		getSops().remove(sop);
		sop.setSopType(null);

		return sop;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Lab getLab() {
		return this.lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public List<SopTypeVisibility> getSopTypeVisibilities() {
		return this.sopTypeVisibilities;
	}

	public void setSopTypeVisibilities(List<SopTypeVisibility> sopTypeVisibilities) {
		this.sopTypeVisibilities = sopTypeVisibilities;
	}

	public void addSopTypeVisibility(SopTypeVisibility sopTypeVisibility) {
		if(this.sopTypeVisibilities == null)
			this.sopTypeVisibilities = new ArrayList<SopTypeVisibility>();
		this.sopTypeVisibilities.add(sopTypeVisibility);
		sopTypeVisibility.setSopType(this);
	}

	public void removeSopTypeVisibility(SopTypeVisibility sopTypeVisibility) {
		this.sopTypeVisibilities.remove(sopTypeVisibility);
		sopTypeVisibility.setSopType(null);
	}

}