package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the qms_deviation_sops database table.
 * 
 */
@Entity
@Table(name="qms_deviation_sops")
@NamedQuery(name="QmsDeviationSop.findAll", query="SELECT q FROM QmsDeviationSop q")
public class QmsDeviationSop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to QmsDeviation
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="deviation_id")
	private QmsDeviation qmsDeviation;

	//bi-directional many-to-one association to Sop
	@ManyToOne(fetch=FetchType.LAZY)
	private Sop sop;

	public QmsDeviationSop() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public QmsDeviation getQmsDeviation() {
		return this.qmsDeviation;
	}

	public void setQmsDeviation(QmsDeviation qmsDeviation) {
		this.qmsDeviation = qmsDeviation;
	}

	public Sop getSop() {
		return this.sop;
	}

	public void setSop(Sop sop) {
		this.sop = sop;
	}

}