package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the application_document_types database table.
 * 
 */
@Entity
@Table(name="application_document_types")
@NamedQuery(name="ApplicationDocumentType.findAll", query="SELECT a FROM ApplicationDocumentType a")
public class ApplicationDocumentType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	private String type;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to ApplicationUserDocument
	@OneToMany(mappedBy="applicationDocumentType")
	private List<ApplicationUserDocument> applicationUserDocuments;

	public ApplicationDocumentType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public List<ApplicationUserDocument> getApplicationUserDocuments() {
		return this.applicationUserDocuments;
	}

	public void setApplicationUserDocuments(List<ApplicationUserDocument> applicationUserDocuments) {
		this.applicationUserDocuments = applicationUserDocuments;
	}

	public ApplicationUserDocument addApplicationUserDocument(ApplicationUserDocument applicationUserDocument) {
		getApplicationUserDocuments().add(applicationUserDocument);
		applicationUserDocument.setApplicationDocumentType(this);

		return applicationUserDocument;
	}

	public ApplicationUserDocument removeApplicationUserDocument(ApplicationUserDocument applicationUserDocument) {
		getApplicationUserDocuments().remove(applicationUserDocument);
		applicationUserDocument.setApplicationDocumentType(null);

		return applicationUserDocument;
	}

}