package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the sop database table.
 * 
 */
@Entity
@NamedQuery(name="Sop.findAll", query="SELECT s FROM Sop s")
public class Sop implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private byte[] content;

	@Column(name="content_type")
	private String contentType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	private String desciption;
	
	@Column(name="completed_step")
	private Integer completedStep;
	
	@Column(name="current_step")
	private Integer currentStep;

	@Lob
	@Column(name="document_json")
	private String documentJson;

	@Column(name="document_name")
	private String documentName;

	private int filesize;

	@Column(name="is_active")
	private Boolean isActive;

	private String name;

	private String number;

	private String revision;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_date")
	private Date updatedDate;

	private String version;

	//bi-directional many-to-one association to BatchSop
	@OneToMany(mappedBy="sop")
	private List<BatchSop> batchSops;

	//bi-directional many-to-one association to QmsDeviationSop
	@OneToMany(mappedBy="sop")
	private List<QmsDeviationSop> qmsDeviationSops;

	//bi-directional many-to-one association to QmsOosSop
	@OneToMany(mappedBy="sop")
	private List<QmsOosSop> qmsOosSops;

	//bi-directional many-to-one association to ApplicationStatus
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="status_id")
	private ApplicationStatus applicationStatus;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="updated_by")
	private ApplicationUser applicationUser2;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to Lab
	@ManyToOne(fetch=FetchType.LAZY)
	private Lab lab;

	//bi-directional many-to-one association to SopType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="type_id")
	private SopType sopType;

	public Sop() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getContent() {
		return this.content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDesciption() {
		return this.desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getDocumentJson() {
		return this.documentJson;
	}

	public void setDocumentJson(String documentJson) {
		this.documentJson = documentJson;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public int getFilesize() {
		return this.filesize;
	}

	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getRevision() {
		return this.revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<BatchSop> getBatchSops() {
		return this.batchSops;
	}

	public void setBatchSops(List<BatchSop> batchSops) {
		this.batchSops = batchSops;
	}

	public BatchSop addBatchSop(BatchSop batchSop) {
		getBatchSops().add(batchSop);
		batchSop.setSop(this);

		return batchSop;
	}

	public BatchSop removeBatchSop(BatchSop batchSop) {
		getBatchSops().remove(batchSop);
		batchSop.setSop(null);

		return batchSop;
	}

	public List<QmsDeviationSop> getQmsDeviationSops() {
		return this.qmsDeviationSops;
	}

	public void setQmsDeviationSops(List<QmsDeviationSop> qmsDeviationSops) {
		this.qmsDeviationSops = qmsDeviationSops;
	}

	public QmsDeviationSop addQmsDeviationSop(QmsDeviationSop qmsDeviationSop) {
		getQmsDeviationSops().add(qmsDeviationSop);
		qmsDeviationSop.setSop(this);

		return qmsDeviationSop;
	}

	public QmsDeviationSop removeQmsDeviationSop(QmsDeviationSop qmsDeviationSop) {
		getQmsDeviationSops().remove(qmsDeviationSop);
		qmsDeviationSop.setSop(null);

		return qmsDeviationSop;
	}

	public List<QmsOosSop> getQmsOosSops() {
		return this.qmsOosSops;
	}

	public void setQmsOosSops(List<QmsOosSop> qmsOosSops) {
		this.qmsOosSops = qmsOosSops;
	}

	public QmsOosSop addQmsOosSop(QmsOosSop qmsOosSop) {
		getQmsOosSops().add(qmsOosSop);
		qmsOosSop.setSop(this);

		return qmsOosSop;
	}

	public QmsOosSop removeQmsOosSop(QmsOosSop qmsOosSop) {
		getQmsOosSops().remove(qmsOosSop);
		qmsOosSop.setSop(null);

		return qmsOosSop;
	}

	public ApplicationStatus getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(ApplicationStatus applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public Lab getLab() {
		return this.lab;
	}

	public void setLab(Lab lab) {
		this.lab = lab;
	}

	public SopType getSopType() {
		return this.sopType;
	}

	public void setSopType(SopType sopType) {
		this.sopType = sopType;
	}

	public Integer getCompletedStep() {
		return completedStep;
	}

	public void setCompletedStep(Integer completedStep) {
		this.completedStep = completedStep;
	}

	public Integer getCurrentStep() {
		return currentStep;
	}

	public void setCurrentStep(Integer currentStep) {
		this.currentStep = currentStep;
	}

}