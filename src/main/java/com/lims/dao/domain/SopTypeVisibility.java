package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the sop_type_visibility database table.
 * 
 */
@Entity
@Table(name="sop_type_visibility")
@NamedQuery(name="SopTypeVisibility.findAll", query="SELECT s FROM SopTypeVisibility s")
public class SopTypeVisibility implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private ApplicationUser applicationUser;

	//bi-directional many-to-one association to AuditLog
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="audit_id")
	private AuditLog auditLog;

	//bi-directional many-to-one association to SopType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sop_type_id")
	private SopType sopType;

	public SopTypeVisibility() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ApplicationUser getApplicationUser() {
		return this.applicationUser;
	}

	public void setApplicationUser(ApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	public AuditLog getAuditLog() {
		return this.auditLog;
	}

	public void setAuditLog(AuditLog auditLog) {
		this.auditLog = auditLog;
	}

	public SopType getSopType() {
		return this.sopType;
	}

	public void setSopType(SopType sopType) {
		this.sopType = sopType;
	}

}