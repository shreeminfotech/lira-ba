package com.lims.dao.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the application_user_documents database table.
 * 
 */
@Entity
@Table(name="application_user_documents")
@NamedQuery(name="ApplicationUserDocument.findAll", query="SELECT a FROM ApplicationUserDocument a")
public class ApplicationUserDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private byte[] content;

	@Column(name="content_type")
	private String contentType;

	@Column(name="document_name")
	private String documentName;

	private int filesize;

	@Column(name="is_active")
	private Boolean isActive;

	private Boolean isdeleted;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="uploaded_date")
	private Date uploadedDate;

	//bi-directional many-to-one association to ApplicationDocumentType
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="document_type_id")
	private ApplicationDocumentType applicationDocumentType;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private ApplicationUser applicationUser1;

	//bi-directional many-to-one association to ApplicationUser
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="uploaded_by")
	private ApplicationUser applicationUser2;

	public ApplicationUserDocument() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getContent() {
		return this.content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public int getFilesize() {
		return this.filesize;
	}

	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsdeleted() {
		return this.isdeleted;
	}

	public void setIsdeleted(Boolean isdeleted) {
		this.isdeleted = isdeleted;
	}

	public Date getUploadedDate() {
		return this.uploadedDate;
	}

	public void setUploadedDate(Date uploadedDate) {
		this.uploadedDate = uploadedDate;
	}

	public ApplicationDocumentType getApplicationDocumentType() {
		return this.applicationDocumentType;
	}

	public void setApplicationDocumentType(ApplicationDocumentType applicationDocumentType) {
		this.applicationDocumentType = applicationDocumentType;
	}

	public ApplicationUser getApplicationUser1() {
		return this.applicationUser1;
	}

	public void setApplicationUser1(ApplicationUser applicationUser1) {
		this.applicationUser1 = applicationUser1;
	}

	public ApplicationUser getApplicationUser2() {
		return this.applicationUser2;
	}

	public void setApplicationUser2(ApplicationUser applicationUser2) {
		this.applicationUser2 = applicationUser2;
	}

}