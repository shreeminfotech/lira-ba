package com.lims.exceptions;

public class UserNotFoundException extends RuntimeException {

	UserNotFoundException(Integer id) {
		super("Could not find employee " + id);
	}
}
